import id.co.telkomsigma.btpns.mprospera.controller.webservice.WebServiceSyncAP3RV2Controller;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanDeviation;
import id.co.telkomsigma.btpns.mprospera.model.sw.AP3R;
import id.co.telkomsigma.btpns.mprospera.model.sw.FundedThings;
import id.co.telkomsigma.btpns.mprospera.model.sw.SWProductMapping;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.request.GetDetailAP3RRequest;
import id.co.telkomsigma.btpns.mprospera.response.AP3RDetailResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class GetDetailAP3RControllerTest {

    @TestConfiguration
    static class GetDetailAP3RControllerTestContextConfiguration {

        @Bean
        public WebServiceSyncAP3RV2Controller webServiceSyncAP3RV2Controller() {
            return new WebServiceSyncAP3RV2Controller();
        }

        @Bean
        protected Locale locale() {
            return new Locale("id", "ID");
        }
    }

    @Autowired
    private WebServiceSyncAP3RV2Controller webServiceSyncAP3RV2Controller;

    @MockBean
    private WSValidationService wsValidationService;

    @MockBean
    private TerminalActivityService terminalActivityService;

    @MockBean
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @MockBean
    private TerminalService terminalService;

    @MockBean
    private AP3RService ap3RService;

    @MockBean
    private UserService userService;

    @MockBean
    private SWService swService;

    @MockBean
    private LoanService loanService;

    @MockBean
    private CustomerService customerService;

    @MockBean
    private AreaService areaService;

    String username = "w0211f";
    String imei = "358525070497940";
    String apkVersion = "3.70";
    String sessionKey = "0beba6ac-2b04-4889-866f-b36dea07aff6";
    AP3RDetailResponse syncAP3RV2Response = new AP3RDetailResponse();
    AP3R ap3r = new AP3R();
    Terminal terminal = new Terminal();
    User user = new User();
    SurveyWawancara sw = new SurveyWawancara();
    SWProductMapping swProductMapping = new SWProductMapping();
    LoanDeviation loanDeviation = new LoanDeviation();
    List<FundedThings> fundedThingsList = new ArrayList<>();

    @Before
    public void setUp() {
        syncAP3RV2Response.setResponseCode("00");
        ap3r.setAp3rId(Long.parseLong("12345"));
        ap3r.setSwId(Long.parseLong("35254"));
        ap3r.setLocalId("24352462622135");
        ap3r.setSwProductId(Long.parseLong("3524652"));
        ap3r.setDisbursementDate(new Date());
        sw.setSwId(Long.parseLong("35254"));
        user.setUserId(Long.parseLong("142432"));
        user.setRoleUser("1");
        swProductMapping.setMappingId(Long.parseLong("3524652"));
        loanDeviation.setDeviationId(Long.parseLong("52563"));
        Mockito.when(wsValidationService.wsValidation(username, imei, sessionKey, apkVersion)).thenReturn("00");
        Mockito.when(userService.findUserByUsername(username)).thenReturn(user);
        Mockito.when(terminalService.loadTerminalByImei(imei)).thenReturn(terminal);
        Mockito.when(ap3RService.findByLocalId(ap3r.getLocalId())).thenReturn(ap3r);
        Mockito.when(swService.getSWById(sw.getSwId().toString())).thenReturn(sw);
        Mockito.when(swService.findProductMapById(ap3r.getSwProductId())).thenReturn(swProductMapping);
        Mockito.when(loanService.findDeviationByAp3rId(ap3r.getAp3rId())).thenReturn(loanDeviation);
        Mockito.when(ap3RService.findByAp3rId(ap3r)).thenReturn(fundedThingsList);
    }

    @Test
    public void WhenGetDetailAP3R_ThenReturnSuccess() {
        GetDetailAP3RRequest ap3RRequest = new GetDetailAP3RRequest();
        ap3RRequest.setImei("358525070497940");
        ap3RRequest.setUsername("w0211f");
        ap3RRequest.setAp3rId("24352462622135");
        ap3RRequest.setSessionKey("0beba6ac-2b04-4889-866f-b36dea07aff6");
        try {
            AP3RDetailResponse result = webServiceSyncAP3RV2Controller.getDetailAp3r(ap3RRequest, apkVersion);
            assertEquals(result.getResponseCode(), syncAP3RV2Response.getResponseCode());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}