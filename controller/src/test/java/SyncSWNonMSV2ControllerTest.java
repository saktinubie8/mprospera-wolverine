import id.co.telkomsigma.btpns.mprospera.controller.webservice.WebServiceSyncSWV2Controller;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.sw.*;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.request.SWRequest;
import id.co.telkomsigma.btpns.mprospera.response.SyncSWV2Response;
import id.co.telkomsigma.btpns.mprospera.service.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class SyncSWNonMSV2ControllerTest {

    @TestConfiguration
    static class SyncSWNonMSV2ControllerTestContextConfiguration {
        @Bean
        public WebServiceSyncSWV2Controller webServiceSyncSWV2Controller() {
            return new WebServiceSyncSWV2Controller();
        }

        @Bean
        protected Locale locale() {
            return new Locale("id", "ID");
        }
    }

    @Autowired
    private WebServiceSyncSWV2Controller webServiceSyncSWV2Controller;

    @MockBean
    private WSValidationService wsValidationService;

    @MockBean
    private TerminalActivityService terminalActivityService;

    @MockBean
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @MockBean
    private TerminalService terminalService;

    @MockBean
    private SWService swService;

    @MockBean
    private CustomerService customerService;

    @MockBean
    private AreaService areaService;

    @MockBean
    private AP3RService ap3RService;

    @MockBean
    private LoanService loanService;

    @MockBean
    private UserService userService;

    String username = "w0211f";
    String imei = "358525070497940";
    String apkVersion = "3.70";
    String sessionKey = "0beba6ac-2b04-4889-866f-b36dea07aff6";
    String page = "1";
    String countData = "500";
    String startLookupDate = "";
    String endLookupDate = "";
    SyncSWV2Response syncSWV2Response = new SyncSWV2Response();
    SurveyWawancara sw = new SurveyWawancara();
    Page<SurveyWawancara> swList;
    List<SWProductMapping> productMapList;
    List<SurveyWawancara> deletedSwList;
    LoanProduct loanProduct = new LoanProduct();
    Customer customer = new Customer();
    Terminal terminal = new Terminal();
    List<AWGMBuyThings> awgmBuyThingsList = new ArrayList<>();
    List<DirectBuyThings> directBuyProductList = new ArrayList<>();
    List<NeighborRecommendation> neighborList = new ArrayList<>();
    List<SWUserBWMPMapping> swUserBwmpList = new ArrayList<>();

    @Before
    public void setUp() {
        syncSWV2Response.setResponseCode("00");
        sw.setSwId(Long.parseLong("12345"));
        sw.setCustomerId(Long.parseLong("424135"));
        sw.setLocalId("24352462622135");
        customer.setCustomerId(Long.parseLong("424135"));
        loanProduct.setProductId(Long.parseLong("2151"));
        Mockito.when(wsValidationService.wsValidation(username, imei, sessionKey, apkVersion)).thenReturn("00");
        try {
            Mockito.when(swService.getSWByUserMS(username, page, countData, startLookupDate, endLookupDate)).thenReturn(swList);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Mockito.when(swService.findProductMapBySwId(sw.getSwId())).thenReturn(productMapList);
        Mockito.when(swService.findByProductId(loanProduct.getProductId().toString())).thenReturn(loanProduct);
        Mockito.when(customerService.findById(sw.getCustomerId().toString())).thenReturn(customer);
        Mockito.when(terminalService.loadTerminalByImei(imei)).thenReturn(terminal);
        Mockito.when(swService.findIsDeletedSwList()).thenReturn(deletedSwList);
        Mockito.when(swService.findSwByLocalId(sw.getLocalId())).thenReturn(sw);
        Mockito.when(swService.findAwgmProductBySwId(sw)).thenReturn(awgmBuyThingsList);
        Mockito.when(swService.findProductBySwId(sw)).thenReturn(directBuyProductList);
        Mockito.when(swService.findNeighborBySwId(sw)).thenReturn(neighborList);
        Mockito.when(swService.findBySw(sw.getSwId())).thenReturn(swUserBwmpList);
    }

    @Test
    public void WhenSyncSWNonMSV2_ThenReturnSuccess() {
        SWRequest swRequest = new SWRequest();
        swRequest.setImei("358525070497940");
        swRequest.setUsername("w0211f");
        swRequest.setStartLookupDate("");
        swRequest.setEndLookupDate("");
        swRequest.setPage(page);
        swRequest.setGetCountData(countData);
        swRequest.setSessionKey("0beba6ac-2b04-4889-866f-b36dea07aff6");
        try {
            SyncSWV2Response result = webServiceSyncSWV2Controller.syncSwNonMsV2(swRequest, apkVersion);
            assertEquals(result.getResponseCode(), syncSWV2Response.getResponseCode());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}