package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanDeviation;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.sw.*;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.request.*;
import id.co.telkomsigma.btpns.mprospera.response.*;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.FieldValidationUtil;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller("webServiceSyncAP3RV2Controller")
public class WebServiceSyncAP3RV2Controller extends GenericController {

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private UserService userService;

    @Autowired
    private SWService swService;

    @Autowired
    private AP3RService ap3rService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private LoanService loanService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private AreaService areaService;

    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
    SimpleDateFormat formatDateTime = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
    SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");
    JsonUtils jsonUtils = new JsonUtils();

    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_AP3R_V2_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    AP3RListV2Response doAP3RListV2(@RequestBody AP3RRequest request,
                                    @PathVariable("apkVersion") String apkVersion) throws Exception {

        final AP3RListV2Response responseCode = new AP3RListV2Response();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        responseCode.setResponseMessage("SUKSES");
        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        String countData = request.getGetCountData();
        String page = request.getPage();
        String startLookupDate = request.getStartLookupDate();
        String endLookupDate = request.getEndLookupDate();
        try {
            log.info("listAp3r/v2 INCOMING MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                log.info("Validation success, get AP3R data");
                User user = userService.findUserByUsername(username);
                if (user.getRoleUser() == null) {
                    responseCode.setResponseCode(WebGuiConstant.ROLE_NULL);
                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(label);
                    return responseCode;
                }
                final Page<AP3R> ap3rPage = ap3rService.getAp3rByUserMS(username, page, countData,
                        startLookupDate, endLookupDate);
                log.info("Finishing get AP3R data");
                if (ap3rPage != null) {
                    List<AP3RListV2> ap3rListResponse = new ArrayList<>();
                    for (final AP3R ap3r : ap3rPage) {
                        AP3RListV2 ap3rMap = new AP3RListV2();
                        ap3rMap.setAp3rId(ap3r.getLocalId());
                        SWProductMapping swProductMapLocal = swService.findProductMapById(ap3r.getSwProductId());
                        if (swProductMapLocal != null) {
                            LoanProduct loanProduct = new LoanProduct();
                            if (swProductMapLocal.getRecommendedProductId() != null) {
                                loanProduct = swService
                                        .findByProductId(swProductMapLocal.getRecommendedProductId().toString());
                            } else {
                                loanProduct = swService.findByProductId(swProductMapLocal.getProductId().toString());
                            }
                            ap3rMap.setProductName(loanProduct.getProductName());
                            if (swProductMapLocal.getRecommendedPlafon() != null) {
                                ap3rMap.setSelectedPlafon(swProductMapLocal.getRecommendedPlafon().toString());
                            } else {
                                ap3rMap.setSelectedPlafon(swProductMapLocal.getPlafon().toString());
                            }
                        }
                        ap3rMap.setCreatedBy(ap3r.getCreatedBy());
                        ap3rMap.setStatus(ap3r.getStatus());
                        if (ap3r.getSwId() != null) {
                            SurveyWawancara sw = swService.getSWById(ap3r.getSwId().toString());
                            if (sw != null) {
                                ap3rMap.setCustomerName(sw.getCustomerIdName());
                                ap3rMap.setAddress(sw.getAddress());
                                if (sw.getCustomerId() != null) {
                                    Customer customer = customerService.findById(sw.getCustomerId().toString());
                                    if (customer != null) {
                                        ap3rMap.setCifNumber(customer.getCustomerCifNumber());
                                        if (customer.getGroup() != null) {
                                            if (customer.getGroup().getSentra() != null) {
                                                ap3rMap.setSentraName(customer.getGroup().getSentra().getSentraName());
                                            }
                                        }
                                        if (ap3r.getAp3rId() != null) {
                                            Loan loan = loanService.findByAp3rAndCustomer(ap3r.getAp3rId(), customer);
                                            if (loan != null) {
                                                ap3rMap.setAppId(loan.getAppId());
                                            }
                                        } else
                                            ap3rMap.setAppId("");
                                    }
                                }
                            }
                        }
                        ap3rListResponse.add(ap3rMap);
                    }
                    final List<AP3R> listDeletedAp3r = ap3rService.findIsDeletedAp3rList();
                    List<String> deletedAp3rList = new ArrayList<>();
                    responseCode.setCurrentTotal(String.valueOf(ap3rPage.getContent().size()));
                    responseCode.setGrandTotal(String.valueOf(ap3rPage.getTotalElements()));
                    responseCode.setTotalPage(String.valueOf(ap3rPage.getTotalPages()));
                    responseCode.setAp3rList(ap3rListResponse);
                    if (!listDeletedAp3r.isEmpty()) {
                        for (AP3R deletedAp3r : listDeletedAp3r) {
                            String id = deletedAp3r.getLocalId();
                            deletedAp3rList.add(id);
                        }
                    }
                    responseCode.setDeletedAp3rList(deletedAp3rList);
                }
            }
        } catch (Exception e) {
            log.error("listAp3r/v2 error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("listAp3r/v2 error: " + e.getMessage());
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(imei);
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_AP3R);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(sessionKey);
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(username.trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("listAp3r/v2 RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("listAp3r/v2 saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return responseCode;
        }
    }

    /*
	 * @author : Ilham
	 * @Since  : 20191128
	 * @category patch 
	 *    - set default 19000101 jika ada disbursmentdate ap3r null  
	 */ 
    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_DETAIL_AP3R_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    AP3RDetailResponse getDetailAp3r(@RequestBody GetDetailAP3RRequest request,
                                     @PathVariable("apkVersion") String apkVersion) throws Exception {

        final AP3RDetailResponse responseCode = new AP3RDetailResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        responseCode.setResponseMessage("SUKSES");
        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        try {
            log.info("getDetailAp3r INCOMING MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(),
                    request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                log.info("START MAPPING DATA AP3R");
                User user = userService.findUserByUsername(username);
                if (user.getRoleUser() == null) {
                    responseCode.setResponseCode(WebGuiConstant.ROLE_NULL);
                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(label);
                    return responseCode;
                }
                AP3R ap3r = ap3rService.findByLocalId(request.getAp3rId());
                AP3RList ap3rMap = new AP3RList();
                ap3rMap.setAp3rId(ap3r.getLocalId());
                if (ap3r.getSwId() != null || ap3r.getSwProductId() != null) {
                    SurveyWawancara swLocalId = swService.getSWById(ap3r.getSwId().toString());
                    if (swLocalId != null) {
                        ap3rMap.setSwId(swLocalId.getLocalId());
                        ListSWResponse swPojo = new ListSWResponse();
                        swPojo.setSwId(FieldValidationUtil.stringRequestValueValidation(swLocalId.getLocalId()));
                        swPojo.setDayLight(swLocalId.getTime());
                        swPojo.setSwLocation(swLocalId.getSwLocation());
                        // get wisma SW
                        WismaSWReq wisma = new WismaSWReq();
                        Location wismaSw = areaService.findLocationById(swLocalId.getWismaId());
                        if (wismaSw != null) {
                            wisma.setOfficeId(wismaSw.getLocationId());
                            wisma.setOfficeCode(wismaSw.getLocationCode());
                            wisma.setOfficeName(wismaSw.getName());
                            swPojo.setWisma(wisma);
                        }
                        log.info("Finish get wisma");
                        swPojo.setCustomerType(FieldValidationUtil
                                .charValueResponseValidation(swLocalId.getCustomerRegistrationType()));
                        if (swLocalId.getSurveyDate() != null)
                            swPojo.setSurveyDate(formatDateTime.format(swLocalId.getSurveyDate()));
                        // get list produk pembiayaan SW
                        List<SWProductListResponse> swProductList = new ArrayList<>();
                        List<SWProductMapping> productList = swService.findProductMapBySwId(swLocalId.getSwId());
                        if (productList != null) {
                            for (SWProductMapping product : productList) {
                                SWProductListResponse swProduct = new SWProductListResponse();
                                swProduct.setSwProductId(
                                        FieldValidationUtil.stringRequestValueValidation(product.getLocalId()));
                                swProduct.setProductId(product.getProductId().getProductId().toString());
                                swProduct.setSelectedPlafon(
                                        FieldValidationUtil.bigDecimalValueResponseValidation(product.getPlafon()));
                                swProduct.setRecommendationProductId(product.getProductId().getProductId().toString());
                                swProduct.setRecommendationSelectedPlafon(FieldValidationUtil
                                        .bigDecimalValueResponseValidation(product.getRecommendedPlafon()));
                                swProductList.add(swProduct);
                            }
                            swPojo.setSwProducts(swProductList);
                        }
                        log.info("Finish get Product");
                        // get profil SW
                        SWProfileResponse profile = new SWProfileResponse();
                        if (swLocalId.getCustomerId() != null) {
                            swPojo.setCustomerId(swLocalId.getCustomerId().toString());
                            Customer customer = customerService.findById(swLocalId.getCustomerId().toString());
                            profile.setLongName(customer.getCustomerName());
                            swPojo.setCustomerCif(customer.getCustomerCifNumber());
                            swPojo.setSentraId(customer.getGroup().getSentra().getSentraId().toString());
                            swPojo.setSentraName(customer.getGroup().getSentra().getSentraName());
                            swPojo.setGroupId(customer.getGroup().getGroupId().toString());
                            swPojo.setGroupName(customer.getGroup().getGroupName());
                            swPojo.setCustomerExists("true");
                            Loan loan = loanService.findByAp3rAndCustomer(ap3r.getAp3rId(), customer);
                            if (loan != null) {
                                swPojo.setLoanExists("true");
                            } else {
                                swPojo.setLoanExists("false");
                            }
                        } else {
                            swPojo.setCustomerExists("false");
                        }
                        swPojo.setIdCardNumber(swLocalId.getCustomerIdNumber());
                        profile.setAlias(swLocalId.getCustomerAliasName());
                        swPojo.setIdCardName(swLocalId.getCustomerIdName());
                        profile.setGender(FieldValidationUtil.stringToZeroValidation(swLocalId.getGender()));
                        profile.setReligion(FieldValidationUtil.stringToZeroValidation(swLocalId.getReligion()));
                        profile.setBirthPlace(swLocalId.getBirthPlace());
                        profile.setBirthPlaceRegencyName(swLocalId.getBirthPlaceRegencyName());
                        if (swLocalId.getBirthDate() != null)
                            profile.setBirthDay(formatter.format(swLocalId.getBirthDate()));
                        if (swLocalId.getIdExpiryDate() != null) {
                            profile.setIdCardExpired(formatter.format(swLocalId.getIdExpiryDate()));
                        } else {
                            profile.setIdCardExpired("");
                        }
                        profile.setMarriedStatus(
                                FieldValidationUtil.stringToZeroValidation(swLocalId.getMaritalStatus()));
                        profile.setLongName(swLocalId.getCustomerName());
                        profile.setWorkStatus(FieldValidationUtil.stringToZeroValidation(swLocalId.getWorkType()));
                        if (swLocalId.getCreatedDate() != null)
                            swPojo.setCreatedAt(formatter.format(swLocalId.getCreatedDate()));
                        profile.setPhoneNumber(swLocalId.getPhoneNumber());
                        profile.setNpwp(swLocalId.getNpwp());
                        profile.setMotherName(swLocalId.getMotherName());
                        profile.setDependants(
                                FieldValidationUtil.integerValueResponseValidation(swLocalId.getDependentCount()));
                        profile.setEducation(FieldValidationUtil.stringToZeroValidation(swLocalId.getEducation()));
                        profile.setIsLifeTime(
                                FieldValidationUtil.booleanResponseValueValidation(swLocalId.getLifeTime()));
                        swPojo.setProfile(profile);
                        // get alamat SW
                        SwAddressResponse address = new SwAddressResponse();
                        if (swLocalId.getSwLocation() != null)
                            address.setName(swLocalId.getSwLocation());
                        else
                            address.setName(swLocalId.getAddress());
                        address.setStreet(swLocalId.getRtrw());
                        address.setPostCode(swLocalId.getPostalCode());
                        address.setVillageId(FieldValidationUtil.stringToZeroValidation(swLocalId.getKelurahan()));
                        address.setRegencyId(FieldValidationUtil.stringToZeroValidation(swLocalId.getKecamatan()));
                        address.setProvinceId(FieldValidationUtil.stringToZeroValidation(swLocalId.getProvince()));
                        address.setDistrictId(FieldValidationUtil.stringToZeroValidation(swLocalId.getCity()));
                        address.setAdditionalAddress(swLocalId.getDomisiliAddress());
                        address.setPlaceOwnerShip(swLocalId.getHouseType());
                        address.setPlaceCertificate(
                                FieldValidationUtil.charValueResponseValidation(swLocalId.getHouseCertificate()));
                        swPojo.setAddress(address);
                        // get pasangan SW
                        SWPartnerResponse partner = new SWPartnerResponse();
                        partner.setName(swLocalId.getCoupleName());
                        partner.setBirthPlace(swLocalId.getCoupleBirthPlaceRegencyName());
                        partner.setBirthPlaceId(swLocalId.getCoupleBirthPlace());
                        if (swLocalId.getCoupleBirthDate() != null)
                            partner.setBirthDay(formatter.format(swLocalId.getCoupleBirthDate()));
                        if (swLocalId.getCoupleJob() != null) {
                            if (swLocalId.getCoupleJob().equals('K')) {
                                partner.setJob("Karyawan");
                            } else if (swLocalId.getCoupleJob().equals('W')) {
                                partner.setJob("Wiraswasta");
                            } else if (swLocalId.getCoupleJob().equals('M')) {
                                partner.setJob("Musiman");
                            } else {
                                partner.setJob("");
                            }
                        } else {
                            partner.setJob("");
                        }
                        swPojo.setPartner(partner);
                        // get usaha SW
                        SWBusinessResponse business = new SWBusinessResponse();
                        business.setType(swLocalId.getBusinessField());
                        business.setDesc(swLocalId.getBusinessDescription());
                        business.setBusinessOwnership(
                                FieldValidationUtil.charValueResponseValidation(swLocalId.getBusinessOwnerStatus()));
                        business.setBusinessShariaCompliance(
                                FieldValidationUtil.charValueResponseValidation(swLocalId.getBusinessShariaType()));
                        business.setBusinessLocation(
                                FieldValidationUtil.charValueResponseValidation(swLocalId.getBusinessLocation()));
                        business.setName(swLocalId.getBusinessName());
                        business.setAddress(swLocalId.getBusinessAddress());
                        business.setAgeYear(
                                FieldValidationUtil.integerValueResponseValidation(swLocalId.getBusinessAgeYear()));
                        business.setAgeMonth(
                                FieldValidationUtil.integerValueResponseValidation(swLocalId.getBusinessAgeMonth()));
                        business.setBusinessRunner(
                                FieldValidationUtil.charValueResponseValidation(swLocalId.getBusinessWorkStatus()));
                        swPojo.setBusiness(business);
                        // get parameter kalkulasi
                        SWCalculationVariable calcVariable = new SWCalculationVariable();
                        calcVariable.setJenisSiklus(
                                FieldValidationUtil.charValueResponseValidation(swLocalId.getBusinessCycle()));
                        calcVariable.setHariOperasional(swLocalId.getBusinessDaysOperation());
                        calcVariable.setPendapatanRamai(FieldValidationUtil
                                .bigDecimalValueResponseValidation(swLocalId.getOneDayBusyIncome()).toString());
                        calcVariable.setJumlahhariramai(FieldValidationUtil
                                .integerValueResponseValidation(swLocalId.getTotalDaysInMonthBusy()));
                        calcVariable.setPendapatanSepi(FieldValidationUtil
                                .bigDecimalValueResponseValidation(swLocalId.getOneDayLessIncome()).toString());
                        calcVariable.setJumlahharisepi(FieldValidationUtil
                                .integerValueResponseValidation(swLocalId.getTotalDaysInMonthLess()));
                        calcVariable.setPendapatanPerbulanRamaiSepi(FieldValidationUtil
                                .bigDecimalValueResponseValidation(swLocalId.getTotalLessBusyIncome()).toString());
                        if (swLocalId.getFirstDayIncome() != null)
                            calcVariable.setPeriode1(swLocalId.getFirstDayIncome().toString());
                        if (swLocalId.getSecondDayIncome() != null)
                            calcVariable.setPeriode2(swLocalId.getSecondDayIncome().toString());
                        if (swLocalId.getThirdDayIncome() != null)
                            calcVariable.setPeriode3(swLocalId.getThirdDayIncome().toString());
                        calcVariable.setPendapatanPerbulan3Periode(FieldValidationUtil
                                .bigDecimalValueResponseValidation(swLocalId.getThreePeriodIncome()).toString());
                        calcVariable.setPendapatanRata2TigaPeriode(FieldValidationUtil
                                .bigDecimalValueResponseValidation(swLocalId.getAvgThreePeriodIncome()).toString());
                        if (swLocalId.getOpenTime() != null)
                            calcVariable.setJamBuka(formatTime.format(swLocalId.getOpenTime()));
                        if (swLocalId.getCloseTime() != null)
                            calcVariable.setJamTutup(formatTime.format(swLocalId.getCloseTime()));
                        calcVariable.setWaktuKerja(
                                FieldValidationUtil.integerValueResponseValidation(swLocalId.getWorkTime()));
                        calcVariable.setWaktukerjaSetelahbuka(
                                FieldValidationUtil.integerValueResponseValidation(swLocalId.getWorkTimeAfterOpen()));
                        if (swLocalId.getCashOpenTime() != null)
                            calcVariable.setUangkasBoxdiBuka(swLocalId.getCashOpenTime().toString());
                        if (swLocalId.getCashCurrentTime() != null)
                            calcVariable.setUangKasBoxSekarang(swLocalId.getCashCurrentTime().toString());
                        if (swLocalId.getCashExpense() != null)
                            calcVariable.setUangBelanja(swLocalId.getCashExpense().toString());
                        calcVariable.setPendapatanPerbulanPerkiraanSehari(FieldValidationUtil
                                .bigDecimalValueResponseValidation(swLocalId.getOneDayPredictIncome()).toString());
                        calcVariable.setPendapatanPerbulanKasSehari(FieldValidationUtil
                                .bigDecimalValueResponseValidation(swLocalId.getOneDayCashIncome()).toString());
                        List<OtherItemCalculation> itemPendapatanLainList = swService
                                .findOtherItemCalcBySwId(swLocalId.getSwId());
                        List<ItemCalcLainReq> itemCalcLainList = new ArrayList<>();
                        for (OtherItemCalculation itemPendapatanLain : itemPendapatanLainList) {
                            ItemCalcLainReq itemCalcLain = new ItemCalcLainReq();
                            itemCalcLain.setName(itemPendapatanLain.getName());
                            itemCalcLain.setAmount(FieldValidationUtil
                                    .bigDecimalValueResponseValidation(itemPendapatanLain.getAmount()).toString());
                            if (itemPendapatanLain.getPeriode() != null)
                                itemCalcLain.setPeriode(itemPendapatanLain.getPeriode().toString());
                            else
                                itemCalcLain.setPeriode("0");
                            itemCalcLain.setPendapatanPerbulan(FieldValidationUtil
                                    .bigDecimalValueResponseValidation(itemPendapatanLain.getPendapatanPerbulan())
                                    .toString());
                            itemCalcLainList.add(itemCalcLain);
                        }
                        calcVariable.setPendapatanLainnya(FieldValidationUtil
                                .bigDecimalValueResponseValidation(swLocalId.getOtherIncome()).toString());
                        calcVariable.setItemCalcLains(itemCalcLainList);
                        swPojo.setCreatedBy(swLocalId.getCreatedBy());
                        swPojo.setCalcVariable(calcVariable);
                        List<DirectBuyList> directBuyList = new ArrayList<>();
                        List<DirectBuyThings> directBuyProductList = swService.findProductBySwId(swLocalId);
                        for (DirectBuyThings directBuyProduct : directBuyProductList) {
                            DirectBuyList product = new DirectBuyList();
                            product.setFrequency(FieldValidationUtil
                                    .integerValueResponseValidation(directBuyProduct.getFrequency()));
                            product.setIndex(
                                    FieldValidationUtil.integerValueResponseValidation(directBuyProduct.getIndex()));
                            product.setItemName(directBuyProduct.getNamaBarang());
                            product.setPrice(
                                    FieldValidationUtil.bigDecimalValueResponseValidation(directBuyProduct.getPrice()));
                            product.setSellingPrice(FieldValidationUtil
                                    .bigDecimalValueResponseValidation(directBuyProduct.getSellingPrice()));
                            product.setTotalItem(directBuyProduct.getTotalItem().toString());
                            product.setType(
                                    FieldValidationUtil.integerValueResponseValidation(directBuyProduct.getType()));
                            directBuyList.add(product);
                        }
                        swPojo.setDirectPurchasing(directBuyList);
                        List<AWGMBuyList> awgmBuyList = new ArrayList<>();
                        List<AWGMBuyThings> awgmBuyProductList = swService.findAwgmProductBySwId(swLocalId);
                        for (AWGMBuyThings awgmBuyProduct : awgmBuyProductList) {
                            AWGMBuyList product = new AWGMBuyList();
                            product.setFrequency(
                                    FieldValidationUtil.integerValueResponseValidation(awgmBuyProduct.getFrequency()));
                            product.setIndex(
                                    FieldValidationUtil.integerValueResponseValidation(awgmBuyProduct.getIndex()));
                            product.setItemName(awgmBuyProduct.getNamaBarang());
                            product.setPrice(FieldValidationUtil
                                    .bigDecimalValueResponseValidation(awgmBuyProduct.getBuyPrice()));
                            product.setSellingPrice(FieldValidationUtil
                                    .bigDecimalValueResponseValidation(awgmBuyProduct.getSellPrice()));
                            product.setTotalItem(awgmBuyProduct.getTotal().toString());
                            product.setType(
                                    FieldValidationUtil.integerValueResponseValidation(awgmBuyProduct.getType()));
                            awgmBuyList.add(product);
                        }
                        swPojo.setAwgmPurchasing(awgmBuyList);
                        SWExpenseResponse expense = new SWExpenseResponse();
                        if (swLocalId.getTransportCost() != null)
                            expense.setTransportUsaha(swLocalId.getTransportCost().toString());
                        if (swLocalId.getUtilityCost() != null)
                            expense.setUtilitasUsaha(swLocalId.getUtilityCost().toString());
                        if (swLocalId.getStaffSalary() != null)
                            expense.setGajiUsaha(swLocalId.getStaffSalary().toString());
                        if (swLocalId.getRentCost() != null)
                            expense.setSewaUsaha(swLocalId.getRentCost().toString());
                        if (swLocalId.getPrivateTransportCost() != null)
                            expense.setTransportNonUsaha(swLocalId.getPrivateTransportCost().toString());
                        if (swLocalId.getPrivateUtilityCost() != null)
                            expense.setUtitlitasNonUsaha(swLocalId.getPrivateUtilityCost().toString());
                        if (swLocalId.getEducationCost() != null)
                            expense.setPendidikanNonUsaha(swLocalId.getEducationCost().toString());
                        if (swLocalId.getHealthCost() != null)
                            expense.setKesehatanNonUsaha(swLocalId.getHealthCost().toString());
                        if (swLocalId.getFoodCost() != null)
                            expense.setMakanNonUsaha(swLocalId.getFoodCost().toString());
                        if (swLocalId.getInstallmentCost() != null)
                            expense.setAngsuranNonUsaha(swLocalId.getInstallmentCost().toString());
                        if (swLocalId.getOtherCost() != null)
                            expense.setLainlainNonUsaha(swLocalId.getOtherCost().toString());
                        swPojo.setExpense(expense);
                        List<ReferenceList> referenceList = new ArrayList<>();
                        List<NeighborRecommendation> neighborList = swService.findNeighborBySwId(swLocalId);
                        for (NeighborRecommendation neighbor : neighborList) {
                            ReferenceList reference = new ReferenceList();
                            reference.setName(neighbor.getNeighborName());
                            reference.setAddress(neighbor.getNeighborAddress());
                            reference.setGoodNeighbour("" + neighbor.getNeighborRelation());
                            if (reference.getGoodNeighbour().equals("t")) {
                                reference.setGoodNeighbour("true");
                            } else {
                                reference.setGoodNeighbour("false");
                            }
                            reference.setVisitedByLandshark("" + neighbor.getNeighborOutstanding());
                            if (reference.getVisitedByLandshark().equals("t")) {
                                reference.setVisitedByLandshark("true");
                            } else {
                                reference.setVisitedByLandshark("false");
                            }
                            reference.setHaveBusiness("" + neighbor.getNeighborBusiness());
                            if (reference.getHaveBusiness().equals("t")) {
                                reference.setHaveBusiness("true");
                            } else {
                                reference.setHaveBusiness("false");
                            }
                            reference.setRecommended("" + neighbor.getNeighborRecomend());
                            if (reference.getRecommended().equals("t")) {
                                reference.setRecommended("true");
                            } else {
                                reference.setRecommended("false");
                            }
                            referenceList.add(reference);
                        }
                        swPojo.setReferenceList(referenceList);
                        swPojo.setStatus(swLocalId.getStatus());
                        if (swLocalId.getPmId() != null)
                            swPojo.setPmId(swLocalId.getPmId().getPmId().toString());
                        List<SWApprovalHistory> swApprovalList = new ArrayList<>();
                        List<SWUserBWMPMapping> swUserBwmpList = swService.findBySw(swLocalId.getSwId());
                        for (SWUserBWMPMapping swUserBwmp : swUserBwmpList) {
                            SWApprovalHistory swApproval = new SWApprovalHistory();
                            User userApproval = userService.loadUserByUserId(swUserBwmp.getUserId());
                            if (userApproval != null) {
                                swApproval.setLevel(swUserBwmp.getLevel());
                                swApproval
                                        .setLimit(FieldValidationUtil.stringToZeroValidation(userApproval.getLimit()));
                                swApproval.setRole(userApproval.getRoleName());
                                swApproval.setName(userApproval.getName());
                                swApproval.setDate(swUserBwmp.getDate());
                                swApproval.setTanggal(formatDateTime.format(swUserBwmp.getCreatedDate()));
                                swApproval.setStatus(swUserBwmp.getStatus());
                                swApproval.setSupervisorId(userApproval.getUserId().toString());
                                swApprovalList.add(swApproval);
                            }
                        }
                        swPojo.setApprovalHistories(swApprovalList);
                        SwIdPhoto swIdPhoto = swService.getSwIdPhoto(swLocalId.getSwId().toString());
                        if (swIdPhoto != null)
                            swPojo.setHasIdPhoto("true");
                        else
                            swPojo.setHasIdPhoto("false");
                        SwSurveyPhoto swSurveyPhoto = swService.getSwSurveyPhoto(swLocalId.getSwId().toString());
                        if (swSurveyPhoto != null)
                            swPojo.setHasBusinessPlacePhoto("true");
                        else
                            swPojo.setHasBusinessPlacePhoto("false");
                        if (swLocalId.getHasApprovedMs() != null) {
                            if (swLocalId.getHasApprovedMs().equals(true)) {
                                swPojo.setHasApprovedMs("true");
                            } else {
                                swPojo.setHasApprovedMs("false");
                            }
                        } else {
                            swPojo.setHasApprovedMs("false");
                        }
                        ap3rMap.setSw(swPojo);
                    }
                    SWProductMapping swProductMapLocal = swService.findProductMapById(ap3r.getSwProductId());
                    if (swProductMapLocal != null) {
                        ap3rMap.setSwProductId(swProductMapLocal.getLocalId());
                    }
                }
                ap3rMap.setHaveAccount("" + ap3r.getHasSaving());
                if (ap3rMap.getHaveAccount().equals("t")) {
                    ap3rMap.setHaveAccount("true");
                } else {
                    ap3rMap.setHaveAccount("false");
                }
                LoanDeviation loanDeviation = loanService.findDeviationByAp3rId(ap3r.getAp3rId());
                if (loanDeviation != null) {
                    if (loanDeviation.getStatus() != null) {
                        if (loanDeviation.getStatus().equals(WebGuiConstant.STATUS_WAITING_APPROVAL)
                                || loanDeviation.getStatus().equals(WebGuiConstant.STATUS_APPROVED)) {
                            ap3rMap.setDeviationExists("true");
                        } else {
                            ap3rMap.setDeviationExists("false");
                        }
                    } else {
                        ap3rMap.setDeviationExists("false");
                    }
                } else {
                    ap3rMap.setDeviationExists("false");
                }
                ap3rMap.setAccountPurpose(ap3r.getOpenSavingReason());
                ap3rMap.setFundSource(ap3r.getFundSource());
                ap3rMap.setYearlyTransaction("" + ap3r.getTransactionInYear());
                ap3rMap.setFinancingPurpose(ap3r.getLoanReason());
                ap3rMap.setBusinessTypeId(ap3r.getBusinessField());
                List<FundedThingsReq> productMapList = new ArrayList<>();
                List<FundedThings> productList = ap3rService.findByAp3rId(ap3r);
                for (FundedThings product : productList) {
                    FundedThingsReq productMap = new FundedThingsReq();
                    productMap.setName(product.getProductName());
                    productMap.setPrice(product.getPrice().toString());
                    productMapList.add(productMap);
                }
                ap3rMap.setFinancedGoods(productMapList);
                /*
                 * validasi disbursmentdate null
                 */
                if(ap3r.getDisbursementDate() != null) {
                    ap3rMap.setDisburseDate(formatter.format(ap3r.getDisbursementDate()));                    	
                }
                else {
                	ap3rMap.setDisburseDate("1900-01-01 00:00:00");
                }
                ap3rMap.setHighRiskCustomer("" + ap3r.getHighRiskCustomer());
                if (ap3rMap.getHighRiskCustomer().equals("t")) {
                    ap3rMap.setHighRiskCustomer("true");
                } else {
                    ap3rMap.setHighRiskCustomer("false");
                }
                ap3rMap.setHighRiskBusiness("" + ap3r.getHighRiskBusiness());
                if (ap3rMap.getHighRiskBusiness().equals("t")) {
                    ap3rMap.setHighRiskBusiness("true");
                } else {
                    ap3rMap.setHighRiskBusiness("false");
                }
                ap3rMap.setStatus(ap3r.getStatus());
                ap3rMap.setCreatedBy(ap3r.getCreatedBy());
                if (ap3r.getDueDate() != null)
                    ap3rMap.setDueDate(formatter.format(ap3r.getDueDate()));
                if (ap3r.getRejectedReason() != null)
                    ap3rMap.setRejectionReason(ap3r.getRejectedReason());
                else
                    ap3rMap.setRejectionReason("");
                if (ap3r.getApprovalStatus() != null)
                    ap3rMap.setApprovalStatus(ap3r.getApprovalStatus().toString());
                else
                    ap3rMap.setApprovalStatus("0");
                if (ap3r.getCreatedDate() != null)
                    ap3rMap.setCreatedAt(formatter.format(ap3r.getCreatedDate()));
                else
                    ap3rMap.setCreatedAt("");
                if (ap3r.getUpdatedDate() != null)
                    ap3rMap.setUpdatedAt(formatter.format(ap3r.getUpdatedDate()));
                else
                    ap3rMap.setUpdatedAt("");
                responseCode.setAp3rList(ap3rMap);
                log.info("FINISH MAPPING DATA AP3R");
            }
        } catch (Exception e) {
            log.error("getDetailAp3r error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("getDetailAp3r error: " + e.getMessage());
        } finally {
            try {
                log.trace("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_AP3RDetail);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("getDetailAp3r RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("getDetailAp3r saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return responseCode;
        }
    }

}