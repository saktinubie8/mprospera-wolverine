package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.sda.*;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.request.SyncAreaRequest;
import id.co.telkomsigma.btpns.mprospera.response.AreaDistrictResponse;
import id.co.telkomsigma.btpns.mprospera.response.AreaKecamatanResponse;
import id.co.telkomsigma.btpns.mprospera.response.AreaProvinceResponse;
import id.co.telkomsigma.btpns.mprospera.response.SyncAreaResponse;
import id.co.telkomsigma.btpns.mprospera.service.SyncAreaService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalActivityService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalService;
import id.co.telkomsigma.btpns.mprospera.service.WSValidationService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

@Controller("webserviceSyncAreaController")
public class WebserviceSyncAreaController extends GenericController {
    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private SyncAreaService syncAreaService;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    JsonUtils jsonUtils = new JsonUtils();

   /* @RequestMapping(value = WebGuiConstant.TERMINAL_SYNC_AREA_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    SyncAreaResponse addSda(@RequestBody final SyncAreaRequest request,
                            @PathVariable("apkVersion") String apkVersion) {
        log.trace("Try to processing SYNC AREA request");
        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();

        final SyncAreaResponse responseCode = new SyncAreaResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);

        log.trace("Validating Request");
        try {
            try {
                log.info("syncArea INCOMING MESSAGE : " + jsonUtils.toJson(request));
            } catch (Exception e) {
            }
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);

            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.trace("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {


                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);

                log.trace("Validation success, get area data");

                AreaDistrict wismaUser = syncAreaService.getAreaDistrictByUsername(request.getUsername());

                if (wismaUser == null) {
                    responseCode.setResponseCode(WebGuiConstant.RC_LOCATION_ID_NOT_FOUND);
                    String labelWismaUser = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(labelWismaUser);
                    return responseCode;
                }
                List<AreaProvinceNeigbour> provinceList = syncAreaService
                        .getAreaProvinceNearby(wismaUser.getParentAreaId());

                log.trace("get Province data");
                List<AreaProvinceResponse> provinceListResponse = new ArrayList<>();

                for (AreaProvinceNeigbour areaProvinceNeigbour : provinceList) {
                    AreaProvince areaProvince = syncAreaService
                            .getAreaProvince(areaProvinceNeigbour.getAreaProvinceNeighbourId());
                    AreaProvinceResponse areaProvinceResponse = new AreaProvinceResponse();
                    areaProvinceResponse.setAreaId(areaProvince.getAreaId());
                    areaProvinceResponse.setAreaName(areaProvince.getAreaName());
                    if (areaProvince.getPostalCode() != null) {
                        areaProvinceResponse.setPostalCode(areaProvince.getPostalCode());
                    } else {
                        areaProvinceResponse.setPostalCode("");
                    }

                    List<AreaDistrictResponse> districtListResponse = new ArrayList<>();
                    List<AreaDistrict> districtList = syncAreaService.getDistrictByParentId(areaProvince.getAreaId());
                    for (AreaDistrict areaDistrictList : districtList) {
                        AreaDistrictResponse areaDistrict = new AreaDistrictResponse();
                        areaDistrict.setAreaId(areaDistrictList.getAreaId());
                        areaDistrict.setAreaName(areaDistrictList.getAreaName());
                        if (areaDistrictList.getParentAreaId() != null) {
                            areaDistrict.setParentAreaId(areaDistrictList.getParentAreaId());
                        } else {
                            areaDistrict.setParentAreaId("");
                        }
                        if (areaDistrictList.getPostalCode() != null) {
                            areaDistrict.setPostalCode(areaDistrictList.getPostalCode());
                        } else {
                            areaDistrict.setPostalCode("");
                        }

                        List<AreaKecamatanResponse> kecamatanListResponse = new ArrayList<>();
                        List<AreaSubDistrict> kecamatanList = syncAreaService
                                .getAreaSubDistrictByDistrictId(areaDistrictList.getAreaId());
                        for (AreaSubDistrict sub : kecamatanList) {
                            AreaKecamatanResponse kec = new AreaKecamatanResponse();
                            kec.setAreaId(sub.getAreaId());
                            kec.setAreaName(sub.getAreaName());
                            kec.setParentAreaId(sub.getParentAreaId());
                            if (sub.getPostalCode() != null) {
                                kec.setPostalCode(sub.getPostalCode());
                            } else {
                                kec.setPostalCode("");
                            }
                            syncAreaService.getAreaKelurahanByParentId(kec.getAreaId(), kec.getAreaListKelurahan());
                            kecamatanListResponse.add(kec);
                        }
                        areaDistrict.setAreaListKecamatan(kecamatanListResponse);
                        districtListResponse.add(areaDistrict);
                    }
                    areaProvinceResponse.setAreaListKota(districtListResponse);
                    provinceListResponse.add(areaProvinceResponse);
                }

                responseCode.setAreaListProvince(provinceListResponse);

                log.trace("Finished set Data Area");
            }
        } catch (Exception e) {
            log.error("syncArea error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("syncArea error: " + e.getMessage());
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());

                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_syncArea);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());

                        List<MessageLogs> messageLogs = new ArrayList<>();

                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);

                    }
                });

                log.info("Finishing create Terminal Activity");
                log.info("syncArea RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("syncArea saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return responseCode;
        }

    }*/
}
