package id.co.telkomsigma.btpns.mprospera.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class PercentageSynchronizer {

    static final Log LOGGER = LogFactory.getLog(PercentageSynchronizer.class);

    public static String processSyncPercent(String input, String totalPage, String data) {

        LOGGER.trace("PERCENTAGE SYNC PROSES...");
        LOGGER.trace("INPUT      : " + input);
        LOGGER.trace("TOTAL PAGE : " + totalPage);
        LOGGER.trace("DATA       : " + data);
        if ("".equals(totalPage) || totalPage == null || "".equals(input) || input == null)
            return "";
        if (totalPage.equals("0"))
            return "100|" + totalPage + "|" + input;
        if ("".equals(data) || data == null)
            return getPercent(1L, Long.parseLong(totalPage)) + "|" + totalPage + "|" + input;
        String[] fragments = data.split("\\|");
        String percentage = fragments[0];
        String pageTemp = fragments[1];
        List<String> pages = new ArrayList<String>();
        if (!totalPage.equals(pageTemp))
            return "";
        pages = Arrays.asList(fragments[2].split(","));
        if (pages.contains(input))
            return data;
        else {
            return getPercent((long) (pages.size() + 1), Long.parseLong(totalPage)) + "|" + totalPage + "|"
                    + fragments[2] + "," + input;
        }
    }

    protected static Long getPercent(Long size, Long count) {
        return 100 * size / count;
    }

}