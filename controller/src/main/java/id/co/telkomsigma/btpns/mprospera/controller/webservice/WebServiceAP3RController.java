package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanDeviation;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.sw.*;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.request.AP3RRequest;
import id.co.telkomsigma.btpns.mprospera.request.BusinessTypeListRequest;
import id.co.telkomsigma.btpns.mprospera.request.FundedThingsReq;
import id.co.telkomsigma.btpns.mprospera.response.*;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.PercentageSynchronizer;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller("webserviceAP3RController")
public class WebServiceAP3RController extends GenericController {

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private AuditLogService auditLogService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private SWService swService;

    @Autowired
    private AP3RService ap3rService;

    @Autowired
    private LoanService loanService;

    @Autowired
    private UserService userService;

    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
    JsonUtils jsonUtils = new JsonUtils();

    // SUBMIT AP3R DATA API
    @RequestMapping(value = WebGuiConstant.AP3R_SUBMIT_DATA_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    AP3RResponse doAdd(@RequestBody AP3RRequest request,
                       @PathVariable("apkVersion") String apkVersion) throws Exception {

        final AP3RResponse responseCode = new AP3RResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        String ap3rId = "";
        try {
            log.info("addAp3r INCOMING MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(),
                    request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei()
                        + " ,sessionKey : " + request.getSessionKey());
            } else {
                if (request.getAp3rId() != null) {
                    if (!"".equals(request.getAp3rId())) {
                        AP3R ap3r = ap3rService.findByLocalId(request.getAp3rId());
                        if (ap3r != null) {
                            if (request.getAction().equals("insert")) {
                                responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
                                log.error("DATA DUPLICATE");
                                String label = getMessage("webservice.rc.label." + WebGuiConstant.AP3R_ALREADY_EXIST);
                                responseCode.setResponseMessage(label);
                                responseCode.setAp3rId(request.getAp3rId());
                                responseCode.setLocalId(request.getLocalId());
                                responseCode.setStatus(request.getStatus());
                                return responseCode;
                            }
                            // validasi user delete data AP3R jika AP3R tersebut sudah di approved
                            if ("delete".equals(request.getAction()) && ap3r.getStatus()
                                    .equals(WebGuiConstant.STATUS_APPROVED)) {
                                log.error("STATUS AP3R SUDAH APPROVED");
                                responseCode.setResponseCode(WebGuiConstant.RC_ALREADY_APPROVED);
                                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                                responseCode.setResponseMessage(label);
                                return responseCode;
                            }
                        } else {
                            log.info("SET AP3R DATA");
                            ap3r = new AP3R();
                        }
                        ap3r.setLatitude(request.getLatitude());
                        ap3r.setLongitude(request.getLongitude());
                        if (request.getSwProductId() != null) {
                            if (!request.getSwProductId().equals("")) {
                                SWProductMapping swProductMapLocal = swService
                                        .findProductMapByLocalId(request.getSwProductId());
                                if (swProductMapLocal == null) {
                                    log.error("PRODUCT SW NULL");
                                    responseCode.setResponseCode(WebGuiConstant.SW_PRODUCT_NULL);
                                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                                    responseCode.setResponseMessage(label);
                                    return responseCode;
                                }
                                ap3r.setSwId(swProductMapLocal.getSwId());
                                ap3r.setSwProductId(swProductMapLocal.getMappingId());
                            }
                        }
                        ap3r.setLocalId(request.getAp3rId());
                        if (request.getHaveAccount() != null)
                            if (!request.getHaveAccount().equals(""))
                                ap3r.setHasSaving(request.getHaveAccount().charAt(0));// FLAG MEMILIKI TABUNGAN ATAU TIDAK
                        ap3r.setOpenSavingReason(request.getAccountPurpose());// TUJUAN BUKA TABUNGAN
                        if (request.getFundSource() != null)
                            if (!request.getFundSource().equals(""))
                                ap3r.setFundSource(request.getFundSource());// SUMBER DANA
                        if (request.getYearlyTransaction() != null)
                            if (!request.getYearlyTransaction().equals(""))
                                ap3r.setTransactionInYear(request.getYearlyTransaction().charAt(0));// TRANSAKSI SETAHUN
                        if (request.getFinancingPurpose() != null)
                            if (!request.getFinancingPurpose().equals(""))
                                ap3r.setLoanReason(request.getFinancingPurpose());// TUJUAN PEMBIAYAAN
                        ap3r.setBusinessField(request.getBusinessTypeId());// JENIS USAHA
                        if (request.getDisburseDate() != null)
                            if (!request.getDisburseDate().equals(""))
                                ap3r.setDisbursementDate(formatter.parse(request.getDisburseDate()));// TANGGAL CAIR
                        if (request.getHighRiskCustomer() != null)
                            if (!request.getHighRiskCustomer().equals(""))
                                ap3r.setHighRiskCustomer(request.getHighRiskCustomer().charAt(0));// HIGH RISK CUSTOMER
                        if (request.getHighRiskBusiness() != null)
                            if (!request.getHighRiskBusiness().equals(""))
                                ap3r.setHighRiskBusiness(request.getHighRiskBusiness().charAt(0));// HIGH RISK BUSINESS
                        ap3r.setStatus(WebGuiConstant.STATUS_WAITING_APPROVAL);
                        ap3r.setCreatedBy(request.getUsername());
                        ap3r.setCreatedDate(new Date());
                        if (request.getDueDate() != null)
                            if (!request.getDueDate().equals(""))
                                ap3r.setDueDate(formatter.parse(request.getDueDate()));// TANGGAL JATUH TEMPO
                        if ("delete".equals(request.getAction()) || "update".equals(request.getAction())) {
                            if (request.getAction().equals("delete")) {
                                List<FundedThings> fundedThings = ap3rService.findByAp3rId(ap3r);
                                for (FundedThings fundThing : fundedThings) {
                                    ap3rService.delete(fundThing);
                                }
                                ap3r.setIsDeleted(true);
                                ap3r.setUpdatedDate(new Date());
                                ap3r.setUpdatedBy(request.getUsername());
                            }
                            ap3r.setUpdatedDate(new Date());
                            ap3r.setUpdatedBy(request.getUsername());
                        }
                        ap3rService.save(ap3r);
                        ap3rId = ap3r.getLocalId();
                        if (request.getAction().equals("insert")) {
                            for (FundedThingsReq product : request.getFinancedGoods()) {
                                FundedThings productAdd = new FundedThings();
                                productAdd.setProductName(product.getName());
                                if (product.getPrice() != null || product.getPrice().equals(""))
                                    productAdd.setPrice(new BigDecimal(product.getPrice()));
                                productAdd.setAp3rId(ap3r);
                                ap3rService.save(productAdd);
                            }
                        }
                        if (request.getAction().equals("update")) {
                            List<FundedThings> fundedThings = ap3rService.findByAp3rId(ap3r);
                            for (FundedThings fundThing : fundedThings) {
                                ap3rService.delete(fundThing);
                            }
                            for (FundedThingsReq product : request.getFinancedGoods()) {
                                FundedThings productAdd = new FundedThings();
                                productAdd.setProductName(product.getName());
                                if (product.getPrice() != null || product.getPrice().equals(""))
                                    productAdd.setPrice(new BigDecimal(product.getPrice()));
                                productAdd.setAp3rId(ap3r);
                                ap3rService.save(productAdd);
                            }
                        }
                        responseCode.setStatus(ap3r.getStatus());
                    } else {
                        responseCode.setResponseCode(WebGuiConstant.NULL_AP3R);
                    }
                } else {
                    responseCode.setResponseCode(WebGuiConstant.NULL_AP3R);
                }
            }
            String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
            responseCode.setResponseMessage(label);
            responseCode.setAp3rId(ap3rId);
            responseCode.setLocalId(request.getLocalId());
        } catch (Exception e) {
            log.error("addAp3r error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("addAp3r error: " + e.getMessage());
        } finally {
            try {
                log.info("Trying to CREATE Terminal Activity....");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_MODY_AP3R);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        log.info("TERMINAL ACTIVITY LOGGING SUCCESS...");
                        log.info("Updating Terminal Activity");
                        AuditLog auditLog = new AuditLog();
                        auditLog.setActivityType(AuditLog.MODIF_AP3R);
                        auditLog.setCreatedBy(request.getUsername());
                        auditLog.setCreatedDate(new Date());
                        if (!request.toString().equals(null) && !(request.toString().length() < 1000)
                                || !request.toString().equals("") && !(request.toString().length() < 1000))
                            auditLog.setDescription(
                                    request.toString().substring(0, 1000) + " | " + responseCode.toString());
                        auditLog.setReffNo(request.getRetrievalReferenceNumber());
                        auditLogService.insertAuditLog(auditLog);
                        log.info("AUDIT LOG LOGGING SUCCESS...");
                        log.info("Updating AUDIT LOG");
                    }
                });
                log.info("addAp3r RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("addAp3r saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return responseCode;
        }
    }

    @RequestMapping(value = WebGuiConstant.AP3R_APPROVAL_DATA_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    AP3RResponse doApproval(@RequestBody String requestString, @PathVariable("apkVersion") String apkVersion) throws Exception {

        Object obj = new JsonUtils().fromJson(requestString, AP3RRequest.class);
        final AP3RRequest request = (AP3RRequest) obj;
        final AP3RResponse responseCode = new AP3RResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        String ap3rId = "";
        try {
            log.info("approvalAp3r INCOMING MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(), request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei() + " ,sessionKey : " + request.getSessionKey());
            } else {
                if (!request.getAp3rId().equals("")) {
                    String localId = request.getAp3rId();
                    AP3R temp = ap3rService.findByLocalId(localId);
                    if (temp == null) {
                        responseCode.setResponseCode(WebGuiConstant.NULL_AP3R);
                        String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                        responseCode.setResponseMessage(label);
                        return responseCode;
                    }
                    AP3R ap3r = ap3rService.findByAp3rId(temp.getAp3rId());
                    if (ap3r.getStatus().equals(WebGuiConstant.STATUS_APPROVED)) {
                        String label = getMessage("webservice.rc.label." + WebGuiConstant.RC_ALREADY_APPROVED);
                        responseCode.setResponseMessage(label);
                        return responseCode;
                    }
                    if (request.getStatus().equals(WebGuiConstant.STATUS_APPROVED)) {
                        ap3r.setStatus(WebGuiConstant.STATUS_APPROVED);
                        ap3r.setUpdatedDate(new Date());
                        ap3r.setApprovalStatus(Integer.parseInt(request.getApprovalStatus()));
                        ap3r.setUpdatedBy(request.getUsername());
                    } else if (request.getStatus().equals(WebGuiConstant.STATUS_REJECTED)) {
                        ap3r.setStatus(WebGuiConstant.STATUS_REJECTED);
                        ap3r.setRejectedReason(request.getRejectionReason());
                    }
                    ap3rService.save(ap3r);
                    ap3rId = ap3r.getLocalId();
                    if (ap3r.getCreatedDate() != null)
                        responseCode.setCreatedAt(formatter.format(ap3r.getCreatedDate()));
                    else
                        responseCode.setCreatedAt("");
                    if (ap3r.getUpdatedDate() != null)
                        responseCode.setUpdatedAt(formatter.format(ap3r.getUpdatedDate()));
                    else
                        responseCode.setUpdatedAt("");
                } else {
                    responseCode.setResponseCode(WebGuiConstant.NULL_AP3R);
                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(label);
                }
            }
            String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
            responseCode.setResponseMessage(label);
            responseCode.setAp3rId(ap3rId);
        } catch (Exception e) {
            log.error("approvalAp3r error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("approvalAp3r error: " + e.getMessage());
        } finally {
            try {
                log.info("approvalAp3r Trying to CREATE Terminal Activity..");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_MODY_approvalAp3r);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        log.info("TERMINAL ACTIVITY LOGGING SUCCESS...");
                        log.info("Updating Terminal Activity");
                        AuditLog auditLog = new AuditLog();
                        auditLog.setActivityType(AuditLog.MODIF_AP3R);
                        auditLog.setCreatedBy(request.getUsername());
                        auditLog.setCreatedDate(new Date());
                        if (!request.toString().equals(null) && !(request.toString().length() < 1000)
                                || !request.toString().equals("") && !(request.toString().length() < 1000))
                            auditLog.setDescription(
                                    request.toString().substring(0, 1000) + " | " + responseCode.toString());
                        auditLog.setReffNo(request.getRetrievalReferenceNumber());
                        auditLogService.insertAuditLog(auditLog);
                        log.info("AUDIT LOG LOGGING SUCCESS...");
                        log.info("Updating AUDIT LOG");
                    }
                });
                log.info("approvalAp3r RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("approvalAp3r saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
        }
        return responseCode;
    }
    
    /*
 	 * @author : Ilham
 	 * @Since  : 20191128
 	 * @category patch 
 	 *    - set default 19000101 jika ada disbursmentdate ap3r null  
 	 */ 
    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_AP3R_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    AP3RListResponse doList(@RequestBody AP3RRequest request,
                            @PathVariable("apkVersion") String apkVersion) throws Exception {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        String countData = request.getGetCountData();
        String page = request.getPage();
        String startLookupDate = request.getStartLookupDate();
        String endLookupDate = request.getEndLookupDate();
        final AP3RListResponse responseCode = new AP3RListResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("listAp3r INCOMING MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(), request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                User user = userService.findUserByUsername(username);
                if (user.getRoleUser() == null) {
                    responseCode.setResponseCode(WebGuiConstant.ROLE_NULL);
                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(label);
                    return responseCode;
                }
                final Page<AP3R> ap3rPage = ap3rService.getAp3rByUser(request.getUsername(), page, countData,
                        startLookupDate, endLookupDate);
                List<AP3RList> ap3rListResponse = new ArrayList<>();
                for (final AP3R ap3r : ap3rPage) {
                    AP3RList ap3rMap = new AP3RList();
                    ap3rMap.setAp3rId(ap3r.getLocalId());
                    if (ap3r.getSwId() != null || ap3r.getSwProductId() != null) {
                        SurveyWawancara swLocalId = swService.getSWById(ap3r.getSwId().toString());
                        if (swLocalId != null) {
                            ap3rMap.setSwId(swLocalId.getLocalId());
                        }
                        SWProductMapping swProductMapLocal = swService.findProductMapById(ap3r.getSwProductId());
                        if (swProductMapLocal != null) {
                            ap3rMap.setSwProductId(swProductMapLocal.getLocalId());
                        }
                    }
                    ap3rMap.setHaveAccount("" + ap3r.getHasSaving());
                    if (ap3rMap.getHaveAccount().equals("t")) {
                        ap3rMap.setHaveAccount("true");
                    } else {
                        ap3rMap.setHaveAccount("false");
                    }
                    LoanDeviation loanDeviation = loanService.findDeviationByAp3rId(ap3r.getAp3rId());
                    if (loanDeviation != null) {
                        if (loanDeviation.getStatus() != null) {
                            if (loanDeviation.getStatus().equals(WebGuiConstant.STATUS_WAITING_APPROVAL)
                                    || loanDeviation.getStatus().equals(WebGuiConstant.STATUS_APPROVED)) {
                                ap3rMap.setDeviationExists("true");
                            } else {
                                ap3rMap.setDeviationExists("false");
                            }
                        } else {
                            ap3rMap.setDeviationExists("false");
                        }
                    } else {
                        ap3rMap.setDeviationExists("false");
                    }
                    ap3rMap.setAccountPurpose(ap3r.getOpenSavingReason());
                    ap3rMap.setFundSource(ap3r.getFundSource());
                    ap3rMap.setYearlyTransaction("" + ap3r.getTransactionInYear());
                    ap3rMap.setFinancingPurpose(ap3r.getLoanReason());
                    ap3rMap.setBusinessTypeId(ap3r.getBusinessField());
                    List<FundedThingsReq> productMapList = new ArrayList<>();
                    List<FundedThings> productList = ap3rService.findByAp3rId(ap3r);
                    for (FundedThings product : productList) {
                        FundedThingsReq productMap = new FundedThingsReq();
                        productMap.setName(product.getProductName());
                        productMap.setPrice(product.getPrice().toString());
                        productMapList.add(productMap);
                    }
                    ap3rMap.setFinancedGoods(productMapList);
                    /*
                     * validasi disbursmentdate null
                     */
                    if(ap3r.getDisbursementDate() != null) {
                        ap3rMap.setDisburseDate(formatter.format(ap3r.getDisbursementDate()));                    	
                    }
                    else {
                    	ap3rMap.setDisburseDate("1900-01-01 00:00:00");
                    }
                    ap3rMap.setHighRiskCustomer("" + ap3r.getHighRiskCustomer());
                    if (ap3rMap.getHighRiskCustomer().equals("t")) {
                        ap3rMap.setHighRiskCustomer("true");
                    } else {
                        ap3rMap.setHighRiskCustomer("false");
                    }
                    ap3rMap.setHighRiskBusiness("" + ap3r.getHighRiskBusiness());
                    if (ap3rMap.getHighRiskBusiness().equals("t")) {
                        ap3rMap.setHighRiskBusiness("true");
                    } else {
                        ap3rMap.setHighRiskBusiness("false");
                    }
                    ap3rMap.setStatus(ap3r.getStatus());
                    ap3rMap.setCreatedBy(ap3r.getCreatedBy());
                    if (ap3r.getDueDate() != null)
                        ap3rMap.setDueDate(formatter.format(ap3r.getDueDate()));
                    if (ap3r.getRejectedReason() != null)
                        ap3rMap.setRejectionReason(ap3r.getRejectedReason());
                    else
                        ap3rMap.setRejectionReason("");
                    if (ap3r.getApprovalStatus() != null)
                        ap3rMap.setApprovalStatus(ap3r.getApprovalStatus().toString());
                    else
                        ap3rMap.setApprovalStatus("0");
                    if (ap3r.getCreatedDate() != null)
                        ap3rMap.setCreatedAt(formatter.format(ap3r.getCreatedDate()));
                    else
                        ap3rMap.setCreatedAt("");
                    if (ap3r.getUpdatedDate() != null)
                        ap3rMap.setUpdatedAt(formatter.format(ap3r.getUpdatedDate()));
                    else
                        ap3rMap.setUpdatedAt("");
                    ap3rListResponse.add(ap3rMap);
                }
                final List<AP3R> listDeletedAp3r = ap3rService.findIsDeletedAp3rList();
                List<String> deletedAp3rList = new ArrayList<>();
                responseCode.setCurrentTotal(String.valueOf(ap3rPage.getContent().size()));
                responseCode.setGrandTotal(String.valueOf(ap3rPage.getTotalElements()));
                responseCode.setTotalPage(String.valueOf(ap3rPage.getTotalPages()));
                responseCode.setAp3rList(ap3rListResponse);
                if (!listDeletedAp3r.isEmpty()) {
                    for (AP3R deletedAp3r : listDeletedAp3r) {
                        String id = deletedAp3r.getLocalId();
                        deletedAp3rList.add(id);
                    }
                }
                responseCode.setDeletedAp3rList(deletedAp3rList);
            }
        } catch (Exception e) {
            log.error("listAp3r error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("listAp3r error: " + e.getMessage());
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_listAp3r);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        log.info("TERMINAL ACTIVITY LOGGING SUCCESS...");
                        log.info("Updating Terminal Activity");
                    }
                });
                log.info("listAp3r RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("listAp3r saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return responseCode;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_BUSINESS_TYPE_AP3R_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    BusinessTypeAp3rListResponse doBusinessTypeList(
            @RequestBody final BusinessTypeListRequest request, @PathVariable("apkVersion") String apkVersion)
            throws Exception {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        String page = request.getPage().toString();
        String countData = request.getGetCountData().toString();
        final BusinessTypeAp3rListResponse responseCode = new BusinessTypeAp3rListResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("listBusinessTypeAP3R INCOMING MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(),
                    request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                log.info("Validation success, get Business Type data");
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                final Page<BusinessTypeAP3R> businessTypePage = swService.getBusinessTypeAp3r(page, countData);
                log.info("Finishing get Business Type AP3R data");
                final List<BusinessTypeAp3rList> businessTypeResponses = new ArrayList<BusinessTypeAp3rList>();
                for (final BusinessTypeAP3R businessType : businessTypePage) {
                    BusinessTypeAp3rList business = new BusinessTypeAp3rList();
                    business.setId(businessType.getBusinessId().toString());
                    business.setBusinessType(businessType.getBusinessType());
                    business.setSubBusiness(businessType.getSubBusiness());
                    businessTypeResponses.add(business);
                }
                responseCode.setBusinessTypeList(businessTypeResponses);
                log.info("Finishing create response Jenis Usaha AP3R data");
                log.info("Try to Update Terminal");
                Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                terminal.setSwProgress(PercentageSynchronizer.processSyncPercent(request.getPage().toString(),
                        String.valueOf(businessTypePage.getTotalPages()), terminal.getSwProgress()));
                terminalService.updateTerminal(terminal);
                log.info("Finishing Update Terminal");
                responseCode.setCurrentTotal(String.valueOf(businessTypePage.getContent().size()));
                responseCode.setGrandTotal(String.valueOf(businessTypePage.getTotalElements()));
                responseCode.setTotalPage(String.valueOf(businessTypePage.getTotalPages()));
            }
        } catch (Exception e) {
            log.error("listBusinessTypeAP3R error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("listBusinessTypeAP3R error: " + e.getMessage());
        } finally {
            try {
                log.trace("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_listBusinessTypeAP3R);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        log.info("TERMINAL ACTIVITY LOGGING SUCCESS...");
                        log.info("Updating Terminal Activity");
                    }
                });
                log.info("listBusinessTypeAP3R RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("listBusinessTypeAP3R ERROR..CREATE Terminal Activity on TERMINAL_GET_BUSINESS_TYPE_AP3R_REQUEST :" + e.getMessage());
            }
            return responseCode;
        }
    }

}