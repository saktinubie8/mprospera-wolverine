package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import id.co.telkomsigma.btpns.mprospera.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.pdk.PelatihanDasarKeanggotaan;
import id.co.telkomsigma.btpns.mprospera.model.pdk.PDKDetail;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.request.PDKDetailRequest;
import id.co.telkomsigma.btpns.mprospera.request.PDKRequest;
import id.co.telkomsigma.btpns.mprospera.response.ListPDKResponse;
import id.co.telkomsigma.btpns.mprospera.response.PDKDetailResponse;
import id.co.telkomsigma.btpns.mprospera.response.PDKResponse;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;

@SuppressWarnings("unchecked")
@Controller("webServicePDKController")
public class WebServicePDKController extends GenericController {

    @Autowired
    private PDKService pdkService;

    @Autowired
    private SWService swService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private AuditLogService auditLogService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private UserService userService;

    JsonUtils jsonUtils = new JsonUtils();

    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_PDK_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    PDKResponse doList(@RequestBody final PDKRequest request,
                       @PathVariable("apkVersion") String apkVersion) throws Exception {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        String countData = request.getGetCountData();
        String page = request.getPage();
        String startLookupDate = request.getStartLookupDate();
        String endLookupDate = request.getEndLookupDate();
        final PDKResponse responseCode = new PDKResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("listPDK INCOMING MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.info("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                log.info("Validation success, get Pelatihan Dasar Keanggotaan data");
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                Timer timer = new Timer();
                timer.start("[" + WebGuiConstant.TERMINAL_GET_PDK_REQUEST + "] [pdkService.getPDK]");
                User user = (User) userService.loadUserByUsername(username);
                String loc = user.getOfficeCode();
                List<String> userList = new ArrayList();
                for (User users : userService.loadUserByLocationId(loc)) {
                    userList.add(users.getUsername());
                }
                final Page<PelatihanDasarKeanggotaan> pdkPage = pdkService.getPDK(userList, page, countData,
                        startLookupDate, endLookupDate);
                timer.stop();
                final List<ListPDKResponse> pdkResponses = new ArrayList<ListPDKResponse>();
                for (final PelatihanDasarKeanggotaan pdk : pdkPage) {
                    ListPDKResponse pdkResponse = new ListPDKResponse();
                    pdkResponse.setPdkId(pdk.getPdkId().toString());
                    pdkResponse.setPlaceOwner(pdk.getPdkOwnerName());
                    pdkResponse.setPdkLocation(pdk.getPdkLocation());
                    pdkResponse.setPhoneNumber(pdk.getPhoneNumber());
                    pdkResponse.setTotalParticipant(pdk.getTotalParticipant());
                    pdkResponse.setGraduatedParticipant(pdk.getGraduateParticipant());
                    pdkResponse.setCreatedBy(pdk.getCreatedBy());
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd hh:mm:ss");
                    if (pdk.getPdkDate() != null)
                        pdkResponse.setPdkDate(formatter.format(pdk.getPdkDate()));
                    else
                        pdkResponse.setPdkDate("");
                    pdkResponse.setPdkDetail(new ArrayList<PDKDetailResponse>());
                    for (PDKDetail pdkDetail : pdk.getSwList()) {
                        PDKDetailResponse pdkDetailList = new PDKDetailResponse();
                        SurveyWawancara swLocal = swService.getSWById(pdkDetail.getSwId().toString());
                        if (swLocal != null) {
                            if (swLocal.getLocalId() != null)
                                pdkDetailList.setSwId(swLocal.getLocalId());
                        }
                        pdkDetailList.setGraduateStatus(pdkDetail.getGraduateStatus());
                        pdkResponse.getPdkDetail().add(pdkDetailList);
                    }
                    pdkResponses.add(pdkResponse);
                }
                final List<PelatihanDasarKeanggotaan> listDeletedPdk = pdkService.findIsDeletedPdkList();
                List<String> deletedPdkList = new ArrayList<>();
                for (PelatihanDasarKeanggotaan deletedPdk : listDeletedPdk) {
                    String id = deletedPdk.getPdkId().toString();
                    deletedPdkList.add(id);
                }
                responseCode.setDeletedPdkList(deletedPdkList);
                responseCode.setCurrentTotal(String.valueOf(pdkPage.getContent().size()));
                responseCode.setGrandTotal(String.valueOf(pdkPage.getTotalElements()));
                responseCode.setTotalPage(String.valueOf(pdkPage.getTotalPages()));
                responseCode.setPdkList(pdkResponses);
            }
        } catch (ParseException e) {
            log.error("listPDK error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("listPDK error: " + e.getMessage());
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_listPDK);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("Finishing create Terminal Activity");
                log.info("listPDK RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("listPDK saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return responseCode;
        }
    }

    @RequestMapping(value = WebGuiConstant.PDK_SUBMIT_DATA_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    PDKResponse doAdd(@RequestBody String requestString,
                      @PathVariable("apkVersion") String apkVersion) throws Exception {

        Object obj = new JsonUtils().fromJson(requestString, PDKRequest.class);
        final PDKRequest request = (PDKRequest) obj;
        final PDKResponse responseCode = new PDKResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        String pdkId = "";
        try {
            log.info("addPDK INCOMING MESSAGE : " + jsonUtils.toJson(request));
            // validation
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(),
                    request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei()
                        + " ,sessionKey : " + request.getSessionKey());
            } else {
                // insert ke tabel PDK
                PelatihanDasarKeanggotaan pdk = new PelatihanDasarKeanggotaan();
                pdk.setPdkDate(new Date());
                pdk.setCreatedBy(request.getUsername());
                pdk.setPdkOwnerName(request.getPlaceOwner());
                pdk.setPdkLocation(request.getPdkLocation());
                pdk.setPhoneNumber(request.getPhoneNumber());
                pdk.setCreatedDate(new Date());
                int totalGraduate = 0;
                int totalParticipant = 0;
                for (PDKDetailRequest mapSw : request.getSwList()) {
                    PDKDetail detail = new PDKDetail();
                    if (mapSw.getSwId() == null) {
                        responseCode.setResponseCode(WebGuiConstant.UNKNOWN_SW_ID);
                        String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                        responseCode.setResponseMessage(label);
                        return responseCode;
                    }
                    SurveyWawancara swLocal = swService.findSwByLocalId(mapSw.getSwId());
                    if (swLocal == null) {
                        responseCode.setResponseCode(WebGuiConstant.UNKNOWN_SW_ID);
                        String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                        responseCode.setResponseMessage(label);
                        return responseCode;
                    }
                    detail.setSwId(swLocal.getSwId());
                    detail.setGraduateStatus(mapSw.getGraduateStatus());
                    detail.setPdk(pdk);
                    if (pdk.getSwList() == null)
                        pdk.setSwList(new java.util.ArrayList<PDKDetail>());
                    pdk.getSwList().add(detail);
                    if ("Y".equals(mapSw.getGraduateStatus()))
                        totalGraduate++;
                    totalParticipant++;
                }
                pdk.setGraduateParticipant(totalGraduate);
                pdk.setTotalParticipant(totalParticipant);
                pdk.setLongitude(request.getLongitude());
                pdk.setLatitude(request.getLatitude());
                if ("delete".equals(request.getAction()))
                    pdk.setIsDeleted(true);
                if ("delete".equals(request.getAction()) || "update".equals(request.getAction())) {
                    if (null != request.getPdkId()) {
                        pdk.setPdkId(Long.parseLong(request.getPdkId()));
                        pdkId = request.getPdkId();
                    } else {
                        responseCode.setResponseCode(WebGuiConstant.UNKNOWN_PDK_ID);
                    }
                }
                if (("delete".equals(request.getAction()) || "update".equals(request.getAction()))
                        && !pdkService.isValidPdk(request.getPdkId()))
                    responseCode.setResponseCode(WebGuiConstant.UNKNOWN_PDK_ID);
                else {
                    if (!terminalService.isDuplicate(request.getRetrievalReferenceNumber(), "addPDK")) {
                        pdkService.save(pdk);
                        pdkId = pdk.getPdkId().toString();
                        if (customerService.getByPdkId(Long.parseLong(pdkId)) == null) {
                            for (PDKDetailRequest sw : request.getSwList()) {
                                SurveyWawancara swId = swService.findSwByLocalId(sw.getSwId());
                                Timer timer = new Timer();
                                timer.start(
                                        "[" + WebGuiConstant.PDK_SUBMIT_DATA_REQUEST + "] [customerService.getBySwId]");
                                Customer cust = customerService.getBySwId(swId.getSwId());
                                timer.stop();
                                if (cust != null) {
                                    cust.setCustomerId(cust.getCustomerId());
                                    cust.setPdkId(Long.parseLong(pdkId));
                                    customerService.save(cust);
                                }
                            }
                        }
                    } else {
                        Timer timer = new Timer();
                        timer.start("[" + WebGuiConstant.PDK_SUBMIT_DATA_REQUEST + "] [pdkService.getPDKByRrn]");
                        PelatihanDasarKeanggotaan pdkExist = pdkService
                                .getPDKByRrn(request.getRetrievalReferenceNumber());

                        if (pdkExist != null)
                            pdkId = pdkExist.getPdkId().toString();
                    }
                }
            }
            String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
            responseCode.setResponseMessage(label);
            responseCode.setPdkId(pdkId);
        } catch (Exception e) {
            log.error("addPDK error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("addPDK error: " + e.getMessage());
        } finally {
            try {
                log.info("Trying to CREATE Terminal Activity...");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_MODY_PDK);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.insertRrn("addPDK", request.getRetrievalReferenceNumber().trim());
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        log.info("TERMINAL ACTIVITY LOGGING SUCCESS...");
                        log.info("Updating Terminal Activity");
                        AuditLog auditLog = new AuditLog();
                        auditLog.setActivityType(AuditLog.MODIF_PDK);
                        auditLog.setCreatedBy(request.getUsername());
                        auditLog.setCreatedDate(new Date());
                        auditLog.setDescription(request.toString() + " | " + responseCode.toString());
                        auditLog.setReffNo(request.getRetrievalReferenceNumber());
                        auditLogService.insertAuditLog(auditLog);
                        log.info("AUDIT LOG LOGGING SUCCESS...");
                        log.info("Updating AUDIT LOG");
                    }
                });
                log.info("addPDK RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("addPDK TERMINAL ACTIVITY ERROR method doAdd" + e.getMessage());
            }
            return responseCode;
        }
    }

}