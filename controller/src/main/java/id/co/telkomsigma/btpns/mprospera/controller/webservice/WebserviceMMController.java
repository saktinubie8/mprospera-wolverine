package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.mm.MiniMeeting;
import id.co.telkomsigma.btpns.mprospera.model.pm.ProjectionMeeting;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaKelurahan;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.request.AddMMRequest;
import id.co.telkomsigma.btpns.mprospera.request.ListMMRequest;
import id.co.telkomsigma.btpns.mprospera.request.PMRequest;
import id.co.telkomsigma.btpns.mprospera.response.AddMMResponse;
import id.co.telkomsigma.btpns.mprospera.response.ListMMResponse;
import id.co.telkomsigma.btpns.mprospera.response.ListPMResponse;
import id.co.telkomsigma.btpns.mprospera.response.MMResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.FieldValidationUtil;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.PercentageSynchronizer;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller("webserviceMMController")
public class WebserviceMMController extends GenericController {

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    @Qualifier("mmService")
    private MMService mmService;

    @Autowired
    @Qualifier("sdaService")
    private SDAService sdaService;

    @Autowired
    @Qualifier("auditLogService")
    private AuditLogService auditLogService;

    @Autowired
    @Qualifier("areaService")
    private AreaService areaService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private PmService pmService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private HazelcastClientService hazelcastService;

    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd hh:mm:ss");
    JsonUtils jsonUtils = new JsonUtils();

    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_MM_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    ListMMResponse listMM(@RequestBody final ListMMRequest request,
                          @PathVariable("apkVersion") String apkVersion) {

        final String imei = request.getImei();
        String countData = request.getGetCountData().toString();
        final String page = request.getPage().toString();
        String startLookupDate = request.getStartLookupDate();
        String endLookupDate = request.getEndLookupDate();
        final ListMMResponse response = new ListMMResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("listMM INCOMING MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(),
                    request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei() + " ,sessionKey : " + request.getSessionKey());
            } else {
                Timer timer = new Timer();
                timer.start("[" + WebGuiConstant.TERMINAL_GET_MM_REQUEST + "] [mmService.getMM]");
                final Page<MiniMeeting> mms = mmService.getMMByUser(page, countData, startLookupDate, endLookupDate,request.getUsername());
                timer.stop();
                final List<MMResponse> mmResponses = new ArrayList<MMResponse>();
                for (final MiniMeeting mm : mms.getContent()) {
                    MMResponse mmResponse = new MMResponse();
                    mmResponse.setCreatedBy(mm.getCreatedBy());
                    mmResponse.setCreatedDate(formatter.format(mm.getCreatedDate()));
                    mmResponse.setMmId(String.valueOf(mm.getMmId()));
                    mmResponse.setMmLocationName(mm.getMmLocationName());
                    mmResponse.setMmParticipant(mm.getMmNumber());
                    mmResponse.setMmOwner(mm.getMmOwner());
                    mmResponse.setPmCandidate(mm.getPmCandidate());
                    final List<ProjectionMeeting> pm = pmService.findByMmId(mm);
                    final List<ListPMResponse> pmResponses = new ArrayList<ListPMResponse>();
                    ListPMResponse pmResponseMap = new ListPMResponse();
                    for (final ProjectionMeeting projectionMeeting : pm) {
                        pmResponseMap.setPmId(projectionMeeting.getPmId().toString());
                    }
                    if (mm.getPmId() != null) {
                        pmResponseMap.setPmId(mm.getPmId().toString());
                    }
                    pmResponses.add(pmResponseMap);
                    mmResponse.setPmList(pmResponses);
                    mmResponses.add(mmResponse);
                }
                Terminal terminal = terminalService.loadTerminalByImei(imei);
                terminal.setMmProgress(PercentageSynchronizer.processSyncPercent(page,
                        String.valueOf(mms.getTotalPages()), terminal.getMmProgress()));
                terminalService.updateTerminal(terminal);
                final List<MiniMeeting> listDeletedMm = mmService.findIsDeletedMmList();
                List<String> deletedMmList = new ArrayList<>();
                for (MiniMeeting deletedMm : listDeletedMm) {
                    String id = deletedMm.getMmId().toString();
                    deletedMmList.add(id);
                }
                response.setDeletedMmList(deletedMmList);
                response.setCurrentTotal(String.valueOf(mms.getContent().size()));
                response.setGrandTotal(String.valueOf(mms.getTotalElements()));
                response.setTotalPage(String.valueOf(mms.getTotalPages()));
                response.setMmList(mmResponses);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
            }
        } catch (Exception e) {
            log.error("listMM error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("listMM error: " + e.getMessage());
        } finally {
            try {
                // insert MessageLogs
                log.info("Trying to CREATE Terminal Activity...");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_MM);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        log.info("TERMINAL ACTIVITY LOGGING SUCCESS...");
                        log.info("Updating Terminal Activity");
                    }
                });
                log.info("listMM RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("listMM ERROR CREATE terminalActivity on TERMINAL_GET_MM_REQUEST : " + e.getMessage());
            }
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_ADD_MM_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    AddMMResponse saveMM(@RequestBody final AddMMRequest request,
                         @PathVariable("apkVersion") String apkVersion) {

        final AddMMResponse response = new AddMMResponse();
        String mmId = "";
        String pmId = "";
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("addMM INCOMING MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(),
                    request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei()
                        + " ,sessionKey : " + request.getSessionKey());
            } else {
                MiniMeeting mm = new MiniMeeting();
                mm.setAreaId(request.getAreaId());
                mm.setCreatedBy(request.getUsername());
                mm.setCreatedDate(new Date());
                mm.setLatitude(FieldValidationUtil.stringToZeroValidation(request.getLatitude()));
                mm.setLongitude(FieldValidationUtil.stringToZeroValidation(request.getLongitude()));
                mm.setMmLocationName(request.getMmLocationName());
                mm.setMmNumber(request.getMmParticipant());
                mm.setMmOwner(request.getMmOwner());
                mm.setRrn(request.getRetrievalReferenceNumber());
                mm.setPmCandidate(request.getPmCandidate());
                List<String> listPmId = new ArrayList<String>();
                for (PMRequest pmPlanList : request.getPmPlanList()) {
                    listPmId.add(pmPlanList.getPmId());
                }
                if (!listPmId.isEmpty()) {
                    if (listPmId.get(0) != null) {
                        if (!listPmId.get(0).equals("")) {
                            mm.setPmId(Long.parseLong(listPmId.get(0)));
                        }
                    }
                }
                if ("delete".equals(request.getAction()))
                    mm.setIsDeleted(true);

                if ("delete".equals(request.getAction()) || "update".equals(request.getAction())) {
                    if (null != request.getMmId()) {
                        mm.setMmId(Long.parseLong(request.getMmId()));
                        mmId = request.getMmId();
                    } else
                        response.setResponseCode(WebGuiConstant.UNKNOWN_MM_ID);
                }
                if (("delete".equals(request.getAction()) || "update".equals(request.getAction()))
                        && !mmService.isValidMm(request.getMmId()))
                    response.setResponseCode(WebGuiConstant.UNKNOWN_MM_ID);
                else {
                    if (!terminalService.isDuplicate(request.getRetrievalReferenceNumber(), "saveMM")) {
                        mmService.save(mm);
                        mmId = mm.getMmId().toString();
                        List<ListPMResponse> pmList = new ArrayList<>();
                        for (PMRequest pmPlanList : request.getPmPlanList()) {
                            ProjectionMeeting pmPlan = new ProjectionMeeting();
                            pmPlan.setAreaId(request.getAreaId());
                            AreaKelurahan area = areaService.findKelurahanById(request.getAreaId());
                            if (area != null) {
                                pmPlan.setAreaName(area.getAreaName());
                            }
                            pmPlan.setCreatedBy(request.getUsername());
                            pmPlan.setCreatedDate(new Date());
                            pmPlan.setPmLocationName(pmPlanList.getPmLocationName());
                            pmPlan.setPmPlanNumber(request.getPmCandidate());
                            pmPlan.setPmOwner(pmPlanList.getPmOwner());
                            pmPlan.setPmOwnerPhoneNumber(pmPlanList.getPmOwnerPhoneNumber());
                            pmPlan.setTime(pmPlanList.getTime());
                            if (pmPlanList.getPmPlanDate() != null)
                                if (!pmPlanList.getPmPlanDate().equals(""))
                                    pmPlan.setPmPlanDate(formatter.parse(pmPlanList.getPmPlanDate()));
                            if (pmPlanList.getPmDate() != null)
                                if (!pmPlanList.getPmDate().equals(""))
                                    pmPlan.setPmDate(formatter.parse(pmPlanList.getPmDate()));
                            pmPlan.setMmId(mm);
                            pmPlan.setStatus(WebGuiConstant.STATUS_PLAN);
                            if ("delete".equals(pmPlanList.getAction()))
                                mm.setPmId(null);
                            if ("delete".equals(pmPlanList.getAction()) || "update".equals(pmPlanList.getAction())) {
                                if (null != pmPlanList.getPmId()) {
                                    pmPlan.setPmId(Long.parseLong(pmPlanList.getPmId()));
                                    pmId = pmPlanList.getPmId();
                                } else
                                    response.setResponseCode(WebGuiConstant.UNKNOWN_MM_ID);
                            }
                            if (("delete".equals(pmPlanList.getAction()) || "update".equals(pmPlanList.getAction()))
                                    && !pmService.isValidPm(pmPlanList.getPmId()))
                                response.setResponseCode(WebGuiConstant.UNKNOWN_MM_ID);
                            pmService.save(pmPlan);
                            ListPMResponse pm = new ListPMResponse();
                            pm.setPmId(pmPlan.getPmId().toString());
                            pm.setStatus(pmPlan.getStatus());
                            pm.setLocalId(pmPlanList.getLocalId());
                            pmList.add(pm);
                            if ("insert".equals(pmPlanList.getAction()) || "update".equals(pmPlanList.getAction())) {
                                mm.setPmId(pmPlan.getPmId());
                            }
                        }
                        mmService.save(mm);
                        response.setPmListId(pmList);
                    } else {
                        Timer timer2 = new Timer();
                        timer2.start("[" + WebGuiConstant.TERMINAL_ADD_MM_REQUEST + "] [mmService.getMMByRrn]");
                        MiniMeeting mmExist = mmService.getMMByRrn(request.getRetrievalReferenceNumber());
                        timer2.stop();
                        mmId = mmExist.getMmId().toString();
                    }
                }
            }
            response.setMmId(mmId);
            response.setLocalId(request.getLocalId());
            String label = getMessage("webservice.rc.label." + response.getResponseCode());
            response.setResponseMessage(label);
        } catch (Exception e) {
            log.error("addMM error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("addMM error: " + e.getMessage());
        } finally {
            try {
                log.info("Trying to CREATE Terminal Activity...");
                // insert MessageLogs
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_MODY_MM);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.insertRrn("saveMM", request.getRetrievalReferenceNumber().trim());
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        log.info("TERMINAL ACTIVITY LOGGING SUCCESS...");
                        log.info("Updating Terminal Activity");
                        log.info("Trying to CREATE Audit Log...");
                        // insert audit log
                        AuditLog auditLog = new AuditLog();
                        auditLog.setActivityType(AuditLog.MODIF_MM);
                        auditLog.setCreatedBy(request.getUsername());
                        auditLog.setCreatedDate(new Date());
                        auditLog.setDescription(request.toString() + " | " + response.toString());
                        auditLog.setReffNo(request.getRetrievalReferenceNumber());
                        auditLogService.insertAuditLog(auditLog);
                        log.info("AUDIT LOG LOGGING SUCCESS...");
                        log.info("Updating AUDIT LOG");
                    }
                });
                log.info("addMM RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("addMM terminalActivity OR auditLog ERROR on method saveMM : " + e.getMessage());
            }
            return response;
        }
    }

}