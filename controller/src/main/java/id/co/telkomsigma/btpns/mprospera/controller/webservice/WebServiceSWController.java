package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import java.math.BigDecimal;
import java.math.MathContext;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import id.co.telkomsigma.btpns.mprospera.model.sw.*;
import id.co.telkomsigma.btpns.mprospera.request.*;
import id.co.telkomsigma.btpns.mprospera.response.*;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.FieldValidationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.pm.ProjectionMeeting;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.PercentageSynchronizer;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;

@Controller("webserviceSWController")
public class WebServiceSWController extends GenericController {

    @Autowired
    private SWService swService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private AuditLogService auditLogService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private LoanService loanService;

    @Autowired
    private UserService userService;

    @Autowired
    private AreaService areaService;

    @Autowired
    private PmService pmService;

    @Autowired
    private AP3RService ap3RService;

    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
    SimpleDateFormat formatDateTime = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
    SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");

    JsonUtils jsonUtils = new JsonUtils();

    // SUBMIT SW
    @RequestMapping(value = WebGuiConstant.SW_SUBMIT_DATA_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    SWResponse doAdd(@RequestBody String requestString,
                     @PathVariable("apkVersion") String apkVersion) throws Exception {

        String swId = "";
        String swProductId = "";
        List<ProductIdListResponse> productIdListResponses = new ArrayList<>();
        Object obj = new JsonUtils().fromJson(requestString, SWRequest.class);
        final SWRequest request = (SWRequest) obj;
        final SWResponse responseCode = new SWResponse();
        // Set Response Sukses terlebih dulu
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        // Validasi messaging request dari Android
        try {
            log.info("addSW INCOMING MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(), request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei() + " ,sessionKey : " + request.getSessionKey());
            } else {
                // insert ke tabel SW
                log.info("MAPPING SW FROM REQUEST");
                User userPs = userService.findUserByUsername(request.getUsername());
                SWPojoRequest swPojo = request.getSw();
                // Validasi SW Lokal ID android sudah ada di server atau belum
                if (swPojo.getSwId() == null || "".equals(swPojo.getSwId())) {
                    log.error("UNKNOWN SW ID");
                    responseCode.setResponseCode(WebGuiConstant.UNKNOWN_SW_ID);
                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(label);
                    return responseCode;
                }
                SurveyWawancara sw = swService.findSwByLocalId(swPojo.getSwId());
                if (request.getAction().equalsIgnoreCase(WebGuiConstant.ACTION_INSERT) && sw != null) {
                    log.error("SW ALREADY EXIST");
                    responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
                    String label = getMessage("webservice.rc.label." + WebGuiConstant.SW_ALREADY_EXIST);
                    responseCode.setResponseMessage(label);
                    return responseCode;
                }
                if (!request.getAction().equalsIgnoreCase(WebGuiConstant.ACTION_INSERT)) {
                    log.info("SW Existing to update/delete");
                    if (sw == null) {
                        log.error("UNKNOWN SW ID");
                        responseCode.setResponseCode(WebGuiConstant.UNKNOWN_SW_ID);
                        String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                        responseCode.setResponseMessage(label);
                        return responseCode;
                    }
                    sw.setUpdatedDate(new Date());
                    sw.setUpdatedBy(request.getUsername());
                }
                if (sw == null) {
                    sw = new SurveyWawancara();
                }
                sw.setCreatedDate(new Date()); // timestamp current date
                sw.setCreatedBy(request.getUsername()); // user yang input data
                sw.setLocalId(swPojo.getSwId());// Local ID SW from device
                if (swPojo.getPmId() != null) {
                    if (!swPojo.getPmId().equals("")) {
                        ProjectionMeeting pm = pmService.findById(swPojo.getPmId());
                        sw.setPmId(pm);// set PM ID Relation
                    }
                }
                sw.setWismaId(userPs.getOfficeCode());// set Wisma
                if (swPojo.getCustomerType() != null)
                    sw.setCustomerRegistrationType(swPojo.getCustomerType().charAt(0));// Set Tipe Nasabah (baru/lama)
                sw.setCustomerIdName(swPojo.getIdCardName());// Set Nama di KTP
                sw.setCustomerIdNumber(swPojo.getIdCardNumber());// Set NIK
                if (swPojo.getSurveyDate() != null) {
                    if (!swPojo.getSurveyDate().equals(""))
                        sw.setSurveyDate(formatDateTime.parse(swPojo.getSurveyDate()));// Set Tanggal SW
                }
                // set data profile SW
                SWProfileReq swProfile = swPojo.getProfile();
                if (swPojo.getCustomerId() != null && !swPojo.getCustomerId().equals("")) {
                    if (!swPojo.getCustomerId().equals("")) {
                        sw.setCustomerId(Long.parseLong(swPojo.getCustomerId()));// Set Customer ID jika nasabah lama
                        Customer customer = customerService.findById(swPojo.getCustomerId());
                        if (customer == null) {
                            log.error("UNKNOWN CUSTOMER ID");
                            responseCode.setResponseCode(WebGuiConstant.UNKNOWN_CUSTOMER_ID);
                            String label = getMessage("webservice.rc.label." + WebGuiConstant.UNKNOWN_CUSTOMER_ID);
                            responseCode.setResponseMessage(label);
                            return responseCode;
                        }
                        if (swProfile.getLongName() != null) {
                            if (!swProfile.getLongName().equals("")) {
                                sw.setCustomerName(swProfile.getLongName());// set nama lengkap nasabah
                            } else {
                                if (customer.getCustomerName() != null) {
                                    log.debug("Customer Name from DB : " + customer.getCustomerName());
                                    sw.setCustomerName(customer.getCustomerName());
                                } else {
                                    log.debug("Customer Name from Handset : " + swPojo.getCustomerName());
                                    sw.setCustomerName(swPojo.getCustomerName());
                                }
                            }
                        } else {
                            if (customer.getCustomerName() != null) {
                                log.debug("Customer Name from DB : " + customer.getCustomerName());
                                sw.setCustomerName(customer.getCustomerName());
                            } else {
                                log.debug("Customer Name from Handset : " + swPojo.getCustomerName());
                                sw.setCustomerName(swPojo.getCustomerName());
                            }
                        }
                    }
                } else {
                    sw.setCustomerName(swProfile.getLongName());
                }
                sw.setCustomerAliasName(swProfile.getAlias());// set nama singkat nasabah
                sw.setGender(swProfile.getGender());// set jenis kelamin
                if (swProfile.getBirthDay() != null) {
                    if (!swProfile.getBirthDay().equals(""))
                        sw.setBirthDate(formatter.parse(swProfile.getBirthDay()));// set tanggal lahir
                }
                sw.setBirthPlace(swProfile.getBirthPlace());// set id tempat lahir
                sw.setBirthPlaceRegencyName(swProfile.getBirthPlaceRegencyName());// set nama tempat lahir
                if (swProfile.getIdCardExpired() != null) {
                    if (!swProfile.getIdCardExpired().equals(""))
                        sw.setIdExpiryDate(formatter.parse(swProfile.getIdCardExpired()));// set masa berlaku KTP
                }
                sw.setMaritalStatus(swProfile.getMarriedStatus());// set status perkawinan
                sw.setWorkType(swProfile.getWorkStatus());// set status pekerjaan
                sw.setReligion(swProfile.getReligion());// set agama nasabah
                sw.setPhoneNumber(swProfile.getPhoneNumber());// set nomor telepon nasabah
                sw.setNpwp(swProfile.getNpwp());// set NPWP
                sw.setMotherName(swProfile.getMotherName());// set nama ibu kandung
                sw.setDependentCount(Integer
                        .parseInt(FieldValidationUtil.integerRequestValueValidation(swProfile.getDependants())));// set
                sw.setEducation(swProfile.getEducation());// set pendidikan terakhir nasabah
                sw.setLifeTime(FieldValidationUtil.booleanRequestValueValidation(swProfile.getIsLifeTime()));// set
                SWAddressReq swAddress = swPojo.getAddress();
                sw.setAddress(swAddress.getName());// set alamat
                sw.setRtrw(swAddress.getStreet());// set rtrw
                sw.setPostalCode(swAddress.getPostCode());// set kode pos
                sw.setProvince(swAddress.getProvinceId());// set provinsi
                sw.setCity(swAddress.getRegencyId());// set kota
                sw.setKecamatan(swAddress.getDistrictId());// set kecamatan
                sw.setKelurahan(swAddress.getVillageId());// set kelurahan
                sw.setDomisiliAddress(swAddress.getAdditionalAddress());// set alamat domisili
                if (swAddress.getPlaceOwnerShip() != null)
                    sw.setHouseType(swAddress.getPlaceOwnerShip());// set status kepemilikan rumah
                sw.setHouseCertificate(swAddress.getPlaceCertificate().charAt(0));// set bukti kepemilikan rumah
                // set data pasangan SW
                SWPartnerReq swPartner = swPojo.getPartner();
                sw.setCoupleName(swPartner.getName());// set nama pasangan
                sw.setCoupleBirthPlace(swPartner.getBirthPlaceId());// set id tempat lahir pasangan
                sw.setCoupleBirthPlaceRegencyName(swPartner.getBirthPlace());// set tempat lahir pasangan
                if (swPartner.getBirthDay() != null) {
                    if (!swPartner.getBirthDay().equals("")) {
                        sw.setCoupleBirthDate(formatter.parse(swPartner.getBirthDay()));// set tanggal lahir
                        // pasangan
                    }
                }
                if (swPartner.getJob() != null) {
                    if (!swPartner.getJob().equals("")) {
                        sw.setCoupleJob(swPartner.getJob().charAt(0));// set pekerjaan pasangan
                    }
                }
                // set data usaha SW
                SWBusinessReq swBusiness = swPojo.getBusiness();
                sw.setBusinessField(swBusiness.getType());
                sw.setBusinessDescription(swBusiness.getDesc());
                sw.setBusinessOwnerStatus(swBusiness.getBusinessOwnership().charAt(0));
                sw.setBusinessShariaType(swBusiness.getBusinessShariaCompliance().charAt(0));
                sw.setBusinessName(swBusiness.getName());
                sw.setBusinessLocation(swBusiness.getBusinessLocation().charAt(0));
                sw.setBusinessAddress(swBusiness.getAddress());
                sw.setBusinessAgeYear(Integer
                        .parseInt(FieldValidationUtil.integerRequestValueValidation(swBusiness.getAgeYear())));
                sw.setBusinessAgeMonth(Integer
                        .parseInt(FieldValidationUtil.integerRequestValueValidation(swBusiness.getAgeMonth())));
                sw.setBusinessWorkStatus(swBusiness.getBusinessRunner().charAt(0));
                // set paramater kalkulasi
                SWCalculationVariableReq swCalcVariable = swPojo.getCalcVariable();
                sw.setBusinessCycle(swCalcVariable.getJenisSiklus().charAt(0));
                sw.setBusinessDaysOperation(swCalcVariable.getHariOperasional());
                if (swCalcVariable.getPendapatanLainnya() != null
                        || !swCalcVariable.getPendapatanLainnya().equals(""))
                    sw.setOtherIncome(new BigDecimal(swCalcVariable.getPendapatanLainnya()));
                if (swCalcVariable.getPendapatanRamai() != null || !swCalcVariable.getPendapatanRamai().equals(""))
                    sw.setOneDayBusyIncome(new BigDecimal(swCalcVariable.getPendapatanRamai()));
                if (swCalcVariable.getJumlahhariramai() != null || !swCalcVariable.getJumlahhariramai().equals(""))
                    sw.setTotalDaysInMonthBusy(Integer.parseInt(swCalcVariable.getJumlahhariramai()));
                if (swCalcVariable.getPendapatanSepi() != null || !swCalcVariable.getPendapatanSepi().equals(""))
                    sw.setOneDayLessIncome(new BigDecimal(swCalcVariable.getPendapatanSepi()));
                if (swCalcVariable.getJumlahharisepi() != null || !swCalcVariable.getJumlahharisepi().equals(""))
                    sw.setTotalDaysInMonthLess(Integer.parseInt(swCalcVariable.getJumlahharisepi()));
                if (swCalcVariable.getPendapatanPerbulanRamaiSepi() != null
                        || !swCalcVariable.getPendapatanPerbulanRamaiSepi().equals(""))
                    sw.setTotalLessBusyIncome(new BigDecimal(swCalcVariable.getPendapatanPerbulanRamaiSepi()));
                if (swCalcVariable.getPeriode1() != null || !swCalcVariable.getPeriode1().equals(""))
                    sw.setFirstDayIncome(new BigDecimal(swCalcVariable.getPeriode1()));
                if (swCalcVariable.getPeriode2() != null || !swCalcVariable.getPeriode2().equals(""))
                    sw.setSecondDayIncome(new BigDecimal(swCalcVariable.getPeriode2()));
                if (swCalcVariable.getPeriode3() != null || !swCalcVariable.getPeriode3().equals(""))
                    sw.setThirdDayIncome(new BigDecimal(swCalcVariable.getPeriode3()));
                if (swCalcVariable.getPendapatanPerbulan3Periode() != null
                        || !swCalcVariable.getPendapatanPerbulan3Periode().equals(""))
                    sw.setThreePeriodIncome(new BigDecimal(swCalcVariable.getPendapatanPerbulan3Periode()));
                if (swCalcVariable.getJamBuka() != null) {
                    if (!swCalcVariable.getJamBuka().equals("")) {
                        if (!swCalcVariable.getJamBuka().equals("0"))
                            sw.setOpenTime(
                                    new java.sql.Time(formatTime.parse(swCalcVariable.getJamBuka()).getTime()));
                    }
                }
                if (swCalcVariable.getJamTutup() != null) {
                    if (!swCalcVariable.getJamTutup().equals("")) {
                        if (!swCalcVariable.getJamTutup().equals("0"))
                            sw.setCloseTime(
                                    new java.sql.Time(formatTime.parse(swCalcVariable.getJamTutup()).getTime()));
                    }
                }
                if (swCalcVariable.getWaktuKerja() != null || !swCalcVariable.getWaktuKerja().equals(""))
                    sw.setWorkTime(Integer.parseInt(swCalcVariable.getWaktuKerja()));
                if (swCalcVariable.getWaktukerjaSetelahbuka() != null
                        || !swCalcVariable.getWaktukerjaSetelahbuka().equals(""))
                    sw.setWorkTimeAfterOpen(Integer.parseInt(swCalcVariable.getWaktukerjaSetelahbuka()));
                if (swCalcVariable.getUangkasBoxdiBuka() != null
                        || !swCalcVariable.getUangkasBoxdiBuka().equals(""))
                    sw.setCashOpenTime(new BigDecimal(swCalcVariable.getUangkasBoxdiBuka()));
                if (swCalcVariable.getUangKasBoxSekarang() != null
                        || !swCalcVariable.getUangKasBoxSekarang().equals(""))
                    sw.setCashCurrentTime(new BigDecimal(swCalcVariable.getUangKasBoxSekarang()));
                if (swCalcVariable.getUangBelanja() != null || !swCalcVariable.getUangBelanja().equals(""))
                    sw.setCashExpense(new BigDecimal(swCalcVariable.getUangBelanja()));
                if (swCalcVariable.getPendapatanPerbulanPerkiraanSehari() != null
                        || !swCalcVariable.getPendapatanPerbulanPerkiraanSehari().equals(""))
                    sw.setOneDayPredictIncome(
                            new BigDecimal(swCalcVariable.getPendapatanPerbulanPerkiraanSehari()));
                if (swCalcVariable.getPendapatanPerbulanKasSehari() != null
                        || !swCalcVariable.getPendapatanPerbulanKasSehari().equals(""))
                    sw.setOneDayCashIncome(new BigDecimal(swCalcVariable.getPendapatanPerbulanKasSehari()));
                if (swCalcVariable.getPendapatanRata2TigaPeriode() != null
                        || !swCalcVariable.getPendapatanRata2TigaPeriode().equals(""))
                    sw.setAvgThreePeriodIncome(new BigDecimal(swCalcVariable.getPendapatanRata2TigaPeriode()));
                if (request.getTotalPendapatan() != null || swPojo.getTotalPendapatan() != null
                        || swCalcVariable.getTotalPendapatan() != null
                        || !swCalcVariable.getTotalPendapatan().equals(""))
                    sw.setTotalIncome((new BigDecimal(swCalcVariable.getTotalPendapatan()).setScale(4,
                            BigDecimal.ROUND_HALF_UP)));
                if (swCalcVariable.getPendapatanLainnya() != null
                        || !swCalcVariable.getPendapatanLainnya().equals(""))
                    sw.setOtherIncome(new BigDecimal(swCalcVariable.getPendapatanLainnya()));
                if (swPojo.getIncome() != null || !swPojo.getIncome().equals(""))
                    sw.setTotalIncome(new BigDecimal(swPojo.getIncome()).setScale(4, BigDecimal.ROUND_HALF_UP));
                if (swPojo.getNetIncome() != null || !swPojo.getNetIncome().equals(""))
                    sw.setNettIncome(new BigDecimal(swPojo.getNetIncome()).setScale(4, BigDecimal.ROUND_HALF_UP));
                if (swPojo.getSpending() != null || !swPojo.getSpending().equals(""))
                    sw.setExpenditure(new BigDecimal(swPojo.getSpending()).setScale(4, BigDecimal.ROUND_HALF_UP));
                // set data pengeluaran SW
                SWExpenseReq swExpense = swPojo.getExpense();
                if (swExpense.getTransportUsaha() != null || !swExpense.getTransportUsaha().equals(""))
                    sw.setTransportCost(new BigDecimal(swExpense.getTransportUsaha()));
                if (swExpense.getUtilitasUsaha() != null || !swExpense.getUtilitasUsaha().equals(""))
                    sw.setUtilityCost(new BigDecimal(swExpense.getUtilitasUsaha()));
                if (swExpense.getGajiUsaha() != null || !swExpense.getGajiUsaha().equals(""))
                    sw.setStaffSalary(new BigDecimal(swExpense.getGajiUsaha()));
                if (swExpense.getSewaUsaha() != null || !swExpense.getSewaUsaha().equals(""))
                    sw.setRentCost(new BigDecimal(swExpense.getSewaUsaha()));
                if (swExpense.getMakanNonUsaha() != null || !swExpense.getMakanNonUsaha().equals(""))
                    sw.setFoodCost(new BigDecimal(swExpense.getMakanNonUsaha()));
                if (swExpense.getUtitlitasNonUsaha() != null || !swExpense.getUtitlitasNonUsaha().equals(""))
                    sw.setPrivateUtilityCost(new BigDecimal(swExpense.getUtitlitasNonUsaha()));
                if (swExpense.getPendidikanNonUsaha() != null || !swExpense.getPendidikanNonUsaha().equals(""))
                    sw.setEducationCost(new BigDecimal(swExpense.getPendidikanNonUsaha()));
                if (swExpense.getKesehatanNonUsaha() != null || !swExpense.getKesehatanNonUsaha().equals(""))
                    sw.setHealthCost(new BigDecimal(swExpense.getKesehatanNonUsaha()));
                if (swExpense.getTransportNonUsaha() != null || !swExpense.getTransportNonUsaha().equals(""))
                    sw.setPrivateTransportCost(new BigDecimal(swExpense.getTransportNonUsaha()));
                if (swExpense.getAngsuranNonUsaha() != null || !swExpense.getAngsuranNonUsaha().equals(""))
                    sw.setInstallmentCost(new BigDecimal(swExpense.getAngsuranNonUsaha()));
                if (swExpense.getLainlainNonUsaha() != null || !swExpense.getLainlainNonUsaha().equals(""))
                    sw.setOtherCost(new BigDecimal(swExpense.getLainlainNonUsaha()));
                BigDecimal totalExpense = sw.getTransportCost().add(sw.getUtilityCost()).add(sw.getStaffSalary())
                        .add(sw.getRentCost());
                sw.setBusinessCost(totalExpense);
                sw.setLongitude(request.getLongitude());
                sw.setLatitude(request.getLatitude());
                sw.setRrn(request.getRetrievalReferenceNumber());
                sw.setTotalBuy(sw.getExpenditure().subtract(sw.getBusinessCost()));
                sw.setNonBusinessCost(sw.getFoodCost().add(sw.getPrivateUtilityCost()).add(sw.getEducationCost())
                        .add(sw.getHealthCost()).add(sw.getPrivateTransportCost()).add(sw.getInstallmentCost())
                        .add(sw.getOtherCost()));
                sw.setResidualIncome(sw.getNettIncome().add(sw.getOtherIncome()).subtract(sw.getNonBusinessCost()));
                sw.setIir(sw.getNettIncome().multiply(new BigDecimal(40).divide(new BigDecimal(100)))
                        .subtract(sw.getInstallmentCost()));
                sw.setIdir(sw.getResidualIncome());
                // jika melakukan delete SW set flag is deleted
                if (WebGuiConstant.ACTION_DELETE.equalsIgnoreCase(request.getAction())) {
                    sw.setIsDeleted(true);
                }
                // jika melakukan delete atau update SW set terlebih dahulu SW
                // ID-nya
                if (WebGuiConstant.ACTION_DELETE.equalsIgnoreCase(request.getAction())
                        || WebGuiConstant.ACTION_UPDATE.equalsIgnoreCase(request.getAction())) {
                    if (swPojo.getSwId() != null) {
                        if (!swPojo.getSwId().equals("")) {
                            String localId = swPojo.getSwId();
                            SurveyWawancara temp = swService.findSwByLocalId(localId);
                            if (temp != null && temp.getSwId() != null) {
                                swPojo.setSwId(temp.getSwId().toString());
                            } else {
                                log.error("UNKNOWN SW ID");
                                responseCode.setResponseCode(WebGuiConstant.UNKNOWN_SW_ID);
                                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                                responseCode.setResponseMessage(label);
                                return responseCode;
                            }
                            sw.setSwId(temp.getSwId());
                            swId = temp.getSwId().toString();
                        }
                    } else {
                        log.error("UNKNOWN SW ID");
                        responseCode.setResponseCode(WebGuiConstant.UNKNOWN_SW_ID);
                    }
                }
                // jika sw tersebut sudah di approved
                if (WebGuiConstant.ACTION_DELETE.equalsIgnoreCase(request.getAction())
                        && swService.getSWById(swId).getStatus().equals(WebGuiConstant.STATUS_APPROVED)) {
                    log.error("SW ALREADY APPROVED");
                    responseCode.setResponseCode(WebGuiConstant.RC_ALREADY_APPROVED);
                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(label);
                    return responseCode;
                }
                log.info("FINISH MAPPING SW DATA");
                // jika sw tersebut tidak ada di database Mprospera
                if ((WebGuiConstant.ACTION_DELETE.equalsIgnoreCase(request.getAction())
                        || WebGuiConstant.ACTION_UPDATE.equalsIgnoreCase(request.getAction()))
                        && !swService.isValidSw(swPojo.getSwId())) {
                    log.error("UNKNOWN SW ID");
                    responseCode.setResponseCode(WebGuiConstant.UNKNOWN_SW_ID);
                } else {
                    // Saving data SW ke DB dengan kondisi RRN tidak sama
                    if (!terminalService.isDuplicate(request.getRetrievalReferenceNumber(), "addSW")) {
                        // set status SW baru dengan WAITING FOR APPROVAL
                        sw.setStatus(swPojo.getStatus());
                        swService.save(sw);
                        responseCode.setSwId(swPojo.getSwId());
                        log.info("sw id yang terbentuk : " + sw.getSwId().toString());
                        swId = sw.getSwId().toString();
                        // mapping sw dengan produk
                        for (SWProductListRequest productList : swPojo.getSwProducts()) {
                            SWProductMapping swProductMap = swService
                                    .findProductMapByLocalId(productList.getSwProductId());
                            if (swProductMap == null)
                                swProductMap = new SWProductMapping();
                            ProductIdListResponse productIdListResponse = new ProductIdListResponse();
                            LoanProduct loanProduct = swService.findByProductId(productList.getProductId());
                            log.info("Produk Pembiayaan yang akan disimpan : " + loanProduct.getProductName());
                            swProductMap.setProductId(loanProduct);
                            if (productList.getSelectedPlafon() != null || !productList.getSelectedPlafon().equals(""))
                                swProductMap.setPlafon(new BigDecimal(productList.getSelectedPlafon()));
                            log.info("swId yang akan disimpan : " + swId);
                            swProductMap.setSwId(Long.parseLong(swId));
                            swProductMap
                                    .setRecommendedProductId(Long.parseLong(productList.getRecommendationProductId()));
                            if (productList.getRecommendationSelectedPlafon() != null
                                    || !productList.getRecommendationSelectedPlafon().equals(""))
                                swProductMap.setRecommendedPlafon(
                                        new BigDecimal(productList.getRecommendationSelectedPlafon()));
                            swProductMap.setLocalId(productList.getSwProductId());
                            // jika melakukan delete SW set flag is deleted
                            if ("delete".equals(productList.getAction()))
                                swProductMap.setDeleted(true);
                            // jika melakukan delete atau update SW set terlebih
                            // dahulu SW ID-nya
                            if (WebGuiConstant.ACTION_DELETE.equalsIgnoreCase(productList.getAction())
                                    || WebGuiConstant.ACTION_UPDATE.equalsIgnoreCase(productList.getAction())) {
                                if (null != productList.getSwProductId()) {
                                    if (!productList.getSwProductId().equals("")) {
                                        swProductMap.setMappingId(swProductMap.getMappingId());
                                    }
                                } else {
                                    log.error("SW PRODUCT NULL");
                                    responseCode.setResponseCode(WebGuiConstant.SW_PRODUCT_NULL);
                                }
                            }
                            try {
                                swService.save(swProductMap);
                                productIdListResponse.setProductId(loanProduct.getProductId().toString());
                                swProductId = swProductMap.getLocalId();
                                productIdListResponse.setSwProductId(swProductId);
                                productIdListResponse
                                        .setRecommendationProductId(swProductMap.getRecommendedProductId().toString());
                                productIdListResponses.add(productIdListResponse);
                            } catch (Exception e) {
                                e.printStackTrace();
                                sw.setIsDeleted(true);
                                swService.save(sw);
                                log.error("CREATE SW FAILED");
                                responseCode.setResponseCode(WebGuiConstant.RC_FAILED);
                                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                                responseCode.setResponseMessage(label);
                                return responseCode;
                            }
                        }
                        responseCode.setSwProducts(productIdListResponses);
                        // mapping sw dengan pembelian langsung
                        if (request.getAction().equalsIgnoreCase(WebGuiConstant.ACTION_UPDATE)
                                || request.getAction().equalsIgnoreCase(WebGuiConstant.ACTION_DELETE)) {
                            List<DirectBuyThings> directBuyThingsList = swService.findProductBySwId(sw);
                            for (DirectBuyThings directBuyThings : directBuyThingsList) {
                                swService.deleteDirectBuy(directBuyThings);
                            }
                            for (DirectBuyList directBuy : swPojo.getDirectPurchasing()) {
                                DirectBuyThings newDirectBuy = new DirectBuyThings();
                                newDirectBuy.setNamaBarang(directBuy.getItemName());
                                if (directBuy.getPrice() != null || !directBuy.getPrice().equals(""))
                                    newDirectBuy.setPrice(directBuy.getPrice());
                                if (directBuy.getFrequency() != null || !directBuy.getFrequency().equals(""))
                                    newDirectBuy.setFrequency(Integer.parseInt(directBuy.getFrequency()));
                                if (directBuy.getSellingPrice() != null || !directBuy.getSellingPrice().equals(""))
                                    newDirectBuy.setSellingPrice(directBuy.getSellingPrice());
                                newDirectBuy.setIndex(Integer.parseInt(directBuy.getIndex()));
                                newDirectBuy.setTotalItem(Integer.parseInt(directBuy.getTotalItem()));
                                newDirectBuy.setType(Integer.parseInt(directBuy.getType()));
                                newDirectBuy.setSwId(sw);
                                try {
                                    swService.save(newDirectBuy);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    sw.setIsDeleted(true);
                                    swService.save(sw);
                                    log.error("CREATE SW FAILED");
                                    responseCode.setResponseCode(WebGuiConstant.RC_FAILED);
                                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                                    responseCode.setResponseMessage(label);
                                    return responseCode;
                                }
                            }
                        } else {
                            for (DirectBuyList directBuy : swPojo.getDirectPurchasing()) {
                                DirectBuyThings direct = new DirectBuyThings();
                                direct.setNamaBarang(directBuy.getItemName());
                                if (directBuy.getSellingPrice() != null || !directBuy.getSellingPrice().equals(""))
                                    direct.setPrice(directBuy.getPrice());
                                if (directBuy.getFrequency() != null || !directBuy.getFrequency().equals(""))
                                    direct.setFrequency(Integer.parseInt(directBuy.getFrequency()));
                                if (directBuy.getSellingPrice() != null || !directBuy.getSellingPrice().equals(""))
                                    direct.setSellingPrice(directBuy.getSellingPrice());
                                direct.setIndex(Integer.parseInt(directBuy.getIndex()));
                                direct.setTotalItem(Integer.parseInt(directBuy.getTotalItem()));
                                direct.setType(Integer.parseInt(directBuy.getType()));
                                direct.setSwId(sw);
                                try {
                                    swService.save(direct);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    sw.setIsDeleted(true);
                                    swService.save(sw);
                                    log.error("CREATE SW FAILED");
                                    responseCode.setResponseCode(WebGuiConstant.RC_FAILED);
                                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                                    responseCode.setResponseMessage(label);
                                    return responseCode;
                                }
                            }
                        }
                        // mapping sw dengan pembelian AWGM
                        if (request.getAction().equalsIgnoreCase(WebGuiConstant.ACTION_UPDATE)
                                || request.getAction().equalsIgnoreCase(WebGuiConstant.ACTION_DELETE)) {
                            List<AWGMBuyThings> awgmList = swService.findAwgmProductBySwId(sw);
                            for (AWGMBuyThings awgm : awgmList) {
                                swService.deleteAwgmBuy(awgm);
                            }
                            for (AWGMBuyList awgmBuy : swPojo.getAwgmPurchasing()) {
                                AWGMBuyThings newAwgmBuy = new AWGMBuyThings();
                                newAwgmBuy.setNamaBarang(awgmBuy.getItemName());
                                if (awgmBuy.getFrequency() != null || !awgmBuy.getFrequency().equals(""))
                                    newAwgmBuy.setFrequency(Integer.parseInt(awgmBuy.getFrequency()));
                                if (awgmBuy.getSellingPrice() != null || !awgmBuy.getSellingPrice().equals(""))
                                    newAwgmBuy.setSellPrice(awgmBuy.getSellingPrice());
                                newAwgmBuy.setIndex(Integer.parseInt(awgmBuy.getIndex()));
                                if (awgmBuy.getPrice() != null || !awgmBuy.getPrice().equals(""))
                                    newAwgmBuy.setBuyPrice(awgmBuy.getPrice());
                                newAwgmBuy.setTotal(Integer.parseInt(awgmBuy.getTotalItem()));
                                newAwgmBuy.setType(Integer.parseInt(awgmBuy.getType()));
                                newAwgmBuy.setSwId(sw);
                                try {
                                    swService.save(newAwgmBuy);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    sw.setIsDeleted(true);
                                    swService.save(sw);
                                    log.error("CREATE SW FAILED");
                                    responseCode.setResponseCode(WebGuiConstant.RC_FAILED);
                                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                                    responseCode.setResponseMessage(label);
                                    return responseCode;
                                }
                            }
                        } else {
                            for (AWGMBuyList awgmBuy : swPojo.getAwgmPurchasing()) {
                                AWGMBuyThings awgm = new AWGMBuyThings();
                                awgm.setNamaBarang(awgmBuy.getItemName());
                                if (awgmBuy.getFrequency() != null || !awgmBuy.getFrequency().equals(""))
                                    awgm.setFrequency(Integer.parseInt(awgmBuy.getFrequency()));
                                if (awgmBuy.getSellingPrice() != null || !awgmBuy.getSellingPrice().equals(""))
                                    awgm.setSellPrice(awgmBuy.getSellingPrice());
                                awgm.setIndex(Integer.parseInt(awgmBuy.getIndex()));
                                if (awgmBuy.getPrice() != null || !awgmBuy.getPrice().equals(""))
                                    awgm.setBuyPrice(awgmBuy.getPrice());
                                awgm.setTotal(Integer.parseInt(awgmBuy.getTotalItem()));
                                awgm.setType(Integer.parseInt(awgmBuy.getType()));
                                awgm.setSwId(sw);
                                try {
                                    swService.save(awgm);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    sw.setIsDeleted(true);
                                    swService.save(sw);
                                    log.error("CREATE SW FAILED");
                                    responseCode.setResponseCode(WebGuiConstant.RC_FAILED);
                                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                                    responseCode.setResponseMessage(label);
                                    return responseCode;
                                }
                            }
                        }
                        // mapping sw dengan tetangga nasabah SW
                        if (request.getAction().equalsIgnoreCase(WebGuiConstant.ACTION_UPDATE)
                                || request.getAction().equalsIgnoreCase(WebGuiConstant.ACTION_DELETE)) {
                            List<NeighborRecommendation> refList = swService.findNeighborBySwId(sw);
                            for (NeighborRecommendation reff : refList) {
                                swService.deleteNeigbourReff(reff);
                            }
                            for (ReferenceList neighborReff : swPojo.getReferenceList()) {
                                NeighborRecommendation newNeighbourReff = new NeighborRecommendation();
                                newNeighbourReff.setNeighborName(neighborReff.getName());
                                newNeighbourReff.setNeighborAddress(neighborReff.getAddress());
                                newNeighbourReff.setNeighborRelation(neighborReff.getGoodNeighbour().charAt(0));
                                newNeighbourReff.setNeighborOutstanding(neighborReff.getVisitedByLandshark().charAt(0));
                                newNeighbourReff.setNeighborBusiness(neighborReff.getHaveBusiness().charAt(0));
                                newNeighbourReff.setNeighborRecomend(neighborReff.getRecommended().charAt(0));
                                newNeighbourReff.setSwId(sw);
                                try {
                                    swService.save(newNeighbourReff);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    sw.setIsDeleted(true);
                                    swService.save(sw);
                                    log.error("CREATE SW FAILED");
                                    responseCode.setResponseCode(WebGuiConstant.RC_FAILED);
                                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                                    responseCode.setResponseMessage(label);
                                    return responseCode;
                                }
                            }
                        } else {
                            for (ReferenceList neighborReff : swPojo.getReferenceList()) {
                                NeighborRecommendation neighbor = new NeighborRecommendation();
                                neighbor.setNeighborName(neighborReff.getName());
                                neighbor.setNeighborAddress(neighborReff.getAddress());
                                neighbor.setNeighborRelation(neighborReff.getGoodNeighbour().charAt(0));
                                neighbor.setNeighborOutstanding(neighborReff.getVisitedByLandshark().charAt(0));
                                neighbor.setNeighborBusiness(neighborReff.getHaveBusiness().charAt(0));
                                neighbor.setNeighborRecomend(neighborReff.getRecommended().charAt(0));
                                neighbor.setSwId(sw);
                                try {
                                    swService.save(neighbor);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    sw.setIsDeleted(true);
                                    swService.save(sw);
                                    log.error("CREATE SW FAILED");
                                    responseCode.setResponseCode(WebGuiConstant.RC_FAILED);
                                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                                    responseCode.setResponseMessage(label);
                                    return responseCode;
                                }
                            }
                        }
                        // mapping calculator pendapatan lainnya
                        List<ItemCalcLainReq> pendapatanLainList = swCalcVariable.getItemCalcLains();
                        if (request.getAction().equalsIgnoreCase(WebGuiConstant.ACTION_UPDATE)
                                || request.getAction().equalsIgnoreCase(WebGuiConstant.ACTION_DELETE)) {
                            List<OtherItemCalculation> otherCalcList = swService.findOtherItemCalcBySwId(sw.getSwId());
                            for (OtherItemCalculation otherCalc : otherCalcList) {
                                swService.deleteOtherItemCalc(otherCalc);
                            }
                            for (ItemCalcLainReq pendapatanLain : pendapatanLainList) {
                                OtherItemCalculation newOtherCalc = new OtherItemCalculation();
                                newOtherCalc.setName(pendapatanLain.getName());
                                if (pendapatanLain.getAmount() != null || !pendapatanLain.getAmount().equals(""))
                                    newOtherCalc.setAmount(new BigDecimal(pendapatanLain.getAmount()));
                                if (pendapatanLain.getPeriode() != null || !pendapatanLain.getPeriode().equals(""))
                                    newOtherCalc.setPeriode(new BigDecimal(pendapatanLain.getPeriode()));
                                if (pendapatanLain.getPendapatanPerbulan() != null
                                        || !pendapatanLain.getPendapatanPerbulan().equals(""))
                                    newOtherCalc.setPendapatanPerbulan(
                                            new BigDecimal(pendapatanLain.getPendapatanPerbulan()));
                                newOtherCalc.setSwId(sw.getSwId());
                                try {
                                    swService.save(newOtherCalc);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    sw.setIsDeleted(true);
                                    swService.save(sw);
                                    log.error("CREATE SW FAILED");
                                    responseCode.setResponseCode(WebGuiConstant.RC_FAILED);
                                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                                    responseCode.setResponseMessage(label);
                                    return responseCode;
                                }
                            }
                        } else {
                            for (ItemCalcLainReq pendapatanLain : pendapatanLainList) {
                                OtherItemCalculation otherItemCalc = new OtherItemCalculation();
                                otherItemCalc.setName(pendapatanLain.getName());
                                if (pendapatanLain.getAmount() != null || !pendapatanLain.getAmount().equals(""))
                                    otherItemCalc.setAmount(new BigDecimal(pendapatanLain.getAmount()));
                                if (pendapatanLain.getPeriode() != null || !pendapatanLain.getPeriode().equals(""))
                                    otherItemCalc.setPeriode(new BigDecimal(pendapatanLain.getPeriode()));
                                if (pendapatanLain.getPendapatanPerbulan() != null
                                        || !pendapatanLain.getPendapatanPerbulan().equals(""))
                                    otherItemCalc.setPendapatanPerbulan(
                                            new BigDecimal(pendapatanLain.getPendapatanPerbulan()));
                                otherItemCalc.setSwId(sw.getSwId());
                                try {
                                    swService.save(otherItemCalc);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    sw.setIsDeleted(true);
                                    swService.save(sw);
                                    log.error("CREATE SW FAILED");
                                    responseCode.setResponseCode(WebGuiConstant.RC_FAILED);
                                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                                    responseCode.setResponseMessage(label);
                                    return responseCode;
                                }
                            }
                        }
                    } else {
                        log.error("SW ALREADY EXISTS");
                        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
                        String label = getMessage("webservice.rc.label." + WebGuiConstant.SW_ALREADY_EXIST);
                        responseCode.setResponseMessage(label);
                        responseCode.setSwId(swPojo.getSwId());
                        return responseCode;
                    }
                }
            }
            if (swId != null) {
                if (!swId.equals("")) {
                    SurveyWawancara swResponse = swService.getSWById(swId);
                    if (swResponse != null) {
                        if (swResponse.getLocalId() != null) {
                            responseCode.setSwId(swResponse.getLocalId());
                        }
                    }
                }
            }
            responseCode.setLocalId(request.getLocalId());
            log.info("ADD SW FINISHED");
            String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
            responseCode.setResponseMessage(label);
        } catch (Exception e) {
            log.error("addSW error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("addSW error: " + e.getMessage());
        } finally {
            try {
                log.trace("Try to create Terminal Activity and Audit Log");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_MODY_SW);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.insertRrn("addSW", request.getRetrievalReferenceNumber().trim());
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        log.info("TERMINAL ACTIVITY LOGGING SUCCESS...");
                        log.info("Updating Terminal Activity");
                        // save ke audit log
                        AuditLog auditLog = new AuditLog();
                        auditLog.setActivityType(AuditLog.MODIF_SW);
                        auditLog.setCreatedBy(request.getUsername());
                        auditLog.setCreatedDate(new Date());
                        if (!request.toString().equals(null) && !(request.toString().length() < 1000)
                                || !request.toString().equals("") && !(request.toString().length() < 1000))
                            auditLog.setDescription(
                                    request.toString().substring(0, 1000) + " | " + responseCode.toString());
                        auditLog.setReffNo(request.getRetrievalReferenceNumber());
                        auditLogService.insertAuditLog(auditLog);
                        log.info("AUDIT LOG LOGGING SUCCESS...");
                        log.info("Updating AUDIT LOG");
                    }
                });
                log.info("addSW RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("addSW ERROR terminalActivity, on SW_SUBMIT_DATA_REQUEST : " + e.getMessage());
            }
            return responseCode;
        }
    }

    // APPROVAL SW
    @RequestMapping(value = WebGuiConstant.SW_APPROVAL_DATA_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    SWResponse doApproval(@RequestBody String requestString,
                          @PathVariable("apkVersion") String apkVersion) throws Exception {

        String swId = "";
        Object obj = new JsonUtils().fromJson(requestString, SWRequest.class);
        final SWRequest request = (SWRequest) obj;
        final SWResponse responseCode = new SWResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        // Validasi messaging request dari Android
        try {
            log.info("approvalSW INCOMING MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(), request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei() + " ,sessionKey : " + request.getSessionKey());
            } else {
                // cek sw id yang akan di approve tidak kosong
                if (!request.getSwId().equals("")) {
                    String localId = request.getSwId();
                    SurveyWawancara sw = swService.findSwByLocalId(localId);
                    if (sw == null) {
                        log.error("UNKNOWN SW ID");
                        responseCode.setResponseCode(WebGuiConstant.UNKNOWN_SW_ID);
                        String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                        responseCode.setResponseMessage(label);
                        return responseCode;
                    }
                    User userApproval = userService.findUserByUsername(request.getUsername());
                    if (sw.getStatus().equals(WebGuiConstant.STATUS_APPROVED)) {
                        log.error("SW ALREADY APPROVED");
                        String label = getMessage("webservice.rc.label." + WebGuiConstant.RC_ALREADY_APPROVED);
                        responseCode.setResponseMessage(label);
                        return responseCode;
                    }
                    log.info("STARTING TO APPROVE SW");
                    // jika status sudah approve
                    if (request.getStatus().equals(WebGuiConstant.STATUS_APPROVED)) {
                        sw.setStatus(WebGuiConstant.STATUS_APPROVED);
                        sw.setUpdatedDate(new Date());
                        sw.setUpdatedBy(request.getUsername());
                        if (userApproval.getRoleUser().equals("2")) {
                            sw.setHasApprovedMs(true);
                        }
                        List<SWApprovalHistoryReq> listApproval = request.getHistory();
                        for (SWApprovalHistoryReq approvalHist : listApproval) {
                            SWUserBWMPMapping swUserBwmpMap = swService.findBySwAndUser(sw.getSwId(),
                                    userApproval.getUserId());
                            // jika approval langsung tanpa ada waiting for
                            // approval ke pejabat lainnya
                            if (swUserBwmpMap == null) {
                                swUserBwmpMap = new SWUserBWMPMapping();
                                swUserBwmpMap.setUserId(userApproval.getUserId());
                                swUserBwmpMap.setSwId(sw.getSwId());
                            }
                            if (swUserBwmpMap.getLevel().equals(approvalHist.getLevel())
                                    && !swUserBwmpMap.getUserId().equals(userApproval.getUserId())) {
                                log.error("SW APPROVED FAILED");
                                responseCode.setResponseCode(WebGuiConstant.SW_APPROVED_FAILED);
                                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                                responseCode.setResponseMessage(label);
                                return responseCode;
                            }
                            swUserBwmpMap.setStatus(WebGuiConstant.STATUS_APPROVED);
                            swUserBwmpMap.setCreatedDate(new Date());
                            swUserBwmpMap.setCreatedBy(request.getUsername());
                            if (request.getHistory() != null) {
                                swUserBwmpMap.setLevel(approvalHist.getLevel());
                                swUserBwmpMap.setDate(approvalHist.getDate());
                            }
                            swService.save(swUserBwmpMap);
                        }
                        // jika status reject
                    } else if (request.getStatus().equals(WebGuiConstant.STATUS_REJECTED)) {
                        sw.setStatus(WebGuiConstant.STATUS_REJECTED);
                        sw.setRejectedReason(request.getRejectedReason());
                        // jika status waiting for approval ke pejabat lainnya
                    } else if (request.getStatus().equals(WebGuiConstant.STATUS_WAITING_APPROVAL)) {
                        sw.setStatus(WebGuiConstant.STATUS_WAITING_APPROVAL);
                        if (userApproval.getRoleUser().equals("2")) {
                            sw.setHasApprovedMs(true);
                        }
                        log.info("SET APPROVAL HISTORY");
                        List<SWApprovalHistoryReq> listApproval = request.getHistory();
                        for (SWApprovalHistoryReq approvalHist : listApproval) {
                            SWUserBWMPMapping swUserBwmpMap = swService.findBySwAndLevel(sw.getSwId(),
                                    approvalHist.getLevel());
                            if (swUserBwmpMap == null)
                                swUserBwmpMap = new SWUserBWMPMapping();
                            swUserBwmpMap.setSwId(sw.getSwId());
                            if (approvalHist.getSupervisorId() != null) {
                                swUserBwmpMap.setUserId(Long.parseLong(approvalHist.getSupervisorId()));
                            } else {
                                swUserBwmpMap.setUserId(userApproval.getUserId());
                            }
                            swUserBwmpMap.setCreatedBy(request.getUsername());
                            swUserBwmpMap.setCreatedDate(new Date());
                            swUserBwmpMap.setLevel(approvalHist.getLevel());
                            swUserBwmpMap.setDate(approvalHist.getDate());
                            swUserBwmpMap.setStatus(WebGuiConstant.STATUS_WAITING_APPROVAL);
                            swService.save(swUserBwmpMap);
                        }
                    } else {
                        log.error("UNKNOWN SW ID");
                        responseCode.setResponseCode(WebGuiConstant.UNKNOWN_SW_ID);
                        String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                        responseCode.setResponseMessage(label);
                    }
                    swId = sw.getLocalId();
                    swService.save(sw);
                    // mapping sw dengan customer existing
                    if (sw.getSwId() != null) {
                        if (customerService.getBySwId(sw.getSwId()) == null) {
                            if (sw.getCustomerId() != null) {
                                Customer cust = customerService.findById(sw.getCustomerId().toString());
                                if (cust != null) {
                                    cust.setCustomerId(cust.getCustomerId());
                                    cust.setSwId(sw.getSwId());
                                    cust.setApprovedDate(new Date());
                                    cust.setCustomerName(sw.getCustomerName());
                                    customerService.save(cust);
                                }
                            }
                        }
                    }
                } else {
                    log.error("UNKNOWN SW ID");
                    responseCode.setResponseCode(WebGuiConstant.UNKNOWN_SW_ID);
                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(label);
                    return responseCode;
                }
            }
            String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
            responseCode.setResponseMessage(label);
            responseCode.setSwId(swId);
        } catch (Exception e) {
            log.error("approvalSW ERROR  : " + e.getMessage(), e);
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("approvalSW error: " + e.getMessage());
        } finally {
            try {
                log.trace("Try to create Terminal Activity and Audit Log");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_MODY_SW);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        log.info("TERMINAL ACTIVITY LOGGING SUCCESS...");
                        log.info("Updating Terminal Activity");
                        AuditLog auditLog = new AuditLog();
                        auditLog.setActivityType(AuditLog.MODIF_SW);
                        auditLog.setCreatedBy(request.getUsername());
                        auditLog.setCreatedDate(new Date());
                        if (!request.toString().equals(null) && !(request.toString().length() < 1000)
                                || !request.toString().equals("") && !(request.toString().length() < 1000))
                            auditLog.setDescription(
                                    request.toString().substring(0, 1000) + " | " + responseCode.toString());
                        auditLog.setReffNo(request.getRetrievalReferenceNumber());
                        auditLogService.insertAuditLog(auditLog);
                        log.info("AUDIT LOG LOGGING SUCCESS...");
                    }
                });
                log.info("approvalSW RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("approvalSW saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return responseCode;
        }
    }

    // SINKRONISASI SW
    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_SW_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    SWResponse doList(@RequestBody final SWRequest request,
                      @PathVariable("apkVersion") String apkVersion) throws Exception {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        String countData = request.getGetCountData();
        String page = request.getPage();
        String startLookupDate = request.getStartLookupDate();
        String endLookupDate = request.getEndLookupDate();
        final SWResponse responseCode = new SWResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("listSW INCOMING MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(), request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                log.info("Validation success, get Survey Wawancara data");
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                Timer timer = new Timer();
                timer.start("[" + WebGuiConstant.TERMINAL_GET_SW_REQUEST + "] [swService.getSW]");
                final Page<SurveyWawancara> swPage = swService.getSWByUser(request.getUsername(), page, countData,
                        startLookupDate, endLookupDate);
                log.info("Finishing get Survey Wawancara data");
                timer.stop();
                final List<ListSWResponse> swResponses = new ArrayList<ListSWResponse>();
                for (final SurveyWawancara sw : swPage) {
                    ListSWResponse swPojo = new ListSWResponse();
                    swPojo.setSwId(FieldValidationUtil.stringRequestValueValidation(sw.getLocalId()).toString());
                    swPojo.setDayLight(sw.getTime());
                    swPojo.setSwLocation(sw.getSwLocation());
                    // get wisma SW
                    WismaSWReq wisma = new WismaSWReq();
                    Location wismaSw = areaService.findLocationById(sw.getWismaId());
                    if (wismaSw != null) {
                        wisma.setOfficeId(wismaSw.getLocationId());
                        wisma.setOfficeCode(wismaSw.getLocationCode());
                        wisma.setOfficeName(wismaSw.getName());
                        swPojo.setWisma(wisma);
                    }
                    swPojo.setCustomerType(
                            FieldValidationUtil.charValueResponseValidation(sw.getCustomerRegistrationType()));
                    if (sw.getSurveyDate() != null)
                        swPojo.setSurveyDate(formatDateTime.format(sw.getSurveyDate()));
                    // get list produk pembiayaan SW
                    List<SWProductListResponse> swProductList = new ArrayList<>();
                    List<SWProductMapping> productList = swService.findProductMapBySwId(sw.getSwId());
                    for (SWProductMapping product : productList) {
                        SWProductListResponse swProduct = new SWProductListResponse();
                        swProduct.setSwProductId(
                                FieldValidationUtil.stringRequestValueValidation(product.getLocalId()).toString());
                        swProduct.setProductId(product.getProductId().getProductId().toString());
                        swProduct.setSelectedPlafon(
                                FieldValidationUtil.bigDecimalValueResponseValidation(product.getPlafon()));
                        swProduct.setRecommendationProductId(product.getProductId().getProductId().toString());
                        swProduct.setRecommendationSelectedPlafon(
                                FieldValidationUtil.bigDecimalValueResponseValidation(product.getRecommendedPlafon()));
                        swProductList.add(swProduct);
                    }
                    swPojo.setSwProducts(swProductList);
                    List<AP3R> ap3rList = ap3RService.findAp3rListBySwId(sw.getSwId());
                    // get profil SW
                    SWProfileResponse profile = new SWProfileResponse();
                    if (sw.getCustomerId() != null) {
                        swPojo.setCustomerId(sw.getCustomerId().toString());
                        Customer customer = customerService.findById(sw.getCustomerId().toString());
                        if (customer != null) {
                            profile.setLongName(customer.getCustomerName());
                            swPojo.setCustomerCif(customer.getCustomerCifNumber());
                            swPojo.setSentraId(customer.getGroup().getSentra().getSentraId().toString());
                            swPojo.setSentraName(customer.getGroup().getSentra().getSentraName());
                            swPojo.setGroupId(customer.getGroup().getGroupId().toString());
                            swPojo.setGroupName(customer.getGroup().getGroupName());
                            swPojo.setCustomerExists("true");
                            for (AP3R ap3r : ap3rList) {
                                Loan loan = loanService.findByAp3rAndCustomer(ap3r.getAp3rId(), customer);
                                if (loan != null) {
                                    swPojo.setLoanExists("true");
                                } else {
                                    swPojo.setLoanExists("false");
                                }
                            }
                        }
                    } else {
                        swPojo.setCustomerExists("false");
                    }
                    swPojo.setIdCardNumber(sw.getCustomerIdNumber());
                    profile.setAlias(sw.getCustomerAliasName());
                    swPojo.setIdCardName(sw.getCustomerIdName());
                    profile.setGender(FieldValidationUtil.stringToZeroValidation(sw.getGender()));
                    profile.setReligion(FieldValidationUtil.stringToZeroValidation(sw.getReligion()));
                    profile.setBirthPlace(sw.getBirthPlace());
                    profile.setBirthPlaceRegencyName(sw.getBirthPlaceRegencyName());
                    if (sw.getBirthDate() != null)
                        profile.setBirthDay(formatter.format(sw.getBirthDate()));
                    if (sw.getIdExpiryDate() != null) {
                        profile.setIdCardExpired(formatter.format(sw.getIdExpiryDate()));
                    } else {
                        profile.setIdCardExpired("");
                    }
                    profile.setMarriedStatus(FieldValidationUtil.stringToZeroValidation(sw.getMaritalStatus()));
                    profile.setLongName(sw.getCustomerName());
                    profile.setWorkStatus(FieldValidationUtil.stringToZeroValidation(sw.getWorkType()));
                    if (sw.getCreatedDate() != null)
                        swPojo.setCreatedAt(formatter.format(sw.getCreatedDate()));
                    profile.setPhoneNumber(sw.getPhoneNumber());
                    profile.setNpwp(sw.getNpwp());
                    profile.setMotherName(sw.getMotherName());
                    profile.setDependants(FieldValidationUtil.integerValueResponseValidation(sw.getDependentCount()));
                    profile.setEducation(FieldValidationUtil.stringToZeroValidation(sw.getEducation()));
                    profile.setIsLifeTime(FieldValidationUtil.booleanResponseValueValidation(sw.getLifeTime()));
                    swPojo.setProfile(profile);
                    // get alamat SW
                    SwAddressResponse address = new SwAddressResponse();
                    if (sw.getSwLocation() != null)
                        address.setName(sw.getSwLocation());
                    else
                        address.setName(sw.getAddress());
                    address.setStreet(sw.getRtrw());
                    address.setPostCode(sw.getPostalCode());
                    address.setVillageId(FieldValidationUtil.stringToZeroValidation(sw.getKelurahan()));
                    address.setRegencyId(FieldValidationUtil.stringToZeroValidation(sw.getKecamatan()));
                    address.setProvinceId(FieldValidationUtil.stringToZeroValidation(sw.getProvince()));
                    address.setDistrictId(FieldValidationUtil.stringToZeroValidation(sw.getCity()));
                    address.setAdditionalAddress(sw.getDomisiliAddress());
                    address.setPlaceOwnerShip(sw.getHouseType());
                    address.setPlaceCertificate(
                            FieldValidationUtil.charValueResponseValidation(sw.getHouseCertificate()));
                    swPojo.setAddress(address);
                    // get pasangan SW
                    SWPartnerResponse partner = new SWPartnerResponse();
                    partner.setName(sw.getCoupleName());
                    partner.setBirthPlace(sw.getCoupleBirthPlaceRegencyName());
                    partner.setBirthPlaceId(sw.getCoupleBirthPlace());
                    if (sw.getCoupleBirthDate() != null)
                        partner.setBirthDay(formatter.format(sw.getCoupleBirthDate()));
                    if (sw.getCoupleJob() != null) {
                        if (sw.getCoupleJob().equals('K')) {
                            partner.setJob("Karyawan");
                        } else if (sw.getCoupleJob().equals('W')) {
                            partner.setJob("Wiraswasta");
                        } else if (sw.getCoupleJob().equals('M')) {
                            partner.setJob("Musiman");
                        } else {
                            partner.setJob("");
                        }
                    } else {
                        partner.setJob("");
                    }
                    swPojo.setPartner(partner);
                    // get usaha SW
                    SWBusinessResponse business = new SWBusinessResponse();
                    business.setType(sw.getBusinessField());
                    business.setDesc(sw.getBusinessDescription());
                    business.setBusinessOwnership(
                            FieldValidationUtil.charValueResponseValidation(sw.getBusinessOwnerStatus()));
                    business.setBusinessShariaCompliance(
                            FieldValidationUtil.charValueResponseValidation(sw.getBusinessShariaType()));
                    business.setBusinessLocation(
                            FieldValidationUtil.charValueResponseValidation(sw.getBusinessLocation()));
                    business.setName(sw.getBusinessName());
                    business.setAddress(sw.getBusinessAddress());
                    business.setAgeYear(FieldValidationUtil.integerValueResponseValidation(sw.getBusinessAgeYear()));
                    business.setAgeMonth(FieldValidationUtil.integerValueResponseValidation(sw.getBusinessAgeMonth()));
                    business.setBusinessRunner(
                            FieldValidationUtil.charValueResponseValidation(sw.getBusinessWorkStatus()));
                    swPojo.setBusiness(business);
                    // get parameter kalkulasi
                    SWCalculationVariable calcVariable = new SWCalculationVariable();
                    calcVariable.setJenisSiklus(FieldValidationUtil.charValueResponseValidation(sw.getBusinessCycle()));
                    calcVariable.setHariOperasional(sw.getBusinessDaysOperation());
                    calcVariable.setPendapatanRamai(
                            FieldValidationUtil.bigDecimalValueResponseValidation(sw.getOneDayBusyIncome()).toString());
                    calcVariable.setJumlahhariramai(
                            FieldValidationUtil.integerValueResponseValidation(sw.getTotalDaysInMonthBusy()));
                    calcVariable.setPendapatanSepi(
                            FieldValidationUtil.bigDecimalValueResponseValidation(sw.getOneDayLessIncome()).toString());
                    calcVariable.setJumlahharisepi(
                            FieldValidationUtil.integerValueResponseValidation(sw.getTotalDaysInMonthLess()));
                    calcVariable.setPendapatanPerbulanRamaiSepi(FieldValidationUtil
                            .bigDecimalValueResponseValidation(sw.getTotalLessBusyIncome()).toString());
                    if (sw.getFirstDayIncome() != null)
                        calcVariable.setPeriode1(sw.getFirstDayIncome().toString());
                    if (sw.getSecondDayIncome() != null)
                        calcVariable.setPeriode2(sw.getSecondDayIncome().toString());
                    if (sw.getThirdDayIncome() != null)
                        calcVariable.setPeriode3(sw.getThirdDayIncome().toString());
                    calcVariable.setPendapatanPerbulan3Periode(FieldValidationUtil
                            .bigDecimalValueResponseValidation(sw.getThreePeriodIncome()).toString());
                    calcVariable.setPendapatanRata2TigaPeriode(FieldValidationUtil
                            .bigDecimalValueResponseValidation(sw.getAvgThreePeriodIncome()).toString());
                    if (sw.getOpenTime() != null)
                        calcVariable.setJamBuka(formatTime.format(sw.getOpenTime()));
                    if (sw.getCloseTime() != null)
                        calcVariable.setJamTutup(formatTime.format(sw.getCloseTime()));
                    calcVariable.setWaktuKerja(FieldValidationUtil.integerValueResponseValidation(sw.getWorkTime()));
                    calcVariable.setWaktukerjaSetelahbuka(
                            FieldValidationUtil.integerValueResponseValidation(sw.getWorkTimeAfterOpen()));
                    if (sw.getCashOpenTime() != null)
                        calcVariable.setUangkasBoxdiBuka(sw.getCashOpenTime().toString());
                    if (sw.getCashCurrentTime() != null)
                        calcVariable.setUangKasBoxSekarang(sw.getCashCurrentTime().toString());
                    if (sw.getCashExpense() != null)
                        calcVariable.setUangBelanja(sw.getCashExpense().toString());
                    calcVariable.setPendapatanPerbulanPerkiraanSehari(FieldValidationUtil
                            .bigDecimalValueResponseValidation(sw.getOneDayPredictIncome()).toString());
                    calcVariable.setPendapatanPerbulanKasSehari(
                            FieldValidationUtil.bigDecimalValueResponseValidation(sw.getOneDayCashIncome()).toString());
                    List<OtherItemCalculation> itemPendapatanLainList = swService.findOtherItemCalcBySwId(sw.getSwId());
                    List<ItemCalcLainReq> itemCalcLainList = new ArrayList<>();
                    for (OtherItemCalculation itemPendapatanLain : itemPendapatanLainList) {
                        ItemCalcLainReq itemCalcLain = new ItemCalcLainReq();
                        itemCalcLain.setName(itemPendapatanLain.getName());
                        itemCalcLain.setAmount(FieldValidationUtil
                                .bigDecimalValueResponseValidation(itemPendapatanLain.getAmount()).toString());
                        if (itemPendapatanLain.getPeriode() != null)
                            itemCalcLain.setPeriode(itemPendapatanLain.getPeriode().toString());
                        else
                            itemCalcLain.setPeriode("0");
                        itemCalcLain.setPendapatanPerbulan(FieldValidationUtil
                                .bigDecimalValueResponseValidation(itemPendapatanLain.getPendapatanPerbulan())
                                .toString());
                        itemCalcLainList.add(itemCalcLain);
                    }
                    calcVariable.setPendapatanLainnya(
                            FieldValidationUtil.bigDecimalValueResponseValidation(sw.getOtherIncome()).toString());
                    calcVariable.setItemCalcLains(itemCalcLainList);
                    swPojo.setCreatedBy(sw.getCreatedBy());
                    swPojo.setCalcVariable(calcVariable);
                    List<DirectBuyList> directBuyList = new ArrayList<>();
                    List<DirectBuyThings> directBuyProductList = swService.findProductBySwId(sw);
                    for (DirectBuyThings directBuyProduct : directBuyProductList) {
                        DirectBuyList product = new DirectBuyList();
                        product.setFrequency(
                                FieldValidationUtil.integerValueResponseValidation(directBuyProduct.getFrequency()));
                        product.setIndex(
                                FieldValidationUtil.integerValueResponseValidation(directBuyProduct.getIndex()));
                        product.setItemName(directBuyProduct.getNamaBarang());
                        product.setPrice(
                                FieldValidationUtil.bigDecimalValueResponseValidation(directBuyProduct.getPrice()));
                        product.setSellingPrice(FieldValidationUtil
                                .bigDecimalValueResponseValidation(directBuyProduct.getSellingPrice()));
                        product.setTotalItem(directBuyProduct.getTotalItem().toString());
                        product.setType(FieldValidationUtil.integerValueResponseValidation(directBuyProduct.getType()));
                        directBuyList.add(product);
                    }
                    swPojo.setDirectPurchasing(directBuyList);
                    List<AWGMBuyList> awgmBuyList = new ArrayList<>();
                    List<AWGMBuyThings> awgmBuyProductList = swService.findAwgmProductBySwId(sw);
                    for (AWGMBuyThings awgmBuyProduct : awgmBuyProductList) {
                        AWGMBuyList product = new AWGMBuyList();
                        product.setFrequency(
                                FieldValidationUtil.integerValueResponseValidation(awgmBuyProduct.getFrequency()));
                        product.setIndex(FieldValidationUtil.integerValueResponseValidation(awgmBuyProduct.getIndex()));
                        product.setItemName(awgmBuyProduct.getNamaBarang());
                        product.setPrice(
                                FieldValidationUtil.bigDecimalValueResponseValidation(awgmBuyProduct.getBuyPrice()));
                        product.setSellingPrice(
                                FieldValidationUtil.bigDecimalValueResponseValidation(awgmBuyProduct.getSellPrice()));
                        product.setTotalItem(awgmBuyProduct.getTotal().toString());
                        product.setType(FieldValidationUtil.integerValueResponseValidation(awgmBuyProduct.getType()));
                        awgmBuyList.add(product);
                    }
                    swPojo.setAwgmPurchasing(awgmBuyList);
                    SWExpenseResponse expense = new SWExpenseResponse();
                    if (sw.getTransportCost() != null)
                        expense.setTransportUsaha(sw.getTransportCost().toString());
                    if (sw.getUtilityCost() != null)
                        expense.setUtilitasUsaha(sw.getUtilityCost().toString());
                    if (sw.getStaffSalary() != null)
                        expense.setGajiUsaha(sw.getStaffSalary().toString());
                    if (sw.getRentCost() != null)
                        expense.setSewaUsaha(sw.getRentCost().toString());
                    if (sw.getPrivateTransportCost() != null)
                        expense.setTransportNonUsaha(sw.getPrivateTransportCost().toString());
                    if (sw.getPrivateUtilityCost() != null)
                        expense.setUtitlitasNonUsaha(sw.getPrivateUtilityCost().toString());
                    if (sw.getEducationCost() != null)
                        expense.setPendidikanNonUsaha(sw.getEducationCost().toString());
                    if (sw.getHealthCost() != null)
                        expense.setKesehatanNonUsaha(sw.getHealthCost().toString());
                    if (sw.getFoodCost() != null)
                        expense.setMakanNonUsaha(sw.getFoodCost().toString());
                    if (sw.getInstallmentCost() != null)
                        expense.setAngsuranNonUsaha(sw.getInstallmentCost().toString());
                    if (sw.getOtherCost() != null)
                        expense.setLainlainNonUsaha(sw.getOtherCost().toString());
                    swPojo.setExpense(expense);
                    List<ReferenceList> referenceList = new ArrayList<>();
                    List<NeighborRecommendation> neighborList = swService.findNeighborBySwId(sw);
                    for (NeighborRecommendation neighbor : neighborList) {
                        ReferenceList reference = new ReferenceList();
                        reference.setName(neighbor.getNeighborName());
                        reference.setAddress(neighbor.getNeighborAddress());
                        reference.setGoodNeighbour("" + neighbor.getNeighborRelation());
                        if (reference.getGoodNeighbour().equals("t")) {
                            reference.setGoodNeighbour("true");
                        } else {
                            reference.setGoodNeighbour("false");
                        }
                        reference.setVisitedByLandshark("" + neighbor.getNeighborOutstanding());
                        if (reference.getVisitedByLandshark().equals("t")) {
                            reference.setVisitedByLandshark("true");
                        } else {
                            reference.setVisitedByLandshark("false");
                        }
                        reference.setHaveBusiness("" + neighbor.getNeighborBusiness());
                        if (reference.getHaveBusiness().equals("t")) {
                            reference.setHaveBusiness("true");
                        } else {
                            reference.setHaveBusiness("false");
                        }
                        reference.setRecommended("" + neighbor.getNeighborRecomend());
                        if (reference.getRecommended().equals("t")) {
                            reference.setRecommended("true");
                        } else {
                            reference.setRecommended("false");
                        }
                        referenceList.add(reference);
                    }
                    swPojo.setReferenceList(referenceList);
                    swPojo.setStatus(sw.getStatus());
                    if (sw.getPmId() != null)
                        swPojo.setPmId(sw.getPmId().getPmId().toString());
                    List<SWApprovalHistory> swApprovalList = new ArrayList<>();
                    List<SWUserBWMPMapping> swUserBwmpList = swService.findBySw(sw.getSwId());
                    for (SWUserBWMPMapping swUserBwmp : swUserBwmpList) {
                        SWApprovalHistory swApproval = new SWApprovalHistory();
                        User userApproval = userService.loadUserByUserId(swUserBwmp.getUserId());
                        if (userApproval != null) {
                            swApproval.setLevel(swUserBwmp.getLevel());
                            swApproval.setLimit(FieldValidationUtil.stringToZeroValidation(userApproval.getLimit()));
                            swApproval.setRole(userApproval.getRoleName());
                            swApproval.setName(userApproval.getName());
                            swApproval.setDate(swUserBwmp.getDate());
                            swApproval.setTanggal(formatDateTime.format(swUserBwmp.getCreatedDate()));
                            swApproval.setStatus(swUserBwmp.getStatus());
                            swApproval.setSupervisorId(userApproval.getUserId().toString());
                            swApprovalList.add(swApproval);
                        }
                    }
                    swPojo.setApprovalHistories(swApprovalList);
                    SwIdPhoto swIdPhoto = swService.getSwIdPhoto(sw.getSwId().toString());
                    if (swIdPhoto != null)
                        swPojo.setHasIdPhoto("true");
                    else
                        swPojo.setHasIdPhoto("false");
                    SwSurveyPhoto swSurveyPhoto = swService.getSwSurveyPhoto(sw.getSwId().toString());
                    if (swSurveyPhoto != null)
                        swPojo.setHasBusinessPlacePhoto("true");
                    else
                        swPojo.setHasBusinessPlacePhoto("false");
                    if (sw.getHasApprovedMs() != null) {
                        if (sw.getHasApprovedMs().equals(true))
                            swPojo.setHasApprovedMs("true");
                        else
                            swPojo.setHasApprovedMs("false");
                    } else
                        swPojo.setHasApprovedMs("false");
                    swResponses.add(swPojo);
                }
                log.info("Finishing create response Survey Wawancara data");
                log.info("Try to Update Terminal");
                Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                terminal.setSwProgress(PercentageSynchronizer.processSyncPercent(request.getPage(),
                        String.valueOf(swPage.getTotalPages()), terminal.getSwProgress()));
                terminalService.updateTerminal(terminal);
                log.info("Finishing Update Terminal");
                final List<SurveyWawancara> listDeletedSw = swService.findIsDeletedSwList();
                List<String> deletedSwList = new ArrayList<>();
                for (SurveyWawancara deletedSw : listDeletedSw) {
                    String id = deletedSw.getLocalId();
                    deletedSwList.add(id);
                }
                responseCode.setDeletedSwList(deletedSwList);
                responseCode.setCurrentTotal(String.valueOf(swPage.getContent().size()));
                responseCode.setGrandTotal(String.valueOf(swPage.getTotalElements()));
                responseCode.setTotalPage(String.valueOf(swPage.getTotalPages()));
                responseCode.setSwList(swResponses);
            }
        } catch (Exception e) {
            log.error("listSW error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("listSW error: " + e.getMessage());
        } finally {
            try {
                log.info("Try to create Terminal Activity and Audit Log");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_SW);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        log.info("TERMINAL ACTIVITY LOGGING SUCCESS...");
                        log.info("Updating Terminal Activity");
                    }
                });
                log.info("listSW RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("listSW saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return responseCode;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_LOAN_PRODUCT_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    ProductListResponse doProductList(@RequestBody final ProductListRequest request,
                                      @PathVariable("apkVersion") String apkVersion) throws Exception {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        String page = request.getPage().toString();
        String countData = request.getGetCountData().toString();
        final ProductListResponse responseCode = new ProductListResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("listProduct INCOMING MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                log.info("Validation success, get Loan Product data");
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                final Page<LoanProduct> productPage = swService.getProduct(page, countData);
                log.info("Finishing get Loan Product data");
                final List<LoanProductResponse> productResponses = new ArrayList<LoanProductResponse>();
                for (final LoanProduct loanProduct : productPage) {
                    LoanProductResponse product = new LoanProductResponse();
                    product.setProductId(loanProduct.getProductId());
                    product.setProductName(loanProduct.getProductName());
                    product.setJumlahAngsur(loanProduct.getInstallmentCount());
                    product.setTenorMinggu(loanProduct.getTenor());
                    product.setTipePembiayaan(loanProduct.getLoanType());
                    product.setFrekuensiAngsurSatuan(loanProduct.getInstallmentFreqTime());
                    product.setFrekuensiAngsur(loanProduct.getInstallmentFreqCount());
                    product.setStatus(loanProduct.getStatus());
                    if (loanProduct.getMargin() != null)
                        product.setMarjin(loanProduct.getMargin().abs(MathContext.DECIMAL32));
                    else
                        product.setMarjin(loanProduct.getMargin().ZERO);
                    List<ProductPlafond> plafonList = swService.findByProductId(loanProduct);
                    List<BigDecimal> plafonNominalList = new ArrayList<>();
                    for (ProductPlafond plafon : plafonList) {
                        BigDecimal nominal;
                        nominal = plafon.getPlafond();
                        if (plafon.getPlafond() == null) {
                            nominal = BigDecimal.ZERO;
                        }
                        plafonNominalList.add(nominal);
                    }
                    product.setPlafon(plafonNominalList);
                    product.setProductRate(loanProduct.getProductRate());
                    productResponses.add(product);
                }
                log.info("Finishing create response Loan Product data");
                responseCode.setCurrentTotal(String.valueOf(productPage.getContent().size()));
                responseCode.setGrandTotal(String.valueOf(productPage.getTotalElements()));
                responseCode.setTotalPage(String.valueOf(productPage.getTotalPages()));
                responseCode.setProductList(productResponses);
            }
        } catch (Exception e) {
            log.error("listProduct error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("listProduct error: " + e.getMessage());
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_listProduct);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("Finishing create Terminal Activity");
                log.info("listProduct RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("listProduct saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return responseCode;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_BUSINESS_TYPE_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    BusinessTypeListResponse doBusinessTypeList(@RequestBody final BusinessTypeListRequest request,
                                                @PathVariable("apkVersion") String apkVersion) throws Exception {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        String page = request.getPage().toString();
        String countData = request.getGetCountData().toString();
        final BusinessTypeListResponse responseCode = new BusinessTypeListResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("listBusinessType INCOMING MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(), request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                log.info("Validation success, get Business Type data");
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                final Page<BusinessType> businessTypePage = swService.getBusinessType(page, countData);
                log.info("Finishing get Business Type data");
                final List<BusinessTypeList> businessTypeResponses = new ArrayList<BusinessTypeList>();
                for (final BusinessType businessType : businessTypePage) {
                    BusinessTypeList business = new BusinessTypeList();
                    business.setId(businessType.getBusinessId().toString());
                    business.setBusinessType(businessType.getBusinessType());
                    businessTypeResponses.add(business);
                }
                responseCode.setBusinessTypeList(businessTypeResponses);
                log.info("Finishing create response Business Type data");
                responseCode.setCurrentTotal(String.valueOf(businessTypePage.getContent().size()));
                responseCode.setGrandTotal(String.valueOf(businessTypePage.getTotalElements()));
                responseCode.setTotalPage(String.valueOf(businessTypePage.getTotalPages()));
            }
        } catch (ParseException e) {
            log.error("listBusinessType error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("listBusinessType error: " + e.getMessage());
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_listBusinessType);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("Finishing create Terminal Activity");
                log.info("listBusinessType RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("listBusinessType saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return responseCode;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_SUBMIT_ID_PHOTO_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    PhotoResponse submitIdPhoto(@RequestBody final PhotoRequest request,
                                @PathVariable("apkVersion") String apkVersion) {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final PhotoResponse response = new PhotoResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("submitIdPhoto INCOMING MESSAGE");
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("trying to submit ID photo ");
                String localId = request.getSwId();
                SurveyWawancara temp = swService.findSwByLocalId(localId);
                if (temp == null) {
                    response.setResponseCode(WebGuiConstant.UNKNOWN_SW_ID);
                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelFailed);
                    return response;
                }
                SwIdPhoto idPhoto = swService.getSwIdPhoto(temp.getSwId().toString());
                if (terminalService.isDuplicate(request.getRetrievalReferenceNumber(), "submitIdPhoto")
                        && idPhoto != null) {
                    label = getMessage("webservice.rc.label." + WebGuiConstant.PHOTO_ALREADY_EXIST);
                    response.setResponseMessage(label);
                }
                if (idPhoto == null)
                    idPhoto = new SwIdPhoto();
                idPhoto.setSwId(temp);
                idPhoto.setIdPhoto(request.getIdPhoto().getBytes());
                swService.save(idPhoto);
            }
        } catch (Exception e) {
            log.error("submitIdPhoto error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("submitIdPhoto error: " + e.getMessage());
        } finally {
            try {
                log.info("submitIdPhoto Try to Create TerminalActivity.....");
                // save ke terminal activity
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SUBMIT_submitIdPhoto);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.insertRrn("submitIdPhoto", request.getRetrievalReferenceNumber().trim());
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        log.info("TERMINAL ACTIVITY LOGGING SUCCESS...");
                        log.info("Updating Terminal Activity");
                    }
                });
                log.info("submitIdPhoto RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("submitIdPhoto saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_SUBMIT_BUSINESS_PLACE_PHOTO_REQUEST, method = {
            RequestMethod.POST})
    public @ResponseBody
    PhotoResponse submitBusinessPlacePhoto(@RequestBody final PhotoRequest request,
                                           @PathVariable("apkVersion") String apkVersion) {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final PhotoResponse response = new PhotoResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("submitBusinessPlacePhoto INCOMING MESSAGE");
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.info("trying to submit Business Place photo ");
                String localId = request.getSwId();
                SurveyWawancara temp = swService.findSwByLocalId(localId);
                if (temp == null) {
                    response.setResponseCode(WebGuiConstant.UNKNOWN_SW_ID);
                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelFailed);
                    return response;
                }
                SwSurveyPhoto surveyPhoto = swService.getSwSurveyPhoto(temp.getSwId().toString());
                if (terminalService.isDuplicate(request.getRetrievalReferenceNumber(), "submitBusinessPhoto")
                        && surveyPhoto != null) {
                    label = getMessage("webservice.rc.label." + WebGuiConstant.PHOTO_ALREADY_EXIST);
                    response.setResponseMessage(label);
                }
                if (surveyPhoto == null)
                    surveyPhoto = new SwSurveyPhoto();
                surveyPhoto.setSwId(temp);
                if (request.getBusinessPlacePhoto() != null)
                    surveyPhoto.setSurveyPhoto(request.getBusinessPlacePhoto().getBytes());
                swService.save(surveyPhoto);
            }
        } catch (Exception e) {
            log.error("submitBusinessPlacePhoto error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("submitBusinessPlacePhoto error: " + e.getMessage());
        } finally {
            try {
                log.info("Trying to Create Terminal Activity");
                // save ke terminal activity
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SUBMIT_submitBusinessPlacePhoto);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.insertRrn("submitBusinessPhoto", request.getRetrievalReferenceNumber().trim());
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        log.info("TERMINAL ACTIVITY LOGGING SUCCESS...");
                        log.info("Updating Terminal Activity");
                    }
                });
                log.info("submitBusinessPlacePhoto RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            } catch (Exception e) {
                log.error("submitBusinessPlacePhoto saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return response;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_USER_NON_MS_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    UserNonMSResponse syncUserNonMs(@RequestBody final UserNonMSReq request,
                                    @PathVariable("apkVersion") String versionNumber) throws KeyManagementException, NoSuchAlgorithmException {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final UserNonMSResponse responseCode = new UserNonMSResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("syncUserNonMs INCOMING MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, versionNumber);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.info("Validation success, get user Non MS");
                List<User> userNonMS = userService.findUserNonMS();
                List<UserNonMSPojo> userPojoList = new ArrayList<>();
                for (User user : userNonMS) {
                    UserNonMSPojo userPojo = new UserNonMSPojo();
                    userPojo.setUserId(user.getUserId().toString());
                    userPojo.setName(user.getName());
                    userPojo.setRole(user.getRoleName());
                    userPojo.setLimit(user.getLimit());
                    userPojoList.add(userPojo);
                }
                responseCode.setUserList(userPojoList);
            }
        } catch (Exception e) {
            log.error("syncUserNonMs error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("syncUserNonMs error: " + e.getMessage());
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_syncUserNonMs);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("syncUserNonMs RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("syncUserNonMs saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return responseCode;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_SW_NON_MS_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    SWResponse doListNonMs(@RequestBody final SWRequest request,
                           @PathVariable("apkVersion") String apkVersion) throws Exception {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        String countData = request.getGetCountData();
        String page = request.getPage();
        String startLookupDate = request.getStartLookupDate();
        String endLookupDate = request.getEndLookupDate();
        final SWResponse responseCode = new SWResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("listSwNonMs INCOMING MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                log.info("Validation success, get Survey Wawancara data");
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                final Page<SurveyWawancara> swPage = swService.getSWNonMS(page, countData, startLookupDate, endLookupDate);
                log.info("Finishing get Survey Wawancara data");
                User userLogin = userService.findUserByUsername(request.getUsername());
                final List<ListSWResponse> swResponses = new ArrayList<ListSWResponse>();
                for (final SurveyWawancara sw : swPage) {
                    if (sw.getStatus().equals(WebGuiConstant.STATUS_WAITING_APPROVAL)) {
                        SWUserBWMPMapping swUserBwmpMap = swService.findBySwAndUser(sw.getSwId(),
                                userLogin.getUserId());
                        if (swUserBwmpMap != null) {
                            ListSWResponse swPojo = new ListSWResponse();
                            swPojo.setSwId(sw.getLocalId().toString());
                            swPojo.setDayLight(sw.getTime());
                            WismaSWReq wisma = new WismaSWReq();
                            Location wismaSw = areaService.findLocationById(sw.getWismaId());
                            if (wismaSw != null) {
                                wisma.setOfficeId(wismaSw.getLocationId());
                                wisma.setOfficeCode(wismaSw.getLocationCode());
                                wisma.setOfficeName(wismaSw.getName());
                                swPojo.setWisma(wisma);
                            }
                            swPojo.setCustomerType(
                                    FieldValidationUtil.charValueResponseValidation(sw.getCustomerRegistrationType()));
                            if (sw.getSurveyDate() != null)
                                swPojo.setSurveyDate(formatDateTime.format(sw.getSurveyDate()));
                            // get list produk pembiayaan SW
                            List<SWProductListResponse> swProductList = new ArrayList<>();
                            List<SWProductMapping> productList = swService.findProductMapBySwId(sw.getSwId());
                            for (SWProductMapping product : productList) {
                                SWProductListResponse swProduct = new SWProductListResponse();
                                swProduct.setSwProductId(FieldValidationUtil
                                        .stringRequestValueValidation(product.getLocalId()).toString());
                                swProduct.setProductId(product.getProductId().getProductId().toString());
                                swProduct.setSelectedPlafon(
                                        FieldValidationUtil.bigDecimalValueResponseValidation(product.getPlafon()));
                                swProduct.setRecommendationProductId(product.getProductId().getProductId().toString());
                                swProduct.setRecommendationSelectedPlafon(FieldValidationUtil
                                        .bigDecimalValueResponseValidation(product.getRecommendedPlafon()));
                                swProductList.add(swProduct);
                            }
                            swPojo.setSwProducts(swProductList);
                            // get profil SW
                            SWProfileResponse profile = new SWProfileResponse();
                            if (sw.getCustomerId() != null) {
                                swPojo.setCustomerId(sw.getCustomerId().toString());
                                Customer customer = customerService.findById(sw.getCustomerId().toString());
                                profile.setLongName(customer.getCustomerName());
                                swPojo.setCustomerCif(customer.getCustomerCifNumber());
                                swPojo.setSentraId(customer.getGroup().getSentra().getSentraId().toString());
                                swPojo.setSentraName(customer.getGroup().getSentra().getSentraName());
                                swPojo.setGroupId(customer.getGroup().getGroupId().toString());
                                swPojo.setGroupName(customer.getGroup().getGroupName());
                            }
                            swPojo.setIdCardNumber(sw.getCustomerIdNumber());
                            profile.setAlias(sw.getCustomerAliasName());
                            swPojo.setIdCardName(sw.getCustomerIdName());
                            profile.setGender(FieldValidationUtil.stringToZeroValidation(sw.getGender()));
                            profile.setReligion(FieldValidationUtil.stringToZeroValidation(sw.getReligion()));
                            profile.setBirthPlace(sw.getBirthPlace());
                            profile.setBirthPlaceRegencyName(sw.getBirthPlaceRegencyName());
                            if (sw.getBirthDate() != null)
                                profile.setBirthDay(formatter.format(sw.getBirthDate()));
                            if (sw.getIdExpiryDate() != null) {
                                profile.setIdCardExpired(formatter.format(sw.getIdExpiryDate()));
                            } else {
                                profile.setIdCardExpired("");
                            }
                            profile.setMarriedStatus(FieldValidationUtil.stringToZeroValidation(sw.getMaritalStatus()));
                            profile.setLongName(sw.getCustomerName());
                            profile.setWorkStatus(FieldValidationUtil.stringToZeroValidation(sw.getWorkType()));
                            if (sw.getCreatedDate() != null)
                                swPojo.setCreatedAt(formatter.format(sw.getCreatedDate()));
                            profile.setPhoneNumber(sw.getPhoneNumber());
                            profile.setNpwp(sw.getNpwp());
                            profile.setMotherName(sw.getMotherName());
                            profile.setDependants(
                                    FieldValidationUtil.integerValueResponseValidation(sw.getDependentCount()));
                            profile.setEducation(FieldValidationUtil.stringToZeroValidation(sw.getEducation()));
                            profile.setIsLifeTime(FieldValidationUtil.booleanResponseValueValidation(sw.getLifeTime()));
                            swPojo.setProfile(profile);
                            // get alamat SW
                            SwAddressResponse address = new SwAddressResponse();
                            if (sw.getSwLocation() != null)
                                address.setName(sw.getSwLocation());
                            else
                                address.setName(sw.getAddress());
                            address.setStreet(sw.getRtrw());
                            address.setPostCode(sw.getPostalCode());
                            address.setVillageId(FieldValidationUtil.stringToZeroValidation(sw.getKelurahan()));
                            address.setRegencyId(FieldValidationUtil.stringToZeroValidation(sw.getKecamatan()));
                            address.setProvinceId(FieldValidationUtil.stringToZeroValidation(sw.getProvince()));
                            address.setDistrictId(FieldValidationUtil.stringToZeroValidation(sw.getCity()));
                            address.setAdditionalAddress(sw.getDomisiliAddress());
                            address.setPlaceOwnerShip(sw.getHouseType());
                            address.setPlaceCertificate(
                                    FieldValidationUtil.charValueResponseValidation(sw.getHouseCertificate()));
                            swPojo.setAddress(address);
                            // get pasangan SW
                            SWPartnerResponse partner = new SWPartnerResponse();
                            partner.setName(sw.getCoupleName());
                            partner.setBirthPlace(sw.getCoupleBirthPlaceRegencyName());
                            partner.setBirthPlaceId(sw.getCoupleBirthPlace());
                            if (sw.getCoupleBirthDate() != null)
                                partner.setBirthDay(formatter.format(sw.getCoupleBirthDate()));
                            if (sw.getCoupleJob() != null) {
                                if (sw.getCoupleJob().equals('K')) {
                                    partner.setJob("Karyawan");
                                } else if (sw.getCoupleJob().equals('W')) {
                                    log.debug("Job : " + sw.getCoupleJob());
                                    partner.setJob("Wiraswasta");
                                } else if (sw.getCoupleJob().equals('M')) {
                                    log.debug("Job : " + sw.getCoupleJob());
                                    partner.setJob("Musiman");
                                } else {
                                    partner.setJob("");
                                }
                            } else {
                                partner.setJob("");
                            }
                            swPojo.setPartner(partner);
                            // get usaha SW
                            SWBusinessResponse business = new SWBusinessResponse();
                            business.setType(sw.getBusinessField());
                            business.setDesc(sw.getBusinessDescription());
                            business.setBusinessOwnership(
                                    FieldValidationUtil.charValueResponseValidation(sw.getBusinessOwnerStatus()));
                            business.setBusinessShariaCompliance(
                                    FieldValidationUtil.charValueResponseValidation(sw.getBusinessShariaType()));
                            business.setBusinessLocation(
                                    FieldValidationUtil.charValueResponseValidation(sw.getBusinessLocation()));
                            business.setName(sw.getBusinessName());
                            business.setAddress(sw.getBusinessAddress());
                            business.setAgeYear(
                                    FieldValidationUtil.integerValueResponseValidation(sw.getBusinessAgeYear()));
                            business.setAgeMonth(
                                    FieldValidationUtil.integerValueResponseValidation(sw.getBusinessAgeMonth()));
                            business.setBusinessRunner(
                                    FieldValidationUtil.charValueResponseValidation(sw.getBusinessWorkStatus()));
                            swPojo.setBusiness(business);
                            // get parameter kalkulasi
                            SWCalculationVariable calcVariable = new SWCalculationVariable();
                            calcVariable.setJenisSiklus(
                                    FieldValidationUtil.charValueResponseValidation(sw.getBusinessCycle()));
                            calcVariable.setHariOperasional(sw.getBusinessDaysOperation());
                            calcVariable.setPendapatanRamai(FieldValidationUtil
                                    .bigDecimalValueResponseValidation(sw.getOneDayBusyIncome()).toString());
                            calcVariable.setJumlahhariramai(
                                    FieldValidationUtil.integerValueResponseValidation(sw.getTotalDaysInMonthBusy()));
                            calcVariable.setPendapatanSepi(FieldValidationUtil
                                    .bigDecimalValueResponseValidation(sw.getOneDayLessIncome()).toString());
                            calcVariable.setJumlahharisepi(
                                    FieldValidationUtil.integerValueResponseValidation(sw.getTotalDaysInMonthLess()));
                            calcVariable.setPendapatanPerbulanRamaiSepi(FieldValidationUtil
                                    .bigDecimalValueResponseValidation(sw.getTotalLessBusyIncome()).toString());
                            if (sw.getFirstDayIncome() != null)
                                calcVariable.setPeriode1(sw.getFirstDayIncome().toString());
                            if (sw.getSecondDayIncome() != null)
                                calcVariable.setPeriode2(sw.getSecondDayIncome().toString());
                            if (sw.getThirdDayIncome() != null)
                                calcVariable.setPeriode3(sw.getThirdDayIncome().toString());
                            calcVariable.setPendapatanPerbulan3Periode(FieldValidationUtil
                                    .bigDecimalValueResponseValidation(sw.getThreePeriodIncome()).toString());
                            calcVariable.setPendapatanRata2TigaPeriode(FieldValidationUtil
                                    .bigDecimalValueResponseValidation(sw.getAvgThreePeriodIncome()).toString());
                            if (sw.getOpenTime() != null)
                                calcVariable.setJamBuka(formatTime.format(sw.getOpenTime()));
                            if (sw.getCloseTime() != null)
                                calcVariable.setJamTutup(formatTime.format(sw.getCloseTime()));
                            calcVariable.setWaktuKerja(
                                    FieldValidationUtil.integerValueResponseValidation(sw.getWorkTime()));
                            calcVariable.setWaktukerjaSetelahbuka(
                                    FieldValidationUtil.integerValueResponseValidation(sw.getWorkTimeAfterOpen()));
                            if (sw.getCashOpenTime() != null)
                                calcVariable.setUangkasBoxdiBuka(sw.getCashOpenTime().toString());
                            if (sw.getCashCurrentTime() != null)
                                calcVariable.setUangKasBoxSekarang(sw.getCashCurrentTime().toString());
                            if (sw.getCashExpense() != null)
                                calcVariable.setUangBelanja(sw.getCashExpense().toString());
                            calcVariable.setPendapatanPerbulanPerkiraanSehari(FieldValidationUtil
                                    .bigDecimalValueResponseValidation(sw.getOneDayPredictIncome()).toString());
                            calcVariable.setPendapatanPerbulanKasSehari(FieldValidationUtil
                                    .bigDecimalValueResponseValidation(sw.getOneDayCashIncome()).toString());
                            List<OtherItemCalculation> itemPendapatanLainList = swService
                                    .findOtherItemCalcBySwId(sw.getSwId());
                            List<ItemCalcLainReq> itemCalcLainList = new ArrayList<>();
                            for (OtherItemCalculation itemPendapatanLain : itemPendapatanLainList) {
                                ItemCalcLainReq itemCalcLain = new ItemCalcLainReq();
                                itemCalcLain.setName(itemPendapatanLain.getName());
                                itemCalcLain.setAmount(FieldValidationUtil
                                        .bigDecimalValueResponseValidation(itemPendapatanLain.getAmount()).toString());
                                if (itemPendapatanLain.getPeriode() != null)
                                    itemCalcLain.setPeriode(itemPendapatanLain.getPeriode().toString());
                                else
                                    itemCalcLain.setPeriode("0");
                                itemCalcLain.setPendapatanPerbulan(FieldValidationUtil
                                        .bigDecimalValueResponseValidation(itemPendapatanLain.getPendapatanPerbulan())
                                        .toString());
                                itemCalcLainList.add(itemCalcLain);
                            }
                            calcVariable.setPendapatanLainnya(FieldValidationUtil
                                    .bigDecimalValueResponseValidation(sw.getOtherIncome()).toString());
                            calcVariable.setItemCalcLains(itemCalcLainList);
                            swPojo.setCreatedBy(sw.getCreatedBy());
                            swPojo.setCalcVariable(calcVariable);
                            List<DirectBuyList> directBuyList = new ArrayList<>();
                            List<DirectBuyThings> directBuyProductList = swService.findProductBySwId(sw);
                            for (DirectBuyThings directBuyProduct : directBuyProductList) {
                                DirectBuyList product = new DirectBuyList();
                                product.setFrequency(FieldValidationUtil
                                        .integerValueResponseValidation(directBuyProduct.getFrequency()));
                                product.setIndex(FieldValidationUtil
                                        .integerValueResponseValidation(directBuyProduct.getIndex()));
                                product.setItemName(directBuyProduct.getNamaBarang());
                                product.setPrice(FieldValidationUtil
                                        .bigDecimalValueResponseValidation(directBuyProduct.getPrice()));
                                product.setSellingPrice(FieldValidationUtil
                                        .bigDecimalValueResponseValidation(directBuyProduct.getSellingPrice()));
                                product.setTotalItem(directBuyProduct.getTotalItem().toString());
                                product.setType(
                                        FieldValidationUtil.integerValueResponseValidation(directBuyProduct.getType()));
                                directBuyList.add(product);
                            }
                            swPojo.setDirectPurchasing(directBuyList);
                            List<AWGMBuyList> awgmBuyList = new ArrayList<>();
                            List<AWGMBuyThings> awgmBuyProductList = swService.findAwgmProductBySwId(sw);
                            for (AWGMBuyThings awgmBuyProduct : awgmBuyProductList) {
                                AWGMBuyList product = new AWGMBuyList();
                                product.setFrequency(FieldValidationUtil
                                        .integerValueResponseValidation(awgmBuyProduct.getFrequency()));
                                product.setIndex(
                                        FieldValidationUtil.integerValueResponseValidation(awgmBuyProduct.getIndex()));
                                product.setItemName(awgmBuyProduct.getNamaBarang());
                                product.setPrice(FieldValidationUtil
                                        .bigDecimalValueResponseValidation(awgmBuyProduct.getBuyPrice()));
                                product.setSellingPrice(FieldValidationUtil
                                        .bigDecimalValueResponseValidation(awgmBuyProduct.getSellPrice()));
                                product.setTotalItem(awgmBuyProduct.getTotal().toString());
                                product.setType(
                                        FieldValidationUtil.integerValueResponseValidation(awgmBuyProduct.getType()));
                                awgmBuyList.add(product);
                            }
                            swPojo.setAwgmPurchasing(awgmBuyList);
                            SWExpenseResponse expense = new SWExpenseResponse();
                            if (sw.getTransportCost() != null)
                                expense.setTransportUsaha(sw.getTransportCost().toString());
                            if (sw.getUtilityCost() != null)
                                expense.setUtilitasUsaha(sw.getUtilityCost().toString());
                            if (sw.getStaffSalary() != null)
                                expense.setGajiUsaha(sw.getStaffSalary().toString());
                            if (sw.getRentCost() != null)
                                expense.setSewaUsaha(sw.getRentCost().toString());
                            if (sw.getPrivateTransportCost() != null)
                                expense.setTransportNonUsaha(sw.getPrivateTransportCost().toString());
                            if (sw.getPrivateUtilityCost() != null)
                                expense.setUtitlitasNonUsaha(sw.getPrivateUtilityCost().toString());
                            if (sw.getEducationCost() != null)
                                expense.setPendidikanNonUsaha(sw.getEducationCost().toString());
                            if (sw.getHealthCost() != null)
                                expense.setKesehatanNonUsaha(sw.getHealthCost().toString());
                            if (sw.getFoodCost() != null)
                                expense.setMakanNonUsaha(sw.getFoodCost().toString());
                            if (sw.getInstallmentCost() != null)
                                expense.setAngsuranNonUsaha(sw.getInstallmentCost().toString());
                            if (sw.getOtherCost() != null)
                                expense.setLainlainNonUsaha(sw.getOtherCost().toString());
                            swPojo.setExpense(expense);
                            List<ReferenceList> referenceList = new ArrayList<>();
                            List<NeighborRecommendation> neighborList = swService.findNeighborBySwId(sw);
                            for (NeighborRecommendation neighbor : neighborList) {
                                ReferenceList reference = new ReferenceList();
                                reference.setName(neighbor.getNeighborName());
                                reference.setAddress(neighbor.getNeighborAddress());
                                reference.setGoodNeighbour("" + neighbor.getNeighborRelation());
                                if (reference.getGoodNeighbour().equals("t")) {
                                    reference.setGoodNeighbour("true");
                                } else {
                                    reference.setGoodNeighbour("false");
                                }
                                reference.setVisitedByLandshark("" + neighbor.getNeighborOutstanding());
                                if (reference.getVisitedByLandshark().equals("t")) {
                                    reference.setVisitedByLandshark("true");
                                } else {
                                    reference.setVisitedByLandshark("false");
                                }
                                reference.setHaveBusiness("" + neighbor.getNeighborBusiness());
                                if (reference.getHaveBusiness().equals("t")) {
                                    reference.setHaveBusiness("true");
                                } else {
                                    reference.setHaveBusiness("false");
                                }
                                reference.setRecommended("" + neighbor.getNeighborRecomend());
                                if (reference.getRecommended().equals("t")) {
                                    reference.setRecommended("true");
                                } else {
                                    reference.setRecommended("false");
                                }
                                referenceList.add(reference);
                            }
                            swPojo.setReferenceList(referenceList);
                            swPojo.setStatus(sw.getStatus());
                            if (sw.getPmId() != null)
                                swPojo.setPmId(sw.getPmId().getPmId().toString());
                            List<SWApprovalHistory> swApprovalList = new ArrayList<>();
                            List<SWUserBWMPMapping> swUserBwmpList = swService.findBySw(sw.getSwId());
                            for (SWUserBWMPMapping swUserBwmp : swUserBwmpList) {
                                SWApprovalHistory swApproval = new SWApprovalHistory();
                                User userApproval = userService.loadUserByUserId(swUserBwmp.getUserId());
                                if (userApproval != null) {
                                    swApproval.setLevel(swUserBwmp.getLevel());
                                    if (swUserBwmp.getCreatedDate() != null)
                                        swApproval.setTanggal(formatDateTime.format(swUserBwmp.getCreatedDate()));
                                    swApproval.setLimit(
                                            FieldValidationUtil.stringToZeroValidation(userApproval.getLimit()));
                                    swApproval.setName(userApproval.getName());
                                    swApproval.setSwId(sw.getSwId().toString());
                                    swApproval.setRole(userApproval.getRoleName());
                                    swApproval.setDate(swUserBwmp.getDate());
                                    swApproval.setStatus(swUserBwmp.getStatus());
                                    swApproval.setSupervisorId(userApproval.getUserId().toString());
                                    swApprovalList.add(swApproval);
                                }
                            }
                            swPojo.setApprovalHistories(swApprovalList);
                            SwIdPhoto swIdPhoto = swService.getSwIdPhoto(sw.getSwId().toString());
                            if (swIdPhoto != null)
                                swPojo.setHasIdPhoto("true");
                            else
                                swPojo.setHasIdPhoto("false");
                            SwSurveyPhoto swSurveyPhoto = swService.getSwSurveyPhoto(sw.getSwId().toString());
                            if (swSurveyPhoto != null)
                                swPojo.setHasBusinessPlacePhoto("true");
                            else
                                swPojo.setHasBusinessPlacePhoto("false");
                            if (sw.getHasApprovedMs() != null) {
                                if (sw.getHasApprovedMs().equals(true))
                                    swPojo.setHasApprovedMs("true");
                                else
                                    swPojo.setHasApprovedMs("false");
                            } else
                                swPojo.setHasApprovedMs("false");
                            swResponses.add(swPojo);
                        }
                    }
                }
                log.info("Finishing create response Survey Wawancara data");
                log.info("Try to Update Terminal");
                Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                terminal.setSwProgress(PercentageSynchronizer.processSyncPercent(request.getPage(),
                        String.valueOf(swPage.getTotalPages()), terminal.getSwProgress()));
                terminalService.updateTerminal(terminal);
                log.info("Finishing Update Terminal");
                final List<SurveyWawancara> listDeletedSw = swService.findIsDeletedSwList();
                List<String> deletedSwList = new ArrayList<>();
                for (SurveyWawancara deletedSw : listDeletedSw) {
                    String id = deletedSw.getLocalId().toString();
                    deletedSwList.add(id);
                }
                responseCode.setDeletedSwList(deletedSwList);
                responseCode.setSwList(swResponses);
                responseCode.setCurrentTotal(String.valueOf(swPage.getContent().size()));
                responseCode.setGrandTotal(String.valueOf(swPage.getTotalElements()));
                responseCode.setTotalPage(String.valueOf(swPage.getTotalPages()));
            }
        } catch (ParseException e) {
            log.error("listSwNonMs error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("listSwNonMs error: " + e.getMessage());
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_listSwNonMs);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("Finishing create Terminal Activity");
                log.info("Send Response to Device");
                log.info("listSwNonMs RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("listSwNonMs saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return responseCode;
        }
    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_SW_NON_MS_DEVIATION_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    SWResponse doListNonMsDeviation(@RequestBody final SWRequest request,
                                    @PathVariable("apkVersion") String apkVersion) throws Exception {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        String countData = request.getGetCountData();
        String page = request.getPage();
        String startLookupDate = request.getStartLookupDate();
        String endLookupDate = request.getEndLookupDate();
        final SWResponse responseCode = new SWResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("listSwNonMsDeviation INCOMING MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                log.info("Validation success, get Survey Wawancara data");
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                final Page<SurveyWawancara> swPage = swService.getSWNonMS(page, countData, startLookupDate, endLookupDate);
                log.info("Finishing get Survey Wawancara data");
                User userLogin = userService.findUserByUsername(request.getUsername());
                final List<ListSWResponse> swResponses = new ArrayList<ListSWResponse>();
                for (final SurveyWawancara sw : swPage) {
                    if (sw.getStatus().equals(WebGuiConstant.STATUS_WAITING_APPROVAL)) {
                        SWUserBWMPMapping swUserBwmpMap = swService.findBySwAndUser(sw.getSwId(),
                                userLogin.getUserId());
                        if (swUserBwmpMap != null) {
                            ListSWResponse swPojo = new ListSWResponse();
                            swPojo.setSwId(sw.getLocalId().toString());
                            swPojo.setDayLight(sw.getTime());
                            WismaSWReq wisma = new WismaSWReq();
                            Location wismaSw = areaService.findLocationById(sw.getWismaId());
                            if (wismaSw != null) {
                                wisma.setOfficeId(wismaSw.getLocationId());
                                wisma.setOfficeCode(wismaSw.getLocationCode());
                                wisma.setOfficeName(wismaSw.getName());
                                swPojo.setWisma(wisma);
                            }
                            swPojo.setCustomerType(
                                    FieldValidationUtil.charValueResponseValidation(sw.getCustomerRegistrationType()));
                            if (sw.getSurveyDate() != null)
                                swPojo.setSurveyDate(formatDateTime.format(sw.getSurveyDate()));
                            // get list produk pembiayaan SW
                            List<SWProductListResponse> swProductList = new ArrayList<>();
                            List<SWProductMapping> productList = swService.findProductMapBySwId(sw.getSwId());
                            for (SWProductMapping product : productList) {
                                SWProductListResponse swProduct = new SWProductListResponse();
                                swProduct.setSwProductId(FieldValidationUtil
                                        .stringRequestValueValidation(product.getLocalId()).toString());
                                swProduct.setProductId(product.getProductId().getProductId().toString());
                                swProduct.setSelectedPlafon(
                                        FieldValidationUtil.bigDecimalValueResponseValidation(product.getPlafon()));
                                swProduct.setRecommendationProductId(product.getProductId().getProductId().toString());
                                swProduct.setRecommendationSelectedPlafon(FieldValidationUtil
                                        .bigDecimalValueResponseValidation(product.getRecommendedPlafon()));
                                swProductList.add(swProduct);
                            }
                            swPojo.setSwProducts(swProductList);
                            // get profil SW
                            SWProfileResponse profile = new SWProfileResponse();
                            if (sw.getCustomerId() != null) {
                                swPojo.setCustomerId(sw.getCustomerId().toString());
                                Customer customer = customerService.findById(sw.getCustomerId().toString());
                                profile.setLongName(customer.getCustomerName());
                                swPojo.setCustomerCif(customer.getCustomerCifNumber());
                                swPojo.setSentraId(customer.getGroup().getSentra().getSentraId().toString());
                                swPojo.setSentraName(customer.getGroup().getSentra().getSentraName());
                                swPojo.setGroupId(customer.getGroup().getGroupId().toString());
                                swPojo.setGroupName(customer.getGroup().getGroupName());
                            }
                            swPojo.setIdCardNumber(sw.getCustomerIdNumber());
                            profile.setAlias(sw.getCustomerAliasName());
                            swPojo.setIdCardName(sw.getCustomerIdName());
                            profile.setGender(FieldValidationUtil.stringToZeroValidation(sw.getGender()));
                            profile.setReligion(FieldValidationUtil.stringToZeroValidation(sw.getReligion()));
                            profile.setBirthPlace(sw.getBirthPlace());
                            profile.setBirthPlaceRegencyName(sw.getBirthPlaceRegencyName());
                            if (sw.getBirthDate() != null)
                                profile.setBirthDay(formatter.format(sw.getBirthDate()));
                            if (sw.getIdExpiryDate() != null) {
                                profile.setIdCardExpired(formatter.format(sw.getIdExpiryDate()));
                            } else {
                                profile.setIdCardExpired("");
                            }
                            profile.setMarriedStatus(FieldValidationUtil.stringToZeroValidation(sw.getMaritalStatus()));
                            profile.setLongName(sw.getCustomerName());
                            profile.setWorkStatus(FieldValidationUtil.stringToZeroValidation(sw.getWorkType()));
                            if (sw.getCreatedDate() != null)
                                swPojo.setCreatedAt(formatter.format(sw.getCreatedDate()));
                            profile.setPhoneNumber(sw.getPhoneNumber());
                            profile.setNpwp(sw.getNpwp());
                            profile.setMotherName(sw.getMotherName());
                            profile.setDependants(
                                    FieldValidationUtil.integerValueResponseValidation(sw.getDependentCount()));
                            profile.setEducation(FieldValidationUtil.stringToZeroValidation(sw.getEducation()));
                            profile.setIsLifeTime(FieldValidationUtil.booleanResponseValueValidation(sw.getLifeTime()));
                            swPojo.setProfile(profile);
                            // get alamat SW
                            SwAddressResponse address = new SwAddressResponse();
                            if (sw.getSwLocation() != null)
                                address.setName(sw.getSwLocation());
                            else
                                address.setName(sw.getAddress());
                            address.setStreet(sw.getRtrw());
                            address.setPostCode(sw.getPostalCode());
                            address.setVillageId(FieldValidationUtil.stringToZeroValidation(sw.getKelurahan()));
                            address.setRegencyId(FieldValidationUtil.stringToZeroValidation(sw.getKecamatan()));
                            address.setProvinceId(FieldValidationUtil.stringToZeroValidation(sw.getProvince()));
                            address.setDistrictId(FieldValidationUtil.stringToZeroValidation(sw.getCity()));
                            address.setAdditionalAddress(sw.getDomisiliAddress());
                            address.setPlaceOwnerShip(sw.getHouseType());
                            address.setPlaceCertificate(
                                    FieldValidationUtil.charValueResponseValidation(sw.getHouseCertificate()));
                            swPojo.setAddress(address);
                            // get pasangan SW
                            SWPartnerResponse partner = new SWPartnerResponse();
                            partner.setName(sw.getCoupleName());
                            partner.setBirthPlace(sw.getCoupleBirthPlaceRegencyName());
                            partner.setBirthPlaceId(sw.getCoupleBirthPlace());
                            if (sw.getCoupleBirthDate() != null)
                                partner.setBirthDay(formatter.format(sw.getCoupleBirthDate()));
                            if (sw.getCoupleJob() != null) {
                                if (sw.getCoupleJob().equals('K')) {
                                    partner.setJob("Karyawan");
                                } else if (sw.getCoupleJob().equals('W')) {
                                    log.debug("Job : " + sw.getCoupleJob());
                                    partner.setJob("Wiraswasta");
                                } else if (sw.getCoupleJob().equals('M')) {
                                    log.debug("Job : " + sw.getCoupleJob());
                                    partner.setJob("Musiman");
                                } else {
                                    partner.setJob("");
                                }
                            } else {
                                partner.setJob("");
                            }
                            swPojo.setPartner(partner);
                            // get usaha SW
                            SWBusinessResponse business = new SWBusinessResponse();
                            business.setType(sw.getBusinessField());
                            business.setDesc(sw.getBusinessDescription());
                            business.setBusinessOwnership(
                                    FieldValidationUtil.charValueResponseValidation(sw.getBusinessOwnerStatus()));
                            business.setBusinessShariaCompliance(
                                    FieldValidationUtil.charValueResponseValidation(sw.getBusinessShariaType()));
                            business.setBusinessLocation(
                                    FieldValidationUtil.charValueResponseValidation(sw.getBusinessLocation()));
                            business.setName(sw.getBusinessName());
                            business.setAddress(sw.getBusinessAddress());
                            business.setAgeYear(
                                    FieldValidationUtil.integerValueResponseValidation(sw.getBusinessAgeYear()));
                            business.setAgeMonth(
                                    FieldValidationUtil.integerValueResponseValidation(sw.getBusinessAgeMonth()));
                            business.setBusinessRunner(
                                    FieldValidationUtil.charValueResponseValidation(sw.getBusinessWorkStatus()));
                            swPojo.setBusiness(business);
                            // get parameter kalkulasi
                            SWCalculationVariable calcVariable = new SWCalculationVariable();
                            calcVariable.setJenisSiklus(
                                    FieldValidationUtil.charValueResponseValidation(sw.getBusinessCycle()));
                            calcVariable.setHariOperasional(sw.getBusinessDaysOperation());
                            calcVariable.setPendapatanRamai(FieldValidationUtil
                                    .bigDecimalValueResponseValidation(sw.getOneDayBusyIncome()).toString());
                            calcVariable.setJumlahhariramai(
                                    FieldValidationUtil.integerValueResponseValidation(sw.getTotalDaysInMonthBusy()));
                            calcVariable.setPendapatanSepi(FieldValidationUtil
                                    .bigDecimalValueResponseValidation(sw.getOneDayLessIncome()).toString());
                            calcVariable.setJumlahharisepi(
                                    FieldValidationUtil.integerValueResponseValidation(sw.getTotalDaysInMonthLess()));
                            calcVariable.setPendapatanPerbulanRamaiSepi(FieldValidationUtil
                                    .bigDecimalValueResponseValidation(sw.getTotalLessBusyIncome()).toString());
                            if (sw.getFirstDayIncome() != null)
                                calcVariable.setPeriode1(sw.getFirstDayIncome().toString());
                            if (sw.getSecondDayIncome() != null)
                                calcVariable.setPeriode2(sw.getSecondDayIncome().toString());
                            if (sw.getThirdDayIncome() != null)
                                calcVariable.setPeriode3(sw.getThirdDayIncome().toString());
                            calcVariable.setPendapatanPerbulan3Periode(FieldValidationUtil
                                    .bigDecimalValueResponseValidation(sw.getThreePeriodIncome()).toString());
                            calcVariable.setPendapatanRata2TigaPeriode(FieldValidationUtil
                                    .bigDecimalValueResponseValidation(sw.getAvgThreePeriodIncome()).toString());
                            if (sw.getOpenTime() != null)
                                calcVariable.setJamBuka(formatTime.format(sw.getOpenTime()));
                            if (sw.getCloseTime() != null)
                                calcVariable.setJamTutup(formatTime.format(sw.getCloseTime()));
                            calcVariable.setWaktuKerja(
                                    FieldValidationUtil.integerValueResponseValidation(sw.getWorkTime()));
                            calcVariable.setWaktukerjaSetelahbuka(
                                    FieldValidationUtil.integerValueResponseValidation(sw.getWorkTimeAfterOpen()));
                            if (sw.getCashOpenTime() != null)
                                calcVariable.setUangkasBoxdiBuka(sw.getCashOpenTime().toString());
                            if (sw.getCashCurrentTime() != null)
                                calcVariable.setUangKasBoxSekarang(sw.getCashCurrentTime().toString());
                            if (sw.getCashExpense() != null)
                                calcVariable.setUangBelanja(sw.getCashExpense().toString());
                            calcVariable.setPendapatanPerbulanPerkiraanSehari(FieldValidationUtil
                                    .bigDecimalValueResponseValidation(sw.getOneDayPredictIncome()).toString());
                            calcVariable.setPendapatanPerbulanKasSehari(FieldValidationUtil
                                    .bigDecimalValueResponseValidation(sw.getOneDayCashIncome()).toString());
                            List<OtherItemCalculation> itemPendapatanLainList = swService
                                    .findOtherItemCalcBySwId(sw.getSwId());
                            List<ItemCalcLainReq> itemCalcLainList = new ArrayList<>();
                            for (OtherItemCalculation itemPendapatanLain : itemPendapatanLainList) {
                                ItemCalcLainReq itemCalcLain = new ItemCalcLainReq();
                                itemCalcLain.setName(itemPendapatanLain.getName());
                                itemCalcLain.setAmount(FieldValidationUtil
                                        .bigDecimalValueResponseValidation(itemPendapatanLain.getAmount()).toString());
                                if (itemPendapatanLain.getPeriode() != null)
                                    itemCalcLain.setPeriode(itemPendapatanLain.getPeriode().toString());
                                else
                                    itemCalcLain.setPeriode("0");
                                itemCalcLain.setPendapatanPerbulan(FieldValidationUtil
                                        .bigDecimalValueResponseValidation(itemPendapatanLain.getPendapatanPerbulan())
                                        .toString());
                                itemCalcLainList.add(itemCalcLain);
                            }
                            calcVariable.setPendapatanLainnya(FieldValidationUtil
                                    .bigDecimalValueResponseValidation(sw.getOtherIncome()).toString());
                            calcVariable.setItemCalcLains(itemCalcLainList);
                            swPojo.setCreatedBy(sw.getCreatedBy());
                            swPojo.setCalcVariable(calcVariable);
                            List<DirectBuyList> directBuyList = new ArrayList<>();
                            List<DirectBuyThings> directBuyProductList = swService.findProductBySwId(sw);
                            for (DirectBuyThings directBuyProduct : directBuyProductList) {
                                DirectBuyList product = new DirectBuyList();
                                product.setFrequency(FieldValidationUtil
                                        .integerValueResponseValidation(directBuyProduct.getFrequency()));
                                product.setIndex(FieldValidationUtil
                                        .integerValueResponseValidation(directBuyProduct.getIndex()));
                                product.setItemName(directBuyProduct.getNamaBarang());
                                product.setPrice(FieldValidationUtil
                                        .bigDecimalValueResponseValidation(directBuyProduct.getPrice()));
                                product.setSellingPrice(FieldValidationUtil
                                        .bigDecimalValueResponseValidation(directBuyProduct.getSellingPrice()));
                                product.setTotalItem(directBuyProduct.getTotalItem().toString());
                                product.setType(
                                        FieldValidationUtil.integerValueResponseValidation(directBuyProduct.getType()));
                                directBuyList.add(product);
                            }
                            swPojo.setDirectPurchasing(directBuyList);
                            List<AWGMBuyList> awgmBuyList = new ArrayList<>();
                            List<AWGMBuyThings> awgmBuyProductList = swService.findAwgmProductBySwId(sw);
                            for (AWGMBuyThings awgmBuyProduct : awgmBuyProductList) {
                                AWGMBuyList product = new AWGMBuyList();
                                product.setFrequency(FieldValidationUtil
                                        .integerValueResponseValidation(awgmBuyProduct.getFrequency()));
                                product.setIndex(
                                        FieldValidationUtil.integerValueResponseValidation(awgmBuyProduct.getIndex()));
                                product.setItemName(awgmBuyProduct.getNamaBarang());
                                product.setPrice(FieldValidationUtil
                                        .bigDecimalValueResponseValidation(awgmBuyProduct.getBuyPrice()));
                                product.setSellingPrice(FieldValidationUtil
                                        .bigDecimalValueResponseValidation(awgmBuyProduct.getSellPrice()));
                                product.setTotalItem(awgmBuyProduct.getTotal().toString());
                                product.setType(
                                        FieldValidationUtil.integerValueResponseValidation(awgmBuyProduct.getType()));
                                awgmBuyList.add(product);
                            }
                            swPojo.setAwgmPurchasing(awgmBuyList);
                            SWExpenseResponse expense = new SWExpenseResponse();
                            if (sw.getTransportCost() != null)
                                expense.setTransportUsaha(sw.getTransportCost().toString());
                            if (sw.getUtilityCost() != null)
                                expense.setUtilitasUsaha(sw.getUtilityCost().toString());
                            if (sw.getStaffSalary() != null)
                                expense.setGajiUsaha(sw.getStaffSalary().toString());
                            if (sw.getRentCost() != null)
                                expense.setSewaUsaha(sw.getRentCost().toString());
                            if (sw.getPrivateTransportCost() != null)
                                expense.setTransportNonUsaha(sw.getPrivateTransportCost().toString());
                            if (sw.getPrivateUtilityCost() != null)
                                expense.setUtitlitasNonUsaha(sw.getPrivateUtilityCost().toString());
                            if (sw.getEducationCost() != null)
                                expense.setPendidikanNonUsaha(sw.getEducationCost().toString());
                            if (sw.getHealthCost() != null)
                                expense.setKesehatanNonUsaha(sw.getHealthCost().toString());
                            if (sw.getFoodCost() != null)
                                expense.setMakanNonUsaha(sw.getFoodCost().toString());
                            if (sw.getInstallmentCost() != null)
                                expense.setAngsuranNonUsaha(sw.getInstallmentCost().toString());
                            if (sw.getOtherCost() != null)
                                expense.setLainlainNonUsaha(sw.getOtherCost().toString());
                            swPojo.setExpense(expense);
                            List<ReferenceList> referenceList = new ArrayList<>();
                            List<NeighborRecommendation> neighborList = swService.findNeighborBySwId(sw);
                            for (NeighborRecommendation neighbor : neighborList) {
                                ReferenceList reference = new ReferenceList();
                                reference.setName(neighbor.getNeighborName());
                                reference.setAddress(neighbor.getNeighborAddress());
                                reference.setGoodNeighbour("" + neighbor.getNeighborRelation());
                                if (reference.getGoodNeighbour().equals("t")) {
                                    reference.setGoodNeighbour("true");
                                } else {
                                    reference.setGoodNeighbour("false");
                                }
                                reference.setVisitedByLandshark("" + neighbor.getNeighborOutstanding());
                                if (reference.getVisitedByLandshark().equals("t")) {
                                    reference.setVisitedByLandshark("true");
                                } else {
                                    reference.setVisitedByLandshark("false");
                                }
                                reference.setHaveBusiness("" + neighbor.getNeighborBusiness());
                                if (reference.getHaveBusiness().equals("t")) {
                                    reference.setHaveBusiness("true");
                                } else {
                                    reference.setHaveBusiness("false");
                                }
                                reference.setRecommended("" + neighbor.getNeighborRecomend());
                                if (reference.getRecommended().equals("t")) {
                                    reference.setRecommended("true");
                                } else {
                                    reference.setRecommended("false");
                                }
                                referenceList.add(reference);
                            }
                            swPojo.setReferenceList(referenceList);
                            swPojo.setStatus(sw.getStatus());
                            if (sw.getPmId() != null)
                                swPojo.setPmId(sw.getPmId().getPmId().toString());
                            List<SWApprovalHistory> swApprovalList = new ArrayList<>();
                            List<SWUserBWMPMapping> swUserBwmpList = swService.findBySw(sw.getSwId());
                            for (SWUserBWMPMapping swUserBwmp : swUserBwmpList) {
                                SWApprovalHistory swApproval = new SWApprovalHistory();
                                User userApproval = userService.loadUserByUserId(swUserBwmp.getUserId());
                                if (userApproval != null) {
                                    swApproval.setLevel(swUserBwmp.getLevel());
                                    if (swUserBwmp.getCreatedDate() != null)
                                        swApproval.setTanggal(formatDateTime.format(swUserBwmp.getCreatedDate()));
                                    swApproval.setLimit(
                                            FieldValidationUtil.stringToZeroValidation(userApproval.getLimit()));
                                    swApproval.setName(userApproval.getName());
                                    swApproval.setSwId(sw.getSwId().toString());
                                    swApproval.setRole(userApproval.getRoleName());
                                    swApproval.setDate(swUserBwmp.getDate());
                                    swApproval.setStatus(swUserBwmp.getStatus());
                                    swApproval.setSupervisorId(userApproval.getUserId().toString());
                                    swApprovalList.add(swApproval);
                                }
                            }
                            swPojo.setApprovalHistories(swApprovalList);
                            SwIdPhoto swIdPhoto = swService.getSwIdPhoto(sw.getSwId().toString());
                            if (swIdPhoto != null)
                                swPojo.setHasIdPhoto("true");
                            else
                                swPojo.setHasIdPhoto("false");
                            SwSurveyPhoto swSurveyPhoto = swService.getSwSurveyPhoto(sw.getSwId().toString());
                            if (swSurveyPhoto != null)
                                swPojo.setHasBusinessPlacePhoto("true");
                            else
                                swPojo.setHasBusinessPlacePhoto("false");
                            if (sw.getHasApprovedMs() != null) {
                                if (sw.getHasApprovedMs().equals(true))
                                    swPojo.setHasApprovedMs("true");
                                else
                                    swPojo.setHasApprovedMs("false");
                            } else
                                swPojo.setHasApprovedMs("false");
                            swResponses.add(swPojo);
                        }
                    }
                }
                log.info("Finishing create response Survey Wawancara data");
                log.info("Try to Update Terminal");
                Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                terminal.setSwProgress(PercentageSynchronizer.processSyncPercent(request.getPage(), String.valueOf(swPage.getTotalPages()), terminal.getSwProgress()));
                terminalService.updateTerminal(terminal);
                log.trace("Finishing Update Terminal");
                final List<SurveyWawancara> listDeletedSw = swService.findIsDeletedSwList();
                List<String> deletedSwList = new ArrayList<>();
                for (SurveyWawancara deletedSw : listDeletedSw) {
                    String id = deletedSw.getLocalId().toString();
                    deletedSwList.add(id);
                }
                responseCode.setDeletedSwList(deletedSwList);
                responseCode.setSwList(swResponses);
            }
        } catch (ParseException e) {
            log.error("listSwNonMsDeviation error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("listSwNonMsDeviation error: " + e.getMessage());
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_listSwNonMsDeviation);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("Finishing create Terminal Activity");
                log.info("listSwNonMsDeviation RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("listSwNonMsDeviation saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            return responseCode;
        }
    }

}