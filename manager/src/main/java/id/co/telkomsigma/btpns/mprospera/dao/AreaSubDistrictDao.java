package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.sda.AreaSubDistrict;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AreaSubDistrictDao extends JpaRepository<AreaSubDistrict, String> {

    @Query("SELECT a FROM AreaSubDistrict a WHERE a.areaId  = :areaId ")
    AreaSubDistrict findSubDistrictById(@Param("areaId") String areaId);

    List<AreaSubDistrict> findByParentAreaId(String areaId);

}