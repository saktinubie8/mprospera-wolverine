package id.co.telkomsigma.btpns.mprospera.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.dao.CustomerDao;
import id.co.telkomsigma.btpns.mprospera.manager.CustomerManager;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;

@SuppressWarnings("RedundantIfStatement")
@Service("customerManager")
public class CustomerManagerImpl implements CustomerManager {

    @Autowired
    private CustomerDao customerDao;

    @Override
    @Caching(evict = {@CacheEvict(value = "hwk.customer.getCustomerByRrn", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.countCustomerByUsername", key = "#customer.assignedUsername", beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getAllCustomerByLocPageable", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "hwk.customer.getAllCustomerByLoc", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "hwk.customer.getAllCustomerByLocAndCreatedDate", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "hwk.customer.getAllCustomerByLocAndCreatedDatePagable", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "hwk.customer.findIsDeletedCustomerList", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "hwk.customer.findByCustomerId", key = "#customer.customerId", beforeInvocation = true)})
    public void save(Customer customer) {
        // TODO Auto-generated method stub
        customerDao.save(customer);
    }

    @Override
    public void clearCache() {
        // TODO Auto-generated method stub
    }

    @Override
    public Customer getBySwId(Long swId) {
        // TODO Auto-generated method stub
        return customerDao.findTopBySwId(swId);
    }

    @Override
    public Customer findById(long parseLong) {
        // TODO Auto-generated method stub
        return customerDao.findOne(parseLong);
    }

    @Override
    public Customer getByPdkId(Long pdkId) {
        // TODO Auto-generated method stub
        return customerDao.findTopByPdkId(pdkId);
    }

}