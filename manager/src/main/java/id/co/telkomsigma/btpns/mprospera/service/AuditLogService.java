package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.AuditLogManager;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Dzulfiqar on 11/12/15.
 */
@Service("auditLogService")
public class AuditLogService extends GenericService {

    @Autowired
    private AuditLogManager auditLogManager;

    public AuditLog insertAuditLog(AuditLog auditLog) {
        return auditLogManager.insertAuditLog(auditLog);
    }

}