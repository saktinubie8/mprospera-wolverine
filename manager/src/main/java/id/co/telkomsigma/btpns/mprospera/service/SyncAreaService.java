package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.AreaManager;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.sda.*;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.response.AreaKelurahanResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

@Service("syncAreaService")
public class SyncAreaService extends GenericService {

    @Autowired
    AreaManager areaManager;

    @Autowired
    UserManager userManager;

    @Autowired
    public ThreadPoolTaskExecutor threadPoolTaskExecutor;

    public AreaDistrict getAreaDistrictByUsername(String username) {
        User user = userManager.getUserByUsername(username);
        AreaDistrict retVal = null;
        if (user.getDistrictCode() != null && !user.getDistrictCode().trim().equals("")) {
            retVal = areaManager.findDistrictById(user.getDistrictCode());
            if (retVal == null) {
                List<AreaDistrict> list = areaManager.getWismaById(user.getOfficeCode());
                if (list != null && list.size() > 0)
                    retVal = list.get(0);
            }
        } else {
            List<AreaDistrict> list = areaManager.getWismaById(user.getOfficeCode());
            if (list != null && list.size() > 0)
                retVal = list.get(0);
        }
        return retVal;
    }

    public List<AreaSubDistrict> getAreaSubDistrictByDistrictId(String areaId) {
        List<AreaSubDistrict> kecamatanList = areaManager.getSubDistrictByParentId(areaId);
        List<AreaSubDistrict> filtered = new ArrayList<>();

        for (AreaSubDistrict kel : kecamatanList) {
            if (kel.getParentAreaId().equals(areaId))
                filtered.add(kel);
        }

        return filtered;
    }

    public List<AreaKelurahan> getAreaKelurahanByParentId(final String areaId, final List<AreaKelurahanResponse> pojo) {

        List<AreaKelurahan> kecamatanList = areaManager.getKelurahanByParentId(areaId);
        final AtomicLong i = new AtomicLong(0);
        final Long max = Long.parseLong("" + kecamatanList.size());
        final List<AreaKelurahan> filtered = new ArrayList<>();

        for (final AreaKelurahan kel : kecamatanList) {
            threadPoolTaskExecutor.execute(new Runnable() {

                @Override
                public void run() {
                    if (kel.getParentAreaId().equals(areaId)) {
                        synchronized (filtered) {
                            AreaKelurahanResponse areaKel = new AreaKelurahanResponse();
                            areaKel.setAreaId(kel.getAreaId());
                            areaKel.setAreaName(kel.getAreaName());
                            areaKel.setParentAreaId(kel.getParentAreaId());
                            if (kel.getPostalCode() != null) {
                                areaKel.setPostalCode(kel.getPostalCode());
                            } else {
                                areaKel.setPostalCode("");
                            }
                            pojo.add(areaKel);
                        }
                    }
                    i.addAndGet(1);
                }
            });
        }

        while (i.longValue() < max) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
            }
        }

        return filtered;
    }

    public AreaProvince getAreaProvince(String areaId) {
        return areaManager.getProvinceById(areaId);

    }

}
