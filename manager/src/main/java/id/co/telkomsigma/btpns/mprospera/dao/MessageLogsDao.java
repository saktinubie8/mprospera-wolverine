package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.Date;

import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MessageLogsDao extends JpaRepository<MessageLogs, Long> {

    @Query("SELECT COUNT(DISTINCT m) FROM MessageLogs m INNER JOIN m.terminalActivity t WHERE t.terminalActivityId = :terminalActivityId AND m.createdDate >= :createdDate AND m.createdDate <= :endedDate  ORDER BY m.createdDate DESC")
    int count(@Param("terminalActivityId") String terminalActivityId, @Param("createdDate") Date createdDate,
              @Param("endedDate") Date endedDate);

    @Query("SELECT N FROM MessageLogs N WHERE N.terminalActivity = :terminalActivity")
    Page<MessageLogs> findByTerminalActivityCache(@Param("terminalActivity") TerminalActivity terminalActivity,
                                                  Pageable pagable);

}