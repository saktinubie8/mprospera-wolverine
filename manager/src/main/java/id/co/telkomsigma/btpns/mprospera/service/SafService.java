package id.co.telkomsigma.btpns.mprospera.service;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.manager.SafManager;
import id.co.telkomsigma.btpns.mprospera.model.saf.Saf;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("safService")
public class SafService extends GenericService {

    @Autowired
    @Qualifier("safManager")
    private SafManager safManager;

    public List<Saf> findAll() {
        return safManager.findAll();
    }

    public Saf findById(Long id) {
        return safManager.findById(id);
    }

    public void delete(Long id) {
        safManager.delete(id);
    }

    public void save(Saf saf) {
        safManager.save(saf);
    }

}