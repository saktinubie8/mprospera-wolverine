package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerDao extends JpaRepository<Customer, Long> {

    Customer findTopBySwId(Long swId);

    Customer findTopByPdkId(Long pdkId);

    Customer findByRrn(String rrn);

}