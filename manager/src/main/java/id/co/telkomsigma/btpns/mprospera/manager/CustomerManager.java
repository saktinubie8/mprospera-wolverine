package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;

public interface CustomerManager {

    void clearCache();

    void save(Customer customer);

    Customer getBySwId(Long swId);

    Customer getByPdkId(Long pdkId);

    Customer findById(long parseLong);

}