package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.mm.MiniMeeting;

import java.util.Date;
import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface MMManager {

	int countAll();

	Page<MiniMeeting> findAll(List<String> kelIdList);

	Page<MiniMeeting> findAllMM();

	Page<MiniMeeting> findAllMMPageable(PageRequest pageRequest);

	Page<MiniMeeting> findAllByCreatedDate(Date startDate, Date endDate);

	Page<MiniMeeting> findAllByCreatedDatePageable(Date startDate, Date endDate, PageRequest pageRequest);

	Page<MiniMeeting> findAllMMByUser(List<String> userIdList);

	Page<MiniMeeting> findAllMMByUserPageable(List<String> userIdList, PageRequest pageRequest);

	Page<MiniMeeting> findAllByCreatedDateAndUser(List<String> userIdList, Date startDate, Date endDate);

	Page<MiniMeeting> findAllByCreatedDateAndUserPageable(List<String> userIdList, Date startDate, Date endDate, PageRequest pageRequest);

	Page<MiniMeeting> findAllPageable(List<String> kelIdList, PageRequest pageRequest);

	Page<MiniMeeting> findByCreatedDate(List<String> kelIdList, Date startDate, Date endDate);

    Page<MiniMeeting> findByCreatedDatePageable(List<String> kelIdList, Date startDate, Date endDate,
                                                PageRequest pageRequest);

    void save(MiniMeeting miniMeeting);

    Boolean isValidMm(String mmId);

    MiniMeeting findByRrn(String rrn);

    void clearCache();

    List<MiniMeeting> findIsDeletedMmList();

}