package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.dao.LoanDao;
import id.co.telkomsigma.btpns.mprospera.dao.LoanDeviationDao;
import id.co.telkomsigma.btpns.mprospera.manager.LoanManager;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanDeviation;

@SuppressWarnings("RedundantIfStatement")
@Service("loanManager")
public class LoanManagerImpl implements LoanManager {

    @Autowired
    private LoanDao loanDao;

    @Autowired
    private LoanDeviationDao loanDeviationDao;

    @Override
    public Page<Loan> findByCreatedDate(Date startDate, Date endDate, String officeId) {
        // TODO Auto-generated method stub
        return loanDao.findByCreatedDate(startDate, endDate, new PageRequest(0, Integer.MAX_VALUE), officeId);
    }

    @Override
    public Page<Loan> findByCreatedDatePageable(Date startDate, Date endDate, PageRequest pageRequest,
                                                String officeId) {
        // TODO Auto-generated method stub
        return loanDao.findByCreatedDate(startDate, endDate, pageRequest, officeId);
    }

    @Override
    public void clearCache() {
        // TODO Auto-generated method stub
    }

    @Override
    public Loan findById(long parseLong) {
        // TODO Auto-generated method stub
        return loanDao.findOne(parseLong);
    }

    @Override
    public LoanDeviation findDeviationByAp3rId(Long ap3rId) {
        // TODO Auto-generated method stub
        return loanDeviationDao.findByAp3rIdAndIsDeletedFalse(ap3rId);
    }
    
    /*
	 * @author : Ilham
	 * @Since  : 20191128
	 * @category patch 
	 *    - set Top 1, jika ada duplicate data  
	 */ 
    @Override
    public Loan findByAp3rAndCustomer(Long ap3rId, Customer customer) {
        List<String> statusList = new ArrayList<>();
        statusList.add(WebGuiConstant.STATUS_ACTIVE);
        statusList.add(WebGuiConstant.STATUS_APPROVED);
        statusList.add(WebGuiConstant.STATUS_DRAFT);
        return loanDao.findTop1ByAp3rIdAndCustomerAndAp3rIdIsNotNullAndIsDeletedFalseAndStatusIn
                (ap3rId, customer, statusList);
    }

}