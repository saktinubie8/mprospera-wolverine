package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.sda.AreaKelurahanDetails;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaSubDistrictDetails;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Date;
import java.util.List;

public interface SDAManager {

    void saveSubDistrictDetails(AreaSubDistrictDetails areaDetails);

    void saveKelurahanDetails(AreaKelurahanDetails areaDetails);

    Boolean isValidSda(String sdaId, String type);

    void clearCache();

    Page<AreaSubDistrictDetails> getAllSubDistrictDetailsByParentAreaId(String areaId);

    Page<AreaSubDistrictDetails> findByCreatedDate(String areaId, Date parse, Date parse2);

    Page<AreaSubDistrictDetails> findByCreatedDatePageable(String areaId, Date parse, Date parse2,
                                                           PageRequest pageRequest);

    Page<AreaSubDistrictDetails> getAllSubDistrictDetailsByCreatedBy(List<String> userList);

    Page<AreaSubDistrictDetails> getAllSubDistrictDetailsByCreatedBy(List<String> userList, PageRequest pageRequest);

    Page<AreaSubDistrictDetails> findByCreatedDate(List<String> userList, Date parse, Date parse2);

    Page<AreaSubDistrictDetails> findByCreatedDatePageable(List<String> userList, Date parse, Date parse2,
                                                           PageRequest pageRequest);

    Page<AreaKelurahanDetails> getAllKelurahanDetailsByParentAreaId(List<String> usernameList);

    Page<AreaKelurahanDetails> getAllKelurahanDetailsByParentAreaId(List<String> usernameList,
                                                                    PageRequest pageRequest);

    Page<AreaKelurahanDetails> getAllKelurahanDetailsCreatedDate(List<String> usernameList, Date parse,
                                                                 Date parse2);

    Page<AreaKelurahanDetails> getAllKelurahanDetailsCreatedDatePageable(List<String> usernameList, Date parse,
                                                                         Date parse2, PageRequest pageRequest);

}