package id.co.telkomsigma.btpns.mprospera.service;


import id.co.telkomsigma.btpns.mprospera.manager.LoanManager;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanDeviation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("loanService")
public class LoanService extends GenericService {

    @Autowired
    private LoanManager loanManager;

    public Loan findById(String loanId) {
        if (loanId == null || "".equals(loanId))
            return null;
        return loanManager.findById(Long.parseLong(loanId));
    }

    public LoanDeviation findDeviationByAp3rId(Long ap3rId) {
        return loanManager.findDeviationByAp3rId(ap3rId);
    }

    public Loan findByAp3rAndCustomer(Long ap3rId, Customer customer) {
        return loanManager.findByAp3rAndCustomer(ap3rId, customer);
    }

}