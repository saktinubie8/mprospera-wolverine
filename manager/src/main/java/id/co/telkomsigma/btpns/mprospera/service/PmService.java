package id.co.telkomsigma.btpns.mprospera.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.manager.PmManager;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.mm.MiniMeeting;
import id.co.telkomsigma.btpns.mprospera.model.pm.ProjectionMeeting;

import id.co.telkomsigma.btpns.mprospera.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service("pmService")
public class PmService extends GenericService {

	@Autowired
	private PmManager pmManager;

	@Autowired
	private UserManager userManager;

	@Autowired
	private AreaService areaService;

	public void save(ProjectionMeeting pm) {
		pmManager.save(pm);
	}

	public Boolean isValidPm(String pmId) {
		return pmManager.isValidPm(pmId);
	}

	public Page<ProjectionMeeting> getPmByCreatedDate(String page, String getCountData, String startDate,
			String endDate, String username) throws Exception {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		Calendar c = Calendar.getInstance();
		Calendar cStart = Calendar.getInstance();
		if (getCountData == null)
			page = null;
		if (endDate == null || "".equals(endDate)) {
			endDate = startDate;
		}
		int pageNumber = Integer.parseInt(page) - 1;
		if (startDate == null || startDate.equals("")) {
			if (page == null || page.equals(""))
				return pmManager.getPm();
			else
				return pmManager.getPmPageable(
						new PageRequest(pageNumber, Integer.parseInt(getCountData)));
		}
		else {
    		c.setTime(formatter.parse(endDate));
			cStart.setTime(formatter.parse(startDate));
			c.add(Calendar.DATE, 1);
			cStart.add(Calendar.DATE, -7);
			endDate = formatter.format(c.getTime());
			startDate = formatter.format(cStart.getTime());
			if (page == null || page.equals(""))
				return pmManager.findPmByCreatedDate(formatter.parse(startDate), formatter.parse(endDate));
			else
				return pmManager.findPmByCreatedDatePageable(formatter.parse(startDate),
						formatter.parse(endDate), new PageRequest(pageNumber, Integer.parseInt(getCountData)));
		}
	}

	public Page<ProjectionMeeting> getPmByCreatedDateAndUser(String page, String getCountData, String startDate,
													  String endDate, String username) throws Exception {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

		Calendar c = Calendar.getInstance();
		Calendar cStart = Calendar.getInstance();

		if (getCountData == null)
			page = null;
		if (endDate == null || "".equals(endDate)) {
			endDate = startDate;
		}

		int pageNumber = Integer.parseInt(page) - 1;

		User userSW = userManager.getUserByUsername(username);
		List<User> userListWisma = userManager.getUserByLocId(userSW.getOfficeCode());
		List<String> userIdList = new ArrayList<>();
		for (User user : userListWisma) {
			userIdList.add(user.getUsername());
		}

		if (startDate == null || startDate.equals("")) {
			if (page == null || page.equals(""))
				return pmManager.getPmByUser(userIdList);
			else
				return pmManager.getPmByUserPageable(userIdList,
						new PageRequest(pageNumber, Integer.parseInt(getCountData)));
		}

		else {

			c.setTime(formatter.parse(endDate));
			cStart.setTime(formatter.parse(startDate));
			c.add(Calendar.DATE, 1);
			cStart.add(Calendar.DATE, -7);
			endDate = formatter.format(c.getTime());
			startDate = formatter.format(cStart.getTime());

			if (page == null || page.equals(""))
				return pmManager.findPmByCreatedDateAndUser(userIdList, formatter.parse(startDate), formatter.parse(endDate));
			else
				return pmManager.findPmByCreatedDateAndUserPageable(userIdList, formatter.parse(startDate),
						formatter.parse(endDate), new PageRequest(pageNumber, Integer.parseInt(getCountData)));
		}
	}

	public ProjectionMeeting getPmByRrn(String rrn) {
		return pmManager.findByRrn(rrn);
	}
	
	public List<ProjectionMeeting> findByMmId(MiniMeeting mmId){
		return pmManager.findByMmId(mmId);
	}
	
	public ProjectionMeeting findById(String pmId){
		return pmManager.findById(pmId);
	}
	
	public List<ProjectionMeeting> findIsDeletedPmList(){
		return pmManager.findIsDeletedPmList();
	}
	
}