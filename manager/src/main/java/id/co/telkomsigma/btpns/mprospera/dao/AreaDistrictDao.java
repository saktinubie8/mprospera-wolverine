package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.sda.AreaDistrict;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AreaDistrictDao extends JpaRepository<AreaDistrict, String> {

    List<AreaDistrict> findByAreaNameLikeIgnoreCase(String kabupaten);

}