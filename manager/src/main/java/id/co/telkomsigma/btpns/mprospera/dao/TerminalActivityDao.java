package id.co.telkomsigma.btpns.mprospera.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;

/**
 * Created by Dzulfiqar on 11/12/15.
 */
public interface TerminalActivityDao extends JpaRepository<TerminalActivity, String> {

}