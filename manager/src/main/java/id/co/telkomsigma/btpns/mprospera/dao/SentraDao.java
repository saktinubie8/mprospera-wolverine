package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.Date;

import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SentraDao extends JpaRepository<Sentra, Long> {

    Sentra findByRrn(String rrn);

    @Query("SELECT s FROM Sentra s WHERE s.createdDate>=:startDate AND s.createdDate<:endDate and s.isDeleted=false and s.assignedTo=:username")
    Page<Sentra> findByCreatedDate(@Param("username") String username, @Param("startDate") Date startDate,
                                   @Param("endDate") Date endDate, Pageable pageRequest);

    @Query("SELECT s FROM Sentra s WHERE s.createdDate>=:startDate AND s.createdDate<:endDate and s.isDeleted=false and s.locationId=:loc")
    Page<Sentra> findByCreatedDateWithLoc(@Param("loc") String loc, @Param("startDate") Date startDate,
                                          @Param("endDate") Date endDate, Pageable pageRequest);

}