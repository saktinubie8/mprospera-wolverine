package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.ArrayList;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.dao.*;
import id.co.telkomsigma.btpns.mprospera.manager.AreaManager;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import id.co.telkomsigma.btpns.mprospera.model.sda.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service("addSdaManager")
public class AreaManagerImpl implements AreaManager {

    @Autowired
    private AreaProvinceDao areaProvinceDao;

    @Autowired
    private AreaDistrictDao areaDistrictDao;

    @Autowired
    private AreaSubDistrictDao areaSubDistrictDao;

    @Autowired
    private AreaKelurahanDao areaKelurahanDao;

    @Autowired
    private LocationDao locationDao;

    protected final Log log = LogFactory.getLog(getClass());

    @Override
    public AreaSubDistrict findSubDistrictById(String areaId) {
        // TODO Auto-generated method stub
        return areaSubDistrictDao.findSubDistrictById(areaId);
    }

    @Override
    @Cacheable(value = "wln.area.findKelurahanById", unless = "#result == null")
    public AreaKelurahan findKelurahanById(String areaId) {
        // TODO Auto-generated method stub
        return areaKelurahanDao.findKelurahanById(areaId);
    }

    @Override
    @Cacheable(value = "wln.area.findById", unless = "#result == null")
    public AreaKelurahan findById(String id) {
        return areaKelurahanDao.findOne(id);
    }

    @Override
    @Cacheable(value = "wln.area.getKelurahanByParentId", unless = "#result == null")
    public List<AreaKelurahan> getKelurahanByParentId(String areaId) {
        return areaKelurahanDao.findByParentAreaId(areaId);
    }

    @Override
    @Cacheable(value = "wln.area.getSubDistrictByParentId", unless = "#result == null")
    public List<AreaSubDistrict> getSubDistrictByParentId(String areaId) {
        // TODO Auto-generated method stub
        return areaSubDistrictDao.findByParentAreaId(areaId);
    }

    @Override
    @Cacheable(value = "wln.area.getWismaById", unless = "#result == null")
    public List<AreaDistrict> getWismaById(String officeCode) {
        Location loc = locationDao.findOne(officeCode);
        if (loc == null) {
            return new ArrayList<>();
        }
        return areaDistrictDao.findByAreaNameLikeIgnoreCase("%" + loc.getKabupaten() + "%");
    }

    @Override
    @Cacheable(value = "wln.area.findDistrictById", unless = "#result == null")
    public AreaDistrict findDistrictById(String districtCode) {
        return areaDistrictDao.findOne(districtCode);
    }

    @Override
    @Cacheable(value = "wln.area.getProvinceById", unless = "#result == null")
    public AreaProvince getProvinceById(String areaId) {
        // TODO Auto-generated method stub
        return areaProvinceDao.findByAreaId(areaId);
    }

    @Override
    @CacheEvict(allEntries = true, cacheNames = {"wln.area.findById", "wln.area.getKelurahanByParentId",
            "wln.area.isValidSda", "wln.area.findKelurahanById", "wln.area.getSubDistrictByParentId",
            "wln.area.getWismaById", "wln.area.findDistrictById", "wln.area.getProvinceById"})
    public void clearCache() {
        // TODO Auto-generated method stub
    }

}