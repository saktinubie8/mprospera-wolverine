package id.co.telkomsigma.btpns.mprospera.manager;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import id.co.telkomsigma.btpns.mprospera.model.mm.MiniMeeting;
import id.co.telkomsigma.btpns.mprospera.model.pm.ProjectionMeeting;

public interface PmManager {

	void save(ProjectionMeeting pm);

	Boolean isValidPm(String pmId);

	long countAllPmNotDeleted();

	Page<ProjectionMeeting> getAllPm(List<String> kelIdList);

	Page<ProjectionMeeting> getPm();

    Page<ProjectionMeeting> getPmPageable(PageRequest pageRequest);

    Page<ProjectionMeeting> findPmByCreatedDate(Date startDate, Date endDate);

    Page<ProjectionMeeting> findPmByCreatedDatePageable(Date startDate, Date endDate, PageRequest pageRequest);

    Page<ProjectionMeeting> getPmByUser(List<String> userIdList);

    Page<ProjectionMeeting> getPmByUserPageable(List<String> userIdList, PageRequest pageRequest);

    Page<ProjectionMeeting> findPmByCreatedDateAndUser(List<String> userIdList, Date startDate, Date endDate);

    Page<ProjectionMeeting> findPmByCreatedDateAndUserPageable(List<String> userIdList, Date startDate, Date endDate, PageRequest pageRequest);

    Page<ProjectionMeeting> getAllPmPageable(List<String> kelIdList, PageRequest pageRequest);

	Page<ProjectionMeeting> findByCreatedDate(List<String> kelIdList, Date startDate, Date endDate);

	Page<ProjectionMeeting> findByCreatedDatePageable(List<String> kelIdList, Date startDate, Date endDate,
			PageRequest pageRequest);

	void clearCache();

    ProjectionMeeting findByRrn(String rrn);

    List<ProjectionMeeting> findByMmId(MiniMeeting mmId);

    ProjectionMeeting findById(String pmId);

    List<ProjectionMeeting> findIsDeletedPmList();

}