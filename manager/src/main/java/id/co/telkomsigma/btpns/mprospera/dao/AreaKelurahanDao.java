package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.sda.AreaKelurahan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AreaKelurahanDao extends JpaRepository<AreaKelurahan, String> {

    @Query("SELECT a FROM AreaKelurahan a WHERE a.areaId  = :areaId ")
    AreaKelurahan findKelurahanById(@Param("areaId") String areaId);

    List<AreaKelurahan> findByParentAreaId(String areaId);

}