package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.dao.*;
import id.co.telkomsigma.btpns.mprospera.model.sw.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.manager.SWManager;
import org.springframework.transaction.annotation.Transactional;

@SuppressWarnings("RedundantIfStatement")
@Service("swManager")
public class SWManagerImpl implements SWManager {

    @Autowired
    SWDao swDao;

    @Autowired
    SwIdPhotoDao swIdPhotoDao;

    @Autowired
    SwSurveyPhotoDao swSurveyPhotoDao;

    @Autowired
    SwUserBwmpMapDao swUserBwmpMapDao;

    @Autowired
    private LoanProductDao loanProductDao;

    @Autowired
    private BusinessTypeDao businessTypeDao;

    @Autowired
    private BusinessTypeAP3RDao businessTypeAp3rDao;

    @Autowired
    private ProductPlafondDao productPlafondDao;

    @Autowired
    private DirectBuyDao directBuyDao;

    @Autowired
    private AWGMDao awgmDao;

    @Autowired
    private NeighborDao neighborDao;

    @Autowired
    private SwProductMapDao swProductMapDao;

    @Autowired
    private OtherItemCalculationDao otherItemCalculationDao;

    protected Log log = LogFactory.getLog(this.getClass());

    @Override
    @Cacheable(value = "wln.sw.countAllSW", unless = "#result == null")
    public int countAll() {
        // TODO Auto-generated method stub
        return swDao.countAll();
    }

    @Override
    @Cacheable(value = "wln.sw.countAllSWByStatus", unless = "#result == null")
    public int countAllByStatus() {
        return swDao.countAllByStatus(WebGuiConstant.STATUS_WAITING_APPROVAL);
    }

    @Override
    @Cacheable(value = "wln.sw.findAll", unless = "#result == null")
    public Page<SurveyWawancara> findAll(List<String> kelIdList) {
        // TODO Auto-generated method stub
        return swDao.findByAreaIdInAndLocalIdIsNotNullAndIsDeleted(kelIdList, new PageRequest(0, countAll()), false);
    }

    @Override
    @Cacheable(value = "wln.sw.findByCreatedDate", unless = "#result == null")
    public Page<SurveyWawancara> findByCreatedDate(List<String> kelIdList, Date startDate, Date endDate) {
        // TODO Auto-generated method stub
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return swDao.findByCreatedDate(kelIdList, calStart.getTime(), cal.getTime(), new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    @Cacheable(value = "wln.sw.findByCreatedDatePageable", unless = "#result == null")
    public Page<SurveyWawancara> findByCreatedDatePageable(List<String> kelIdList, Date startDate, Date endDate, PageRequest pageRequest) {
        // TODO Auto-generated method stub
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return swDao.findByCreatedDate(kelIdList, calStart.getTime(), cal.getTime(), pageRequest);
    }

    @Override
    @Cacheable(value = "wln.sw.isValidSw", unless = "#result == null")
    public Boolean isValidSw(String swId) {
        Integer count = swDao.countBySwId(Long.parseLong(swId));
        if (count > 0)
            return true;
        else
            return false;
    }

    @Override
    @Cacheable(value = "wln.sw.getSWById", unless = "#result == null")
    public SurveyWawancara getSWById(String swId) {
        // TODO Auto-generated method stub
        return swDao.findSwBySwId(Long.parseLong(swId));
    }

    @Override
    @Cacheable(value = "wln.sw.findByRrn", unless = "#result == null")
    public SurveyWawancara findByRrn(String rrn) {
        // TODO Auto-generated method stub
        return swDao.findByRrnAndLocalIdIsNotNull(rrn);
    }

    @Override
    @Cacheable(value = "wln.sw.findAllByUser", unless = "#result == null")
    public Page<SurveyWawancara> findAllByUser(List<String> userIdList) {
        // TODO Auto-generated method stub
        return swDao.findByCreatedByInAndLocalIdIsNotNullAndIsDeleted(userIdList, new PageRequest(0, countAll()), false);
    }

    @Override
    @Cacheable(value = "wln.sw.findAllByUserPageable", unless = "#result == null")
    public Page<SurveyWawancara> findAllByUserPageable(List<String> userIdList, PageRequest pageRequest) {
        // TODO Auto-generated method stub
        return swDao.findByCreatedByInAndLocalIdIsNotNullAndIsDeleted(userIdList, pageRequest, false);
    }

    @Override
    @Cacheable(value = "wln.sw.findByCreatedDateAndUser", unless = "#result == null")
    public Page<SurveyWawancara> findByCreatedDateAndUser(List<String> userIdList, Date startDate, Date endDate) {
        // TODO Auto-generated method stub
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return swDao.findByCreatedDateAndUser(userIdList, calStart.getTime(), cal.getTime(), new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    @Cacheable(value = "wln.sw.findByCreatedDateAndUserPageable", unless = "#result == null")
    public Page<SurveyWawancara> findByCreatedDateAndUserPageable(List<String> userIdList, Date startDate,
                                                                  Date endDate, PageRequest pageRequest) {
        // TODO Auto-generated method stub
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return swDao.findByCreatedDateAndUser(userIdList, calStart.getTime(), cal.getTime(), pageRequest);
    }

    @Override
    //@Cacheable(value = "wln.sw.findAllByUserMS", unless = "#result == null")
    public Page<SurveyWawancara> findAllByUserMS(List<String> userIdList) {
        return swDao.findByCreatedByInAndLocalIdIsNotNullAndIsDeletedAndStatus(userIdList, new PageRequest(0, countAll()), false, WebGuiConstant.STATUS_WAITING_APPROVAL);
    }

    @Override
    //@Cacheable(value = "wln.sw.findAllByUserMSPageable", unless = "#result == null")
    public Page<SurveyWawancara> findAllByUserMSPageable(List<String> userIdList, PageRequest pageRequest) {
        log.info("Accessing DB");
        return swDao.findByCreatedByInAndLocalIdIsNotNullAndIsDeletedAndStatus(userIdList, pageRequest, false, WebGuiConstant.STATUS_WAITING_APPROVAL);
    }

    @Override
   // @Cacheable(value = "wln.sw.findByCreatedDateAndUserMS", unless = "#result == null")
    public Page<SurveyWawancara> findByCreatedDateAndUserMS(List<String> userIdList, Date startDate, Date endDate) {
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return swDao.findByCreatedDateAndUserAndStatus(userIdList, calStart.getTime(), cal.getTime(), new PageRequest(0, Integer.MAX_VALUE), WebGuiConstant.STATUS_WAITING_APPROVAL);
    }

    @Override
    //@Cacheable(value = "wln.sw.findByCreatedDateAndUserMSPageable", unless = "#result == null")
    public Page<SurveyWawancara> findByCreatedDateAndUserMSPageable(List<String> userIdList, Date startDate, Date endDate, PageRequest pageRequest) {
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return swDao.findByCreatedDateAndUserAndStatus(userIdList, calStart.getTime(), cal.getTime(), pageRequest, WebGuiConstant.STATUS_WAITING_APPROVAL);
    }

    @Override
    @Cacheable(value = "wln.sw.findAllNonMs", unless = "#result == null")
    public Page<SurveyWawancara> findAllNonMs() {
        return swDao.findByStatusAndLocalIdIsNotNullAndIsDeleted(WebGuiConstant.STATUS_WAITING_APPROVAL, new PageRequest(0, countAllByStatus()), false);
    }

    @Override
    @Cacheable(value = "wln.sw.findAllNonMsPageable", unless = "#result == null")
    public Page<SurveyWawancara> findAllNonMsPageable(PageRequest pageRequest) {
        return swDao.findByStatusAndLocalIdIsNotNullAndIsDeleted(WebGuiConstant.STATUS_WAITING_APPROVAL, pageRequest, false);
    }

    @Override
    @Cacheable(value = "wln.sw.findAllNonMsWithUserId", unless = "#result == null")
    public Page<SurveyWawancara> findAllNonMsWithUserId(Long userId) {
        return swDao.findSWWaitingForApprovalNonMS(userId, WebGuiConstant.STATUS_WAITING_APPROVAL, new PageRequest(0, countAllByStatus()));
    }

    @Override
    @Cacheable(value = "wln.sw.findAllNonMsWithUserIdPageable", unless = "#result == null")
    public Page<SurveyWawancara> findAllNonMsWithUserIdPageable(Long userId, PageRequest pageRequest) {
        return swDao.findSWWaitingForApprovalNonMS(userId, WebGuiConstant.STATUS_WAITING_APPROVAL, pageRequest);
    }

    @Override
    @Cacheable(value = "wln.sw.findByCreatedDateNonMs", unless = "#result == null")
    public Page<SurveyWawancara> findByCreatedDateNonMs(Date startDate, Date endDate) {
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return swDao.findByCreatedDateAndStatus(WebGuiConstant.STATUS_WAITING_APPROVAL, calStart.getTime(), cal.getTime(), new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    @Cacheable(value = "wln.sw.findByCreatedDateNonMsPageable", unless = "#result == null")
    public Page<SurveyWawancara> findByCreatedDateNonMsPageable(Date startDate, Date endDate, PageRequest pageRequest) {
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return swDao.findByCreatedDateAndStatus(WebGuiConstant.STATUS_WAITING_APPROVAL, calStart.getTime(), cal.getTime(), pageRequest);
    }

    @Override
    @Cacheable(value = "wln.sw.findIsDeletedSwList", unless = "#result == null")
    public List<SurveyWawancara> findIsDeletedSwList() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -7);
        Date dateBefore7Days = cal.getTime();
        return swDao.findIsDeletedSwId(dateBefore7Days, new Date());
    }

    @Override
    public Boolean hasIdPhoto(String swId) {
        Integer count = swIdPhotoDao.countBySwId(Long.parseLong(swId));
        if (count > 0)
            return true;
        else
            return false;
    }

    @Override
    public Boolean hasSurveyPhoto(String swId) {
        Integer count = swSurveyPhotoDao.countBySwId(Long.parseLong(swId));
        if (count > 0)
            return true;
        else
            return false;
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "wln.sw.countAllSW", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.countAllSWByStatus", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findAll", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findByCreatedDate", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findByCreatedDatePageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.isValidSw", key = "#surveyWawancara.swId", beforeInvocation = true),
            @CacheEvict(value = "wln.sw.getSWById", key = "#surveyWawancara.swId", beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findByRrn", key = "#surveyWawancara.rrn", beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findAllByUser", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findAllByUserPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findByCreatedDateAndUser", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findByCreatedDateAndUserPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findAllByUserMS", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findAllByUserMSPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findByCreatedDateAndUserMS", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findByCreatedDateAndUserMSPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findAllNonMs", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findAllNonMsPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findAllNonMsWithUserId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findAllNonMsWithUserIdPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findByCreatedDateNonMs", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findByCreatedDateNonMsPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findByLocalId", key = "#surveyWawancara.localId", beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findIsDeletedSwList", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sw.countAllSW", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sw.findIsDeletedSwList", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sw.isValidSw",  key = "#surveyWawancara.swId", beforeInvocation = true),
            @CacheEvict(value = "hwk.sw.getSWById",  key = "#surveyWawancara.swId", beforeInvocation = true),
            @CacheEvict(value = "hwk.sw.findByLocalId",  key = "#surveyWawancara.localId", beforeInvocation = true),
            @CacheEvict(value = "hwk.sw.findByUserMS",  allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sw.findSwIdInLoanDeviationWithBatch",  allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sw.findSwIdInLoanDeviation",  allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sw.findByUserMSWithDate", allEntries = true, beforeInvocation = true)})
    public void save(SurveyWawancara surveyWawancara) {
        // TODO Auto-generated method stub
        swDao.save(surveyWawancara);
    }

    @Override
    @Cacheable(value = "wln.loan.product.countAllProduct", unless = "#result == null")
    public int countAllProduct() {
        // TODO Auto-generated method stub
        return loanProductDao.countAll();
    }

    @Override
    @Cacheable(value = "wln.loan.product.findByProductId", unless = "#result == null")
    public LoanProduct findByProductId(String productId) {
        // TODO Auto-generated method stub
        return loanProductDao.findByProductId(Long.parseLong(productId));
    }

    @Override
    @Cacheable(value = "wln.loan.product.findAllProduct", unless = "#result == null")
    public Page<LoanProduct> findAllProduct() {
        // TODO Auto-generated method stub
        return loanProductDao.findAll(new PageRequest(0, countAllProduct()));
    }

    @Override
    @Cacheable(value = "wln.loan.product.findAllProductPageable", unless = "#result == null")
    public Page<LoanProduct> findAllProductPageable(PageRequest pageRequest) {
        // TODO Auto-generated method stub
        return loanProductDao.findAll(pageRequest);
    }

    @Override
    @Cacheable(value = "wln.product.plafon.findByProductId", unless = "#result == null")
    public List<ProductPlafond> findByProductId(LoanProduct productId) {
        // TODO Auto-generated method stub
        return productPlafondDao.findByProductIdOrderByPlafond(productId);
    }

    @Override
    @Cacheable(value = "wln.business.countAllBusinessType", unless = "#result == null")
    public int countAllBusinessType() {
        // TODO Auto-generated method stub
        return businessTypeDao.countAll();
    }

    @Override
    @Cacheable(value = "wln.businessType.findAllBusinessType", unless = "#result == null")
    public Page<BusinessType> findAllBusinessType() {
        // TODO Auto-generated method stub
        return businessTypeDao.findAll(new PageRequest(0, countAllBusinessType()));
    }

    @Override
    @Cacheable(value = "wln.businessType.findAllBusinessTypePageable", unless = "#result == null")
    public Page<BusinessType> findAllBusinessTypePageable(PageRequest pageRequest) {
        // TODO Auto-generated method stub
        return businessTypeDao.findAll(pageRequest);
    }

    @Override
    @Cacheable(value = "wln.businessTypeAp3r.findAllBusinessTypeAP3R", unless = "#result == null")
    public Page<BusinessTypeAP3R> findAllBusinessTypeAP3R() {
        return businessTypeAp3rDao.findAll(new PageRequest(0, countAllBusinessType()));
    }

    @Override
    @Cacheable(value = "wln.businessTypeAp3r.findAllBusinessTypeAP3RPageable", unless = "#result == null")
    public Page<BusinessTypeAP3R> findAllBusinessTypeAP3RPageable(PageRequest pageRequest) {
        return businessTypeAp3rDao.findAll(pageRequest);
    }

    @Override
    @Cacheable(value = "wln.idPhoto.getSwIdPhoto", unless = "#result == null")
    public SwIdPhoto getSwIdPhoto(String swId) {
        // TODO Auto-generated method stub
        return swIdPhotoDao.getSwIdPhoto(Long.parseLong(swId));
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "wln.idPhoto.getSwIdPhoto", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.idPhoto.getSwIdPhoto", allEntries = true, beforeInvocation = true)})
    public void save(SwIdPhoto swIdPhoto) {
        // TODO Auto-generated method stub
        swIdPhotoDao.save(swIdPhoto);
    }

    @Override
    @Cacheable(value = "wln.businessPlacePhoto.getSwSurveyPhoto", unless = "#result == null")
    public SwSurveyPhoto getSwSurveyPhoto(String swId) {
        // TODO Auto-generated method stub
        return swSurveyPhotoDao.getSwSurveyPhoto(Long.parseLong(swId));
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "wln.businessPlacePhoto.getSwSurveyPhoto", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.businessPlacePhoto.getSwSurveyPhoto", allEntries = true, beforeInvocation = true)})
    public void save(SwSurveyPhoto SwSurveyPhoto) {
        // TODO Auto-generated method stub
        swSurveyPhotoDao.save(SwSurveyPhoto);
    }

    @Override
    @Cacheable(value = "wln.direct.findProductBySwId", unless = "#result == null")
    public List<DirectBuyThings> findProductBySwId(SurveyWawancara sw) {
        // TODO Auto-generated method stub
        return directBuyDao.findBySwId(sw);
    }


    @Override
    @Caching(evict = {@CacheEvict(value = "hwk.direct.findProductBySwId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.direct.findProductBySwId", allEntries = true, beforeInvocation = true)})
    public void save(DirectBuyThings directBuyProduct) {
        // TODO Auto-generated method stub
        directBuyDao.save(directBuyProduct);
    }

    @Override
    @Transactional
    @Caching(evict = {@CacheEvict(value = "hwk.direct.findProductBySwId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.direct.findProductBySwId", allEntries = true, beforeInvocation = true)})
    public void deleteDirectBuy(DirectBuyThings id) {
        directBuyDao.delete(id);
    }

    @Override
    @Cacheable(value = "wln.awgm.findAwgmProductBySwId", unless = "#result == null")
    public List<AWGMBuyThings> findAwgmProductBySwId(SurveyWawancara sw) {
        // TODO Auto-generated method stub
        return awgmDao.findBySwId(sw);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "hwk.awgm.findAwgmProductBySwId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.awgm.findAwgmProductBySwId",allEntries = true, beforeInvocation = true)})
    public void save(AWGMBuyThings awgmBuyProduct) {
        // TODO Auto-generated method stub
        awgmDao.save(awgmBuyProduct);
    }

    @Override
    @Transactional
    @Caching(evict = {@CacheEvict(value = "hwk.awgm.findAwgmProductBySwId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.awgm.findAwgmProductBySwId", allEntries = true, beforeInvocation = true)})
    public void deleteAwgmBuy(AWGMBuyThings id) {
        awgmDao.delete(id);
    }

    @Override
    @Cacheable(value = "wln.neighbor.findNeighborBySwId", unless = "#result == null")
    public List<NeighborRecommendation> findNeighborBySwId(SurveyWawancara sw) {
        // TODO Auto-generated method stub
        return neighborDao.findBySwId(sw);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "wln.neighbor.findNeighborBySwId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.neighbor.findNeighborBySwId", allEntries = true, beforeInvocation = true)})
    public void save(NeighborRecommendation neighbor) {
        // TODO Auto-generated method stub
        neighborDao.save(neighbor);
    }

    @Override
    @Transactional
    @Caching(evict = {@CacheEvict(value = "wln.neighbor.findNeighborBySwId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.neighbor.findNeighborBySwId", allEntries = true, beforeInvocation = true)
    })
    public void deleteReffNeighbour(NeighborRecommendation id) {
        neighborDao.delete(id);
    }

    @Override
    @Cacheable(value = "wln.other.item.findOtherItemCalcBySwId", unless = "#result == null")
    public List<OtherItemCalculation> findOtherItemCalcBySwId(Long swId) {
        return otherItemCalculationDao.findBySwId(swId);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "hwk.other.item.findOtherItemCalcBySwId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.other.item.findOtherItemCalcBySwId", allEntries = true, beforeInvocation = true)})
    public void save(OtherItemCalculation otherItemCalculation) {
        otherItemCalculationDao.save(otherItemCalculation);
    }

    @Override
    @Transactional
    @Caching(evict = {@CacheEvict(value = "hwk.other.item.findOtherItemCalcBySwId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.other.item.findOtherItemCalcBySwId", allEntries = true, beforeInvocation = true)})
    public void deleteOtherItemCalc(OtherItemCalculation id) {
        otherItemCalculationDao.delete(id);
    }

    @Override
   // @Cacheable(value = "wln.swProductMap.findProductMapBySwId", unless = "#result == null")
    public List<SWProductMapping> findProductMapBySwId(Long swId) {
        // TODO Auto-generated method stub
        return swProductMapDao.findSwProductMapBySwId(swId);
    }

    @Override
    public SWProductMapping findProductMapById(Long id) {
        return swProductMapDao.findByMappingId(id);
    }

    @Override
    //@Cacheable(value = "wln.sw.findByLocalId", unless = "#result == null")
    public SurveyWawancara findByLocalId(String localId) {
        return swDao.findByLocalId(localId);
    }

    @Override
    //@Cacheable(value = "wln.swProductMap.findProductMapByLocalId", unless = "#result == null")
    public SWProductMapping findProductMapByLocalId(String localId) {
        return swProductMapDao.findByLocalId(localId);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "wln.swProductMap.findProductMapBySwId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.swProductMap.findProductMapById", allEntries = true, beforeInvocation = true),
            //@CacheEvict(value = "wln.swProductMap.findProductMapByLocalId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.swProductMap.findProductMapById", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.swProductMap.findProductMapBySwId", allEntries = true, beforeInvocation = true)})
    public void save(SWProductMapping map) {
        // TODO Auto-generated method stub
        swProductMapDao.save(map);
    }

    @Override
    @Cacheable(value = "wln.swUserBwmpMap.findBySwAndUser", unless = "#result == null")
    public SWUserBWMPMapping findBySwAndUser(Long sw, Long userId) {
        return swUserBwmpMapDao.findBySwIdAndUserId(sw, userId);
    }

    @Override
    @Cacheable(value = "wln.swUserBwmpMap.findBySwAndLevel", unless = "#result == null")
    public SWUserBWMPMapping findBySwAndLevel(Long sw, String level) {
        return swUserBwmpMapDao.findBySwIdAndLevel(sw, level);
    }

    @Override
    @Cacheable(value = "wln.swUserBwmpMap.findBySw", unless = "#result == null")
    public List<SWUserBWMPMapping> findBySw(Long sw) {
        return swUserBwmpMapDao.findBySwId(sw);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "wln.swUserBwmpMap.findSwMapApprovalBySwId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.swUserBwmpMap.findSWUserBWMPMappingBySW", allEntries = true, beforeInvocation = true)})
    public void save(SWUserBWMPMapping swUserBWMPMapping) {
        // TODO Auto-generated method stub
        swUserBwmpMapDao.save(swUserBWMPMapping);
    }

    @Override
    @CacheEvict(value = {"wln.sw.countAllSW", "wln.sw.countAllSWByStatus", "wln.sw.findAll", "wln.sw.findByCreatedDate",
            "wln.sw.findByCreatedDatePageable", "wln.sw.isValidSw", "wln.sw.getSWById",
            "wln.sw.findByRrn", "wln.sw.findAllByUser", "wln.sw.findAllByUserPageable",
            "wln.sw.findByCreatedDateAndUser", "wln.sw.findByCreatedDateAndUserPageable",
            "wln.sw.findAllByUserMS", "wln.sw.findAllByUserMSPageable", "wln.sw.findByCreatedDateAndUserMS",
            "wln.sw.findByCreatedDateAndUserMSPageable", "wln.sw.findAllNonMs", "wln.sw.findAllNonMsPageable",
            "wln.sw.findAllNonMsWithUserId", "wln.sw.findAllNonMsWithUserIdPageable", "wln.sw.findByCreatedDateNonMs",
            "wln.sw.findByCreatedDateNonMsPageable", "wln.sw.findByLocalId", "wln.sw.findIsDeletedSwList",
            "wln.loan.product.countAllProduct", "wln.loan.product.findByProductId", "wln.loan.product.findAllProduct",
            "wln.loan.product.findAllProductPageable", "wln.product.plafon.findByProductId",
            "wln.business.countAllBusinessType", "wln.businessType.findAllBusinessType", "wln.businessType.findAllBusinessTypePageable",
            "wln.businessTypeAp3r.findAllBusinessTypeAP3R", "wln.businessTypeAp3r.findAllBusinessTypeAP3RPageable",
            "wln.idPhoto.getSwIdPhoto", "wln.businessPlacePhoto.getSwSurveyPhoto", "wln.direct.findProductBySwId",
            "wln.awgm.findAwgmProductBySwId", "wln.neighbor.findNeighborBySwId", "wln.other.item.findOtherItemCalcBySwId",
            "wln.swProductMap.findProductMapById", "wln.swProductMap.findProductMapBySwId","wln.swProductMap",//findProductMapByLocalId",
            "wln.swUserBwmpMap.findBySwAndUser", "wln.swUserBwmpMap.findBySwAndLevel", "wln.swUserBwmpMap.findBySw"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {
        // TODO Auto-generated method stub
    }



}