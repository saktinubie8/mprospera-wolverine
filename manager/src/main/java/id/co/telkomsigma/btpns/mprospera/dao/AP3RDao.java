package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import id.co.telkomsigma.btpns.mprospera.model.sw.AP3R;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AP3RDao extends JpaRepository<AP3R, Long> {

    @Query("SELECT COUNT(m) FROM AP3R m WHERE m.localId is not null AND m.isDeleted = false")
    int countAll();

    List<AP3R> findBySwIdAndIsDeleted(Long sw, Boolean deleted);

    AP3R findByAp3rIdAndLocalIdIsNotNullAndIsDeleted(Long ap3rId, Boolean deleted);

    AP3R findByLocalIdAndIsDeleted(String localId, Boolean deleted);

    Page<AP3R> findByCreatedByInAndLocalIdIsNotNullAndIsDeleted(List<String> userIdList, Pageable pageRequest, Boolean deleted);

    Page<AP3R> findByCreatedByInAndLocalIdIsNotNullAndIsDeletedAndStatus(List<String> userIdList, Pageable pageRequest, Boolean deleted, String status);

    @Query("SELECT a FROM AP3R a WHERE a.ap3rId in (SELECT d.ap3rId FROM LoanDeviation d) and a.localId is not null and a.isDeleted = false")
    Page<AP3R> findAllForDeviation(Pageable pageRequset);

    @Query("SELECT m FROM AP3R m WHERE m.createdDate>=:startDate AND m.createdDate<:endDate AND m.createdBy in :userList AND m.localId is not null AND m.isDeleted = false ORDER BY m.createdBy ASC")
    Page<AP3R> findByCreatedDateAndUser(@Param("userList") List<String> userIdList,
                                        @Param("startDate") Date startDate, @Param("endDate") Date endDate, Pageable pageable);

    @Query("SELECT m FROM AP3R m WHERE m.createdDate>=:startDate AND m.createdDate<:endDate AND m.createdBy in :userList AND m.localId is not null AND m.isDeleted = false AND m.status = :status ORDER BY m.createdBy ASC")
    Page<AP3R> findByCreatedDateAndUserAndStatus(@Param("userList") List<String> userIdList,
                                                 @Param("startDate") Date startDate, @Param("endDate") Date endDate, Pageable pageable, @Param("status") String status);

    @Query("SELECT a FROM AP3R a WHERE a.ap3rId in (SELECT d.ap3rId FROM LoanDeviation d) AND a.createdDate>=:startDate AND a.createdDate<:endDate AND a.localId is not null AND a.isDeleted = false")
    Page<AP3R> findForDeviationByCreatedDate(@Param("startDate") Date startDate, @Param("endDate") Date endDate, Pageable pageable);

    @Query("SELECT a FROM AP3R a WHERE a.createdDate>=:startDate AND a.createdDate<:endDate and a.localId is not null and a.isDeleted = true")
    List<AP3R> findIsDeletedAp3rId(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

}