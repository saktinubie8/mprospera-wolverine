package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.pdk.PelatihanDasarKeanggotaan;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface PDKManager {

    int countAll();

    Page<PelatihanDasarKeanggotaan> findAll(List<String> userList);

    Page<PelatihanDasarKeanggotaan> findAllPageable(List<String> userList, PageRequest pageRequest);

    Page<PelatihanDasarKeanggotaan> findByCreatedDate(List<String> userList, Date startDate, Date endDate);

    Page<PelatihanDasarKeanggotaan> findByCreatedDatePageable(List<String> userList, Date startDate, Date endDate,
                                                              PageRequest pageRequest);

    Boolean isValidpdk(String pdkId);

    void save(PelatihanDasarKeanggotaan pelatihanDasarKeanggotaan);

    PelatihanDasarKeanggotaan findByRrn(String rrn);

    void clearCache();

    List<PelatihanDasarKeanggotaan> findIsDeletedPdkList();

}