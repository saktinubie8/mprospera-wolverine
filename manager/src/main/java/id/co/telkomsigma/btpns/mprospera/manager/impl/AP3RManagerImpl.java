package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.dao.AP3RDao;
import id.co.telkomsigma.btpns.mprospera.dao.FundedThingsDao;
import id.co.telkomsigma.btpns.mprospera.manager.AP3RManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.AP3R;
import id.co.telkomsigma.btpns.mprospera.model.sw.FundedThings;

@Service("ap3rManager")
public class AP3RManagerImpl implements AP3RManager {

    @Autowired
    private FundedThingsDao fundedThingsDao;

    @Autowired
    private AP3RDao ap3rDao;

    @Override
    public int countAll() {
        // TODO Auto-generated method stub
        return ap3rDao.countAll();
    }

    @Override
    public void save(FundedThings product) {
        // TODO Auto-generated method stub
        fundedThingsDao.save(product);
    }

    @Override
    public void delete(FundedThings products) {
        fundedThingsDao.delete(products);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "hwk.ap3r.findAp3rById", allEntries = true, beforeInvocation = true)})
    public void save(AP3R ap3r) {
        // TODO Auto-generated method stub
        ap3rDao.save(ap3r);
    }

    @Override
    public List<FundedThings> findByAp3rId(AP3R ap3r) {
        // TODO Auto-generated method stub
        return fundedThingsDao.findByAp3rId(ap3r);
    }

    @Override
    public AP3R findByAp3rId(Long id) {
        return ap3rDao.findByAp3rIdAndLocalIdIsNotNullAndIsDeleted(id, false);
    }

    @Override
    public Page<AP3R> findAllByUser(List<String> userIdList) {
        // TODO Auto-generated method stub
        return ap3rDao.findByCreatedByInAndLocalIdIsNotNullAndIsDeleted(userIdList, new PageRequest(0, countAll()), false);
    }

    @Override
    public Page<AP3R> findAllByUserPageable(List<String> userIdList, PageRequest pageRequest) {
        // TODO Auto-generated method stub
        return ap3rDao.findByCreatedByInAndLocalIdIsNotNullAndIsDeleted(userIdList, pageRequest, false);
    }

    @Override
    public Page<AP3R> findByCreatedDateAndUser(List<String> userIdList, Date startDate, Date endDate) {
        // TODO Auto-generated method stub
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return ap3rDao.findByCreatedDateAndUser(userIdList, calStart.getTime(), cal.getTime(), new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    public Page<AP3R> findByCreatedDateAndUserPageable(List<String> userIdList, Date startDate,
                                                       Date endDate, PageRequest pageRequest) {
        // TODO Auto-generated method stub
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return ap3rDao.findByCreatedDateAndUser(userIdList, calStart.getTime(), cal.getTime(), pageRequest);
    }

    @Override
    public Page<AP3R> findAllByUserMS(List<String> userIdList) {
        return ap3rDao.findByCreatedByInAndLocalIdIsNotNullAndIsDeletedAndStatus(userIdList, new PageRequest(0, countAll()), false, WebGuiConstant.STATUS_WAITING_APPROVAL);
    }

    @Override
    public Page<AP3R> findAllByUserMSPageable(List<String> userIdList, PageRequest pageRequest) {
        return ap3rDao.findByCreatedByInAndLocalIdIsNotNullAndIsDeletedAndStatus(userIdList, pageRequest, false, WebGuiConstant.STATUS_WAITING_APPROVAL);
    }

    @Override
    public Page<AP3R> findByCreatedDateAndUserMS(List<String> userIdList, Date startDate, Date endDate) {
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return ap3rDao.findByCreatedDateAndUserAndStatus(userIdList, calStart.getTime(), cal.getTime(), new PageRequest(0, Integer.MAX_VALUE), WebGuiConstant.STATUS_WAITING_APPROVAL);
    }

    @Override
    public Page<AP3R> findByCreatedDateAndUserMSPageable(List<String> userIdList, Date startDate, Date endDate, PageRequest pageRequest) {
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return ap3rDao.findByCreatedDateAndUserAndStatus(userIdList, calStart.getTime(), cal.getTime(), pageRequest, WebGuiConstant.STATUS_WAITING_APPROVAL);
    }

    @Override
    public Page<AP3R> findForDeviation() {
        return ap3rDao.findAllForDeviation(new PageRequest(0, countAll()));
    }

    @Override
    public Page<AP3R> findForDeviationPageable(PageRequest pageRequest) {
        return ap3rDao.findAllForDeviation(pageRequest);
    }

    @Override
    public Page<AP3R> findForDeviationByCreatedDate(Date startDate, Date endDate) {
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return ap3rDao.findForDeviationByCreatedDate(calStart.getTime(), cal.getTime(), new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    public Page<AP3R> findForDeviationByCreatedDatePageable(Date startDate, Date endDate, PageRequest pageRequest) {
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return ap3rDao.findForDeviationByCreatedDate(calStart.getTime(), cal.getTime(), pageRequest);
    }

    public AP3R findByLocalId(String localId) {
        return ap3rDao.findByLocalIdAndIsDeleted(localId, false);
    }

    @Override
    public List<AP3R> findIsDeletedAp3rList() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -7);
        Date dateBefore7Days = cal.getTime();
        return ap3rDao.findIsDeletedAp3rId(dateBefore7Days, new Date());
    }

    @Override
    public List<AP3R> findAp3rListBySwId(Long swId) {
        return ap3rDao.findBySwIdAndIsDeleted(swId, false);
    }

}