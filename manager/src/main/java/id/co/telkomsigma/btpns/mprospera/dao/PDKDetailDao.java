package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.pdk.PDKDetail;
import id.co.telkomsigma.btpns.mprospera.model.pdk.PDKDetailId;
import id.co.telkomsigma.btpns.mprospera.model.pdk.PelatihanDasarKeanggotaan;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PDKDetailDao extends JpaRepository<PDKDetail, PDKDetailId> {

    @Modifying
    @Query("delete from PDKDetail pd Where pdk = :pdk")
    Integer cleanPdkId(@Param("pdk") PelatihanDasarKeanggotaan pelatihanDasarKeanggotaan);

}