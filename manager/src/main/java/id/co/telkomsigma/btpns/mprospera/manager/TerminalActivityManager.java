package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;

public interface TerminalActivityManager {

	TerminalActivity insertTerminalActivity(TerminalActivity terminalActivity);

}