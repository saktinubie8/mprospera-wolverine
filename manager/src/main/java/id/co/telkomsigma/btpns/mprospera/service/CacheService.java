package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("cacheService")
public class CacheService extends GenericService {

    @Autowired
    PDKManager pdkManager;
    @Autowired
    SentraManager sentraManager;
    @Autowired
    SWManager swManager;
    @Autowired
    TerminalManager terminalManager;
    @Autowired
    UserManager userManager;
    @Autowired
    PmManager pmManager;
    @Autowired
    AreaManager areaManager;
    @Autowired
    SDAManager sdaManager;
    @Autowired
    LocationManager locationManager;

    public void clearCache() {
        areaManager.clearCache();
        pdkManager.clearCache();
        sentraManager.clearCache();
        swManager.clearCache();
        terminalManager.clearCache();
        userManager.clearCache();
        pmManager.clearCache();
        sdaManager.clearCache();
        locationManager.clearCache();
    }

}