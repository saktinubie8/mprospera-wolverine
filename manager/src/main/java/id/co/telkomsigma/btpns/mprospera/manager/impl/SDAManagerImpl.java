package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.dao.*;
import id.co.telkomsigma.btpns.mprospera.manager.SDAManager;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaKelurahanDetails;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaSubDistrict;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaSubDistrictDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service("sdaManager")
public class SDAManagerImpl implements SDAManager {

    @Autowired
    private AreaProvinceDao areaProvinceDao;

    @Autowired
    private AreaDistrictDao areaDistrictDao;

    @Autowired
    private AreaSubDistrictDao areaSubDistrictDao;

    @Autowired
    private AreaKelurahanDao areaKelurahanDao;

    @Autowired
    private AreaProvinceDetailsDao areaProvinceDetailsDao;

    @Autowired
    private AreaDistrictDetailsDao areaDistrictDetailsDao;

    @Autowired
    private AreaSubDistrictDetailsDao areaSubDistrictDetailsDao;

    @Autowired
    private AreaKelurahanDetailsDao areaKelurahanDetailsDao;

    @Autowired
    private LocationDao locationDao;

    @Override
    @Caching(evict = {
            @CacheEvict(value = "wln.sda.isValidSda", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "wln.sda.findByCreatedDate", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "wln.sda.findByCreatedDatePageable", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "wln.sda.getAllSubDistrictDetailsByCreatedBy", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "wln.sda.getAllSubDistrictDetailsByCreatedByPaging", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "wln.sda.findAllByCreatedDate", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "wln.sda.findAllByCreatedDatePageable", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "wln.sda.getAllKelurahanDetailsByParentAreaId", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "wln.sda.getAllKelurahanDetailsByParentAreaIdPaging", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "wln.sda.getAllKelurahanDetailsCreatedDate", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "wln.sda.getAllKelurahanDetailsCreatedDatePageable", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "wln.sda.getAllSubDistrictDetailsByParentAreaId", beforeInvocation = true, allEntries = true)})
    public void saveSubDistrictDetails(AreaSubDistrictDetails areaDetails) {
        // TODO Auto-generated method stub
        Date start = new Date();
        areaSubDistrictDetailsDao.save(areaDetails);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "wln.sda.isValidSda", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "wln.sda.findByCreatedDate", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "wln.sda.findByCreatedDatePageable", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "wln.sda.getAllSubDistrictDetailsByCreatedBy", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "wln.sda.getAllSubDistrictDetailsByCreatedByPaging", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "wln.sda.findAllByCreatedDate", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "wln.sda.findAllByCreatedDatePageable", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "wln.sda.getAllKelurahanDetailsByParentAreaId", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "wln.sda.getAllKelurahanDetailsByParentAreaIdPaging", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "wln.sda.getAllKelurahanDetailsCreatedDate", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "wln.sda.getAllKelurahanDetailsCreatedDatePageable", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "wln.sda.getAllSubDistrictDetailsByParentAreaId", beforeInvocation = true, allEntries = true)})
    public void saveKelurahanDetails(AreaKelurahanDetails areaDetails) {
        // TODO Auto-generated method stub
        Date start = new Date();
        areaKelurahanDetailsDao.save(areaDetails);
    }

    @Override
    @Cacheable(value = "wln.sda.isValidSda", unless = "#result == null")
    public Boolean isValidSda(String sdaId, String type) {
        if (WebGuiConstant.SDA_PROVINSI.equalsIgnoreCase(type)) {
            Integer count = areaProvinceDetailsDao.countByAreaDetailId(Long.parseLong(sdaId));
            if (count > 0)
                return true;
        } else if (WebGuiConstant.SDA_KABUPATEN.equalsIgnoreCase(type)) {
            Integer count = areaDistrictDetailsDao.countByAreaDetailId(Long.parseLong(sdaId));
            if (count > 0)
                return true;
        } else if (WebGuiConstant.SDA_KECAMATAN.equalsIgnoreCase(type)) {
            Integer count = areaSubDistrictDetailsDao.countByAreaDetailId(Long.parseLong(sdaId));
            if (count > 0)
                return true;
        } else if (WebGuiConstant.SDA_KELURAHAN.equalsIgnoreCase(type)) {
            Integer count = areaKelurahanDetailsDao.countByAreaDetailId(Long.parseLong(sdaId));
            if (count > 0)
                return true;
        }
        return false;
    }

    @Override
    @CacheEvict(allEntries = true, cacheNames = {"wln.sda.isValidSda", "wln.sda.findKelurahanById",
            "wln.sda.getAllSubDistrictDetailsByParentAreaId", "wln.sda.findByCreatedDate",
            "wln.sda.findByCreatedDatePageable", "wln.sda.getAllSubDistrictDetailsByCreatedBy",
            "wln.sda.getAllSubDistrictDetailsByCreatedByPaging", "wln.sda.findAllByCreatedDate",
            "wln.sda.findAllByCreatedDatePageable", "wln.sda.getAllKelurahanDetailsByParentAreaId",
            "wln.sda.getAllKelurahanDetailsByParentAreaIdPaging", "wln.sda.getAllKelurahanDetailsCreatedDate",
            "wln.sda.getAllKelurahanDetailsCreatedDatePageable"})
    public void clearCache() {
        // TODO Auto-generated method stub
    }

    @Override
    @Cacheable(value = "wln.sda.getAllSubDistrictDetailsByParentAreaId", unless = "#result == null")
    public Page<AreaSubDistrictDetails> getAllSubDistrictDetailsByParentAreaId(String districtAreaId) {
        List<AreaSubDistrict> subDistrictList = areaSubDistrictDao.findByParentAreaId(districtAreaId);
        if (subDistrictList == null || subDistrictList.size() == 0)
            return null;
        return areaSubDistrictDetailsDao.searchAllDetailsByParentId(subDistrictList,
                new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    @Cacheable(value = "wln.sda.findByCreatedDate", unless = "#result == null")
    public Page<AreaSubDistrictDetails> findByCreatedDate(String areaId, Date parse, Date parse2) {
        List<AreaSubDistrict> subDistrictList = areaSubDistrictDao.findByParentAreaId(areaId);
        if (subDistrictList == null || subDistrictList.size() == 0)
            return null;
        return areaSubDistrictDetailsDao.searchDetailsByParentIdAndDate(subDistrictList, parse, parse2,
                new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    @Cacheable(value = "wln.sda.findByCreatedDatePageable", unless = "#result == null")
    public Page<AreaSubDistrictDetails> findByCreatedDatePageable(String areaId, Date parse, Date parse2,
                                                                  PageRequest pageRequest) {
        List<AreaSubDistrict> subDistrictList = areaSubDistrictDao.findByParentAreaId(areaId);
        if (subDistrictList == null || subDistrictList.size() == 0)
            return null;
        return areaSubDistrictDetailsDao.searchDetailsByParentIdAndDate(subDistrictList, parse, parse2, pageRequest);
    }

    @Override
    @Cacheable(value = "wln.sda.getAllSubDistrictDetailsByCreatedBy", unless = "#result == null")
    public Page<AreaSubDistrictDetails> getAllSubDistrictDetailsByCreatedBy(List<String> userList) {
        return areaSubDistrictDetailsDao.searchAllDetailByCreatedBy(userList, new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    @Cacheable(value = "wln.sda.getAllSubDistrictDetailsByCreatedByPaging", unless = "#result == null")
    public Page<AreaSubDistrictDetails> getAllSubDistrictDetailsByCreatedBy(List<String> userList, PageRequest pageRequest) {
        return areaSubDistrictDetailsDao.searchAllDetailByCreatedBy(userList, pageRequest);
    }

    @Override
    @Cacheable(value = "wln.sda.findAllByCreatedDate", unless = "#result == null")
    public Page<AreaSubDistrictDetails> findByCreatedDate(List<String> userList, Date parse, Date parse2) {
        return areaSubDistrictDetailsDao.searchAllDetailByCreatedByAndDate(userList, parse, parse2, new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    @Cacheable(value = "wln.sda.findAllByCreatedDatePageable", unless = "#result == null")
    public Page<AreaSubDistrictDetails> findByCreatedDatePageable(List<String> userList, Date parse, Date parse2, PageRequest pageRequest) {
        return areaSubDistrictDetailsDao.searchAllDetailByCreatedByAndDate(userList, parse, parse2, pageRequest);
    }

    @Override
    @Cacheable(value = "wln.sda.getAllKelurahanDetailsByParentAreaId", unless = "#result == null")
    public Page<AreaKelurahanDetails> getAllKelurahanDetailsByParentAreaId(List<String> userList) {
        return areaKelurahanDetailsDao.searchByParentAreaId(userList, new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    @Cacheable(value = "wln.sda.getAllKelurahanDetailsByParentAreaIdPaging", unless = "#result == null")
    public Page<AreaKelurahanDetails> getAllKelurahanDetailsByParentAreaId(List<String> userList,
                                                                           PageRequest pageRequest) {
        return areaKelurahanDetailsDao.searchByParentAreaId(userList, pageRequest);
    }

    @Override
    @Cacheable(value = "wln.sda.getAllKelurahanDetailsCreatedDate", unless = "#result == null")
    public Page<AreaKelurahanDetails> getAllKelurahanDetailsCreatedDate(List<String> userList, Date start,
                                                                        Date end) {
        return areaKelurahanDetailsDao.searchByParentAreaId(userList, start, end,
                new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    @Cacheable(value = "wln.sda.getAllKelurahanDetailsCreatedDatePageable", unless = "#result == null")
    public Page<AreaKelurahanDetails> getAllKelurahanDetailsCreatedDatePageable(List<String> userList,
                                                                                Date start, Date end, PageRequest pageRequest) {
        return areaKelurahanDetailsDao.searchByParentAreaId(userList, start, end, pageRequest);
    }

}