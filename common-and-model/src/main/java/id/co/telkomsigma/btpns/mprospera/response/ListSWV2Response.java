package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class ListSWV2Response {

    private String swId;
    private String idCardName;
    private String sentraName;
    private String status;
    private String createdBy;
    private String customerCif;
    private List<SWProductListResponse> swProducts;
    private String address;

    public String getSwId() {
        return swId;
    }

    public void setSwId(String swId) {
        this.swId = swId;
    }

    public String getSentraName() {
        return sentraName;
    }

    public void setSentraName(String sentraName) {
        this.sentraName = sentraName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getIdCardName() {
        return idCardName;
    }

    public void setIdCardName(String idCardName) {
        this.idCardName = idCardName;
    }

    public List<SWProductListResponse> getSwProducts() {
        return swProducts;
    }

    public void setSwProducts(List<SWProductListResponse> swProducts) {
        this.swProducts = swProducts;
    }

    public String getCustomerCif() {
        return customerCif;
    }

    public void setCustomerCif(String customerCif) {
        this.customerCif = customerCif;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

}