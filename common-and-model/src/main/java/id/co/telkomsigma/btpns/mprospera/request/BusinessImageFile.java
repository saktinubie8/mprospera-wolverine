package id.co.telkomsigma.btpns.mprospera.request;

/**
 * Created by Dzulfiqar on 26/07/2017.
 */
public class BusinessImageFile {

    private String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

}