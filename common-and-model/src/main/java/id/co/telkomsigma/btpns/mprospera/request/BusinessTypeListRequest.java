package id.co.telkomsigma.btpns.mprospera.request;

public class BusinessTypeListRequest extends BaseRequest {

    private String username;
    private String imei;
    private String sessionKey;
    private Integer page;
    private Integer getCountData;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getGetCountData() {
        return getCountData;
    }

    public void setGetCountData(Integer getCountData) {
        this.getCountData = getCountData;
    }

}