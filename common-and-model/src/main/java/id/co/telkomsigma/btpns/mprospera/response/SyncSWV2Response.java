package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class SyncSWV2Response extends BaseResponse {

    private String grandTotal;
    private String currentTotal;
    private String totalPage;
    private List<ListSWV2Response> swList;
    private List<String> deletedSwList;

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getCurrentTotal() {
        return currentTotal;
    }

    public void setCurrentTotal(String currentTotal) {
        this.currentTotal = currentTotal;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    public List<ListSWV2Response> getSwList() {
        return swList;
    }

    public void setSwList(List<ListSWV2Response> swList) {
        this.swList = swList;
    }

    public List<String> getDeletedSwList() {
        return deletedSwList;
    }

    public void setDeletedSwList(List<String> deletedSwList) {
        this.deletedSwList = deletedSwList;
    }

}