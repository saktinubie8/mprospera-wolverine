package id.co.telkomsigma.btpns.mprospera.response;

import java.util.Arrays;

public class LoginResponse extends ProsperaLoginResponse {

    private String sessionKey;
    private String downloadUrl;

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    @Override
    public String toString() {
        return "LoginResponse [sessionKey=" + sessionKey + ", downloadUrl=" + downloadUrl + ", getOfficeId()="
                + getOfficeId() + ", getState()=" + getState() + ", getRole()=" + Arrays.toString(getRole())
                + ", getCenters()=" + Arrays.toString(getCenters()) + ", getResponseCode()=" + getResponseCode()
                + ", getResponseMessage()=" + getResponseMessage() + "]";
    }

}