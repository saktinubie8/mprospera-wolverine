package id.co.telkomsigma.btpns.mprospera.model.sw;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

@Entity
@Table(name = "T_SW_PRODUCT_MAP")
public class SWProductMapping extends GenericModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private Long mappingId;
    private String localId;
    private LoanProduct productId;
    private BigDecimal plafon;
    private Long recommendedProductId;
    private BigDecimal recommendedPlafon;
    private Long swId;
    private Boolean isDeleted = false;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getMappingId() {
        return mappingId;
    }

    public void setMappingId(Long mappingId) {
        this.mappingId = mappingId;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = LoanProduct.class)
    @JoinColumn(name = "product_id", referencedColumnName = "id", nullable = true)
    public LoanProduct getProductId() {
        return productId;
    }

    public void setProductId(LoanProduct productId) {
        this.productId = productId;
    }

    @Column(name = "plafon")
    public BigDecimal getPlafon() {
        return plafon;
    }

    public void setPlafon(BigDecimal plafon) {
        this.plafon = plafon;
    }

    @Column(name = "sw_id")
    public Long getSwId() {
        return swId;
    }

    public void setSwId(Long swId) {
        this.swId = swId;
    }

    @Column(name = "rec_product_id")
    public Long getRecommendedProductId() {
        return recommendedProductId;
    }

    public void setRecommendedProductId(Long recommendedProductId) {
        this.recommendedProductId = recommendedProductId;
    }

    @Column(name = "rec_plafon")
    public BigDecimal getRecommendedPlafon() {
        return recommendedPlafon;
    }

    public void setRecommendedPlafon(BigDecimal recommendedPlafon) {
        this.recommendedPlafon = recommendedPlafon;
    }

    @Column(name = "is_deleted", nullable = true)
    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    @Column(name = "local_id")
    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

}