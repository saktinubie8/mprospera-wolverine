package id.co.telkomsigma.btpns.mprospera.model.customer;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Group;

@Entity
@Table(name = "T_CUSTOMER")
public class Customer extends GenericModel {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    protected Long customerId;
    protected String customerCifNumber;
    protected String customerName;
    protected String customerIdNumber;
    protected Date createdDate;
    protected String createdBy;
    protected String customerStatus;
    protected Group group;
    protected String assignedUsername;
    protected Long pdkId;
    protected Long swId;
    protected String longitude;
    protected String latitude;
    protected String prosperaId;
    private String rrn;
    private String approvedBy;
    private Date approvedDate;

    @Column(name = "approved_by")
    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    @Column(name = "created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "prospera_id")
    public String getProsperaId() {
        return prosperaId;
    }

    public void setProsperaId(String prosperaId) {
        this.prosperaId = prosperaId;
    }

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue
    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    @Column(name = "status")
    public String getCustomerStatus() {
        return customerStatus;
    }

    public void setCustomerStatus(String customerStatus) {
        this.customerStatus = customerStatus;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = Group.class)
    @JoinColumn(name = "group_id", referencedColumnName = "id")
    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    @Column(name = "assigned_username")
    public String getAssignedUsername() {
        return assignedUsername;
    }

    public void setAssignedUsername(String assignedUsername) {
        this.assignedUsername = assignedUsername;
    }

    @Column(name = "cif_number")
    public String getCustomerCifNumber() {
        return customerCifNumber;
    }

    public void setCustomerCifNumber(String customerCifNumber) {
        this.customerCifNumber = customerCifNumber;
    }

    @Column(name = "name")
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @Column(name = "id_number")
    public String getCustomerIdNumber() {
        return customerIdNumber;
    }

    public void setCustomerIdNumber(String customerIdNumber) {
        this.customerIdNumber = customerIdNumber;
    }

    @Column(name = "created_dt", nullable = false)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    protected Boolean isDeleted = false;

    @Column(name = "is_deleted", nullable = false)
    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Column(name = "pdk_id")
    public Long getPdkId() {
        return pdkId;
    }

    public void setPdkId(Long pdkId) {
        this.pdkId = pdkId;
    }

    @Column(name = "sw_id")
    public Long getSwId() {
        return swId;
    }

    public void setSwId(Long swId) {
        this.swId = swId;
    }

    @Column(name = "rrn")
    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    @Column(name = "approved_dt")
    public Date getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(Date approvedDate) {
        this.approvedDate = approvedDate;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

}