package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class BusinessTypeListResponse extends BaseResponse {

    private List<BusinessTypeList> businessTypeList;
    private String grandTotal;
    private String currentTotal;
    private String totalPage;

    public List<BusinessTypeList> getBusinessTypeList() {
        return businessTypeList;
    }

    public void setBusinessTypeList(List<BusinessTypeList> businessTypeList) {
        this.businessTypeList = businessTypeList;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getCurrentTotal() {
        return currentTotal;
    }

    public void setCurrentTotal(String currentTotal) {
        this.currentTotal = currentTotal;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

}