package id.co.telkomsigma.btpns.mprospera.model.sw;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

@Entity
@Table(name = "T_PRODUCT_PLAFOND")
public class ProductPlafond extends GenericModel {

    Long plafondId;
    BigDecimal plafond;
    LoanProduct productId;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getPlafondId() {
        return plafondId;
    }

    public void setPlafondId(Long plafondId) {
        this.plafondId = plafondId;
    }

    @Column(name = "plafond")
    public BigDecimal getPlafond() {
        return plafond;
    }

    public void setPlafond(BigDecimal plafond) {
        this.plafond = plafond;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = LoanProduct.class)
    @JoinColumn(name = "product_id", referencedColumnName = "id", nullable = true)
    public LoanProduct getProductId() {
        return productId;
    }

    public void setProductId(LoanProduct productId) {
        this.productId = productId;
    }

}