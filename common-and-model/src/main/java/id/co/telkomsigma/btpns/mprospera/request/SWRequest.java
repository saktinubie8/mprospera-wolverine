package id.co.telkomsigma.btpns.mprospera.request;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@SuppressWarnings("ALL")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class SWRequest extends BaseRequest {

    /* untuk add SW PLan */
    SWProfileReq profile;
    private String idCardName;
    private String swLocation;
    private String dayLight;
    private String surveyDate;
    private String pmId;
    private String time;
    private String tokenKey;
    /* untuk add SW */
    private String username;
    private String sessionKey;
    private String imei;
    private String areaId;
    private String longitude;
    private String latitude;
    private String action;
    private SWPojoRequest sw;
    private String totalPendapatan;
    /* untuk rest client */
    private String idPhoto;
    private String surveyPhoto;
    /* untuk approval SW */
    private String swId;
    private String status;
    private String finalRecommendation;
    private String rejectedReason;
    private String userBWMP;
    private String localId;
    private List<SWApprovalHistoryReq> history;
    /* untuk list SW */
    private String getCountData;
    private String page;
    private String startLookupDate;
    private String endLookupDate;

    public String getSurveyDate() {
        return surveyDate;
    }

    public void setSurveyDate(String surveyDate) {
        this.surveyDate = surveyDate;
    }

    public SWProfileReq getProfile() {
        return profile;
    }

    public void setProfile(SWProfileReq profile) {
        this.profile = profile;
    }

    public String getIdCardName() {
        return idCardName;
    }

    public void setIdCardName(String idCardName) {
        this.idCardName = idCardName;
    }

    public String getSwLocation() {
        return swLocation;
    }

    public void setSwLocation(String swLocation) {
        this.swLocation = swLocation;
    }

    public SWPojoRequest getSw() {
        return sw;
    }

    public void setSw(SWPojoRequest sw) {
        this.sw = sw;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getGetCountData() {
        return getCountData;
    }

    public void setGetCountData(String getCountData) {
        this.getCountData = getCountData;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getStartLookupDate() {
        return startLookupDate;
    }

    public void setStartLookupDate(String startLookupDate) {
        this.startLookupDate = startLookupDate;
    }

    public String getEndLookupDate() {
        return endLookupDate;
    }

    public void setEndLookupDate(String endLookupDate) {
        this.endLookupDate = endLookupDate;
    }

    public String getSwId() {
        return swId;
    }

    public void setSwId(String swId) {
        this.swId = swId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFinalRecommendation() {
        return finalRecommendation;
    }

    public void setFinalRecommendation(String finalRecommendation) {
        this.finalRecommendation = finalRecommendation;
    }

    public String getRejectedReason() {
        return rejectedReason;
    }

    public void setRejectedReason(String rejectedReason) {
        this.rejectedReason = rejectedReason;
    }

    public String getUserBWMP() {
        return userBWMP;
    }

    public void setUserBWMP(String userBWMP) {
        this.userBWMP = userBWMP;
    }

    public String getIdPhoto() {
        return idPhoto;
    }

    public void setIdPhoto(String idPhoto) {
        this.idPhoto = idPhoto;
    }

    public String getSurveyPhoto() {
        return surveyPhoto;
    }

    public void setSurveyPhoto(String surveyPhoto) {
        this.surveyPhoto = surveyPhoto;
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    public String getDayLight() {
        return dayLight;
    }

    public void setDayLight(String dayLight) {
        this.dayLight = dayLight;
    }

    public String getPmId() {
        return pmId;
    }

    public void setPmId(String pmId) {
        this.pmId = pmId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTotalPendapatan() {
        return totalPendapatan;
    }

    public void setTotalPendapatan(String totalPendapatan) {
        this.totalPendapatan = totalPendapatan;
    }

    public List<SWApprovalHistoryReq> getHistory() {
        return history;
    }

    public void setHistory(List<SWApprovalHistoryReq> history) {
        this.history = history;
    }

    public String getTokenKey() {
        return tokenKey;
    }

    public void setTokenKey(String tokenKey) {
        this.tokenKey = tokenKey;
    }

    @Override
    public String toString() {
        return "SWRequest{" +
                "profile=" + profile +
                ", idCardName='" + idCardName + '\'' +
                ", swLocation='" + swLocation + '\'' +
                ", dayLight='" + dayLight + '\'' +
                ", surveyDate='" + surveyDate + '\'' +
                ", pmId='" + pmId + '\'' +
                ", time='" + time + '\'' +
                ", tokerKey='" + tokenKey + '\'' +
                ", username='" + username + '\'' +
                ", sessionKey='" + sessionKey + '\'' +
                ", imei='" + imei + '\'' +
                ", areaId='" + areaId + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", action='" + action + '\'' +
                ", sw=" + sw +
                ", totalPendapatan='" + totalPendapatan + '\'' +
                ", idPhoto='" + idPhoto + '\'' +
                ", surveyPhoto='" + surveyPhoto + '\'' +
                ", swId='" + swId + '\'' +
                ", status='" + status + '\'' +
                ", finalRecommendation='" + finalRecommendation + '\'' +
                ", rejectedReason='" + rejectedReason + '\'' +
                ", userBWMP='" + userBWMP + '\'' +
                ", localId='" + localId + '\'' +
                ", history=" + history +
                ", getCountData='" + getCountData + '\'' +
                ", page='" + page + '\'' +
                ", startLookupDate='" + startLookupDate + '\'' +
                ", endLookupDate='" + endLookupDate + '\'' +
                '}';
    }

}