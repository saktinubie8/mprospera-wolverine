package id.co.telkomsigma.btpns.mprospera.response;

/**
 * Created by Dzulfiqar on 27/04/2017.
 */
public class BusinessTypeAp3rList {

    private String id;
    private String businessType;
    private String subBusiness;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getSubBusiness() {
        return subBusiness;
    }

    public void setSubBusiness(String subBusiness) {
        this.subBusiness = subBusiness;
    }

}