package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@SuppressWarnings("ALL")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class AddSDAResponse extends BaseResponse {

    @Override
    public String toString() {
        return "AddSDAResponse{" +
                "sdaId='" + sdaId + '\'' +
                ", sdaKelList=" + sdaKelList +
                ", localId='" + localId + '\'' +
                '}';
    }

    private String sdaId;
    private List<AddSDAKelListResponse> sdaKelList;
    private String localId;

    public String getSdaId() {
        return sdaId;
    }

    public void setSdaId(String sdaId) {
        this.sdaId = sdaId;
    }

    public List<AddSDAKelListResponse> getSdaKelList() {
        return sdaKelList;
    }

    public void setSdaKelList(List<AddSDAKelListResponse> sdaKelList) {
        this.sdaKelList = sdaKelList;
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

}