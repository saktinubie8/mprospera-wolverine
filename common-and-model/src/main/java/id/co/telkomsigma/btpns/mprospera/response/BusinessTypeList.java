package id.co.telkomsigma.btpns.mprospera.response;

public class BusinessTypeList {

    private String id;
    private String businessType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

}