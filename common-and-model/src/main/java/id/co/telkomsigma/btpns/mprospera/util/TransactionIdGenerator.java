package id.co.telkomsigma.btpns.mprospera.util;

import java.text.Format;
import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.lang3.time.FastDateFormat;

/**
 * Simple STAN generator
 *
 * @author <a href="mailto:muhammad.ichsan@sigma.co.id">Muhammad Ichsan</a>
 */
public class TransactionIdGenerator {

    private static final Format FORMAT = FastDateFormat.getInstance("yyyyMMdd.HH.mm.ss.SSS");
    private static final AtomicLong lastCounter = new AtomicLong();

    public static String generateTransactionId() {
        long sequence = lastCounter.getAndIncrement();
        Date now = new Date();
        return new StringBuilder().append(FORMAT.format(now)).append('-').append(sequence).toString();
    }

}