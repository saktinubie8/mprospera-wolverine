package id.co.telkomsigma.btpns.mprospera.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Created by Dzulfiqar on 13/04/2017.
 */
@SuppressWarnings("ALL")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class SwAddressResponse {

    private String additionalAddress;
    private String districtId;
    private String districtName;
    private String name;
    private String placeCertificate;
    private String placeOwnerShip;
    private String postCode;
    private String provinceId;
    private String provinceName;
    private String regencyId;
    private String regencyName;
    private String street;
    private String villageId;
    private String villageName;

    public String getAdditionalAddress() {
        return additionalAddress;
    }

    public void setAdditionalAddress(String additionalAddress) {
        this.additionalAddress = additionalAddress;
    }

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlaceCertificate() {
        return placeCertificate;
    }

    public void setPlaceCertificate(String placeCertificate) {
        this.placeCertificate = placeCertificate;
    }

    public String getPlaceOwnerShip() {
        return placeOwnerShip;
    }

    public void setPlaceOwnerShip(String placeOwnerShip) {
        this.placeOwnerShip = placeOwnerShip;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getRegencyId() {
        return regencyId;
    }

    public void setRegencyId(String regencyId) {
        this.regencyId = regencyId;
    }

    public String getRegencyName() {
        return regencyName;
    }

    public void setRegencyName(String regencyName) {
        this.regencyName = regencyName;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getVillageId() {
        return villageId;
    }

    public void setVillageId(String villageId) {
        this.villageId = villageId;
    }

    public String getVillageName() {
        return villageName;
    }

    public void setVillageName(String villageName) {
        this.villageName = villageName;
    }

}