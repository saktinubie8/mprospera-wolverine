package id.co.telkomsigma.btpns.mprospera.response;

public class SWDetailResponse extends BaseResponse {

    private ListSWResponse swDetail;

    public ListSWResponse getSwDetail() {
        return swDetail;
    }

    public void setSwDetail(ListSWResponse swDetail) {
        this.swDetail = swDetail;
    }

}