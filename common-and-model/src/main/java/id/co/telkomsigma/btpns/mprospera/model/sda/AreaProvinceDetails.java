package id.co.telkomsigma.btpns.mprospera.model.sda;

import java.util.Date;

import javax.persistence.*;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

@Entity
@Table(name = "T_SDA_PROVINCE_DETAIL")
public class AreaProvinceDetails extends GenericModel {

    /**
     *
     */
    private static final long serialVersionUID = -6660012802097889240L;

    private Long areaDetailId;
    private AreaProvince areaProvince;
    private String countKKMiskin;
    private Date createdDate;
    private String createdBy;
    private String createdAt;
    private String longitude;
    private String latitude;

    @Column(name = "longitude")
    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Column(name = "latitude")
    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    private Boolean isDeleted = false;

    @Column(name = "is_deleted")
    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getAreaDetailId() {
        return areaDetailId;
    }

    public void setAreaDetailId(Long areaDetailId) {
        this.areaDetailId = areaDetailId;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = AreaProvince.class)
    @JoinColumn(name = "area_id", referencedColumnName = "id", nullable = true)
    public AreaProvince getAreaProvince() {
        return areaProvince;
    }

    public void setAreaProvince(AreaProvince areaProvince) {
        this.areaProvince = areaProvince;
    }

    @Column(name = "count_kk_miskin", nullable = false)
    public String getCountKKMiskin() {
        return countKKMiskin;
    }

    public void setCountKKMiskin(String countKKMiskin) {
        this.countKKMiskin = countKKMiskin;
    }

    @Column(name = "created_dt", nullable = false)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "created_by", nullable = false)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "created_at", nullable = false)
    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

}