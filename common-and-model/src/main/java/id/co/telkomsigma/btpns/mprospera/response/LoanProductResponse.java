package id.co.telkomsigma.btpns.mprospera.response;

import java.math.BigDecimal;
import java.util.List;

public class LoanProductResponse {

    private Long productId;
    private String productName;
    private Integer jumlahAngsur;
    private Integer tenorMinggu;
    private String tipePembiayaan;
    private String frekuensiAngsurSatuan;
    private Integer frekuensiAngsur;
    private String status;
    private BigDecimal marjin;
    private BigDecimal productRate;
    private List<BigDecimal> plafon;

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getJumlahAngsur() {
        return jumlahAngsur;
    }

    public void setJumlahAngsur(Integer jumlahAngsur) {
        this.jumlahAngsur = jumlahAngsur;
    }

    public Integer getTenorMinggu() {
        return tenorMinggu;
    }

    public void setTenorMinggu(Integer tenorMinggu) {
        this.tenorMinggu = tenorMinggu;
    }

    public String getTipePembiayaan() {
        return tipePembiayaan;
    }

    public void setTipePembiayaan(String tipePembiayaan) {
        this.tipePembiayaan = tipePembiayaan;
    }

    public String getFrekuensiAngsurSatuan() {
        return frekuensiAngsurSatuan;
    }

    public void setFrekuensiAngsurSatuan(String frekuensiAngsurSatuan) {
        this.frekuensiAngsurSatuan = frekuensiAngsurSatuan;
    }

    public Integer getFrekuensiAngsur() {
        return frekuensiAngsur;
    }

    public void setFrekuensiAngsur(Integer frekuensiAngsur) {
        this.frekuensiAngsur = frekuensiAngsur;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getMarjin() {
        return marjin;
    }

    public void setMarjin(BigDecimal marjin) {
        this.marjin = marjin;
    }

    public List<BigDecimal> getPlafon() {
        return plafon;
    }

    public void setPlafon(List<BigDecimal> plafon) {
        this.plafon = plafon;
    }

    public BigDecimal getProductRate() {
        return productRate;
    }

    public void setProductRate(BigDecimal productRate) {
        this.productRate = productRate;
    }

}