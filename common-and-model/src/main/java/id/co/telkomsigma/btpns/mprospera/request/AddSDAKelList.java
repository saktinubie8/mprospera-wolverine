package id.co.telkomsigma.btpns.mprospera.request;

public class AddSDAKelList {

    private String areaCategory;
    private String parentAreaId;
    private String areaId;
    private String areaName;
    private String countKkMiskin;
    private String sdaId;
    private String action;
    private String pejabat;
    private String jabatan;
    private String phoneNumber;
    private String localId;

    public String getAreaCategory() {
        return areaCategory;
    }

    public void setAreaCategory(String areaCategory) {
        this.areaCategory = areaCategory;
    }

    public String getParentAreaId() {
        return parentAreaId;
    }

    public void setParentAreaId(String parentAreaId) {
        this.parentAreaId = parentAreaId;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getCountKkMiskin() {
        return countKkMiskin;
    }

    public void setCountKkMiskin(String countKkMiskin) {
        this.countKkMiskin = countKkMiskin;
    }

    public String getSdaId() {
        return sdaId;
    }

    public void setSdaId(String sdaId) {
        this.sdaId = sdaId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getPejabat() {
        return pejabat;
    }

    public void setPejabat(String pejabat) {
        this.pejabat = pejabat;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    @Override
    public String toString() {
        return "AddSDAKelList{" +
                "areaCategory='" + areaCategory + '\'' +
                ", parentAreaId='" + parentAreaId + '\'' +
                ", areaId='" + areaId + '\'' +
                ", areaName='" + areaName + '\'' +
                ", countKkMiskin='" + countKkMiskin + '\'' +
                ", sdaId='" + sdaId + '\'' +
                ", action='" + action + '\'' +
                ", pejabat='" + pejabat + '\'' +
                ", jabatan='" + jabatan + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", localId='" + localId + '\'' +
                '}';
    }

}