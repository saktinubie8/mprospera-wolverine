package id.co.telkomsigma.btpns.mprospera.request;

import java.util.List;

/**
 * Created by Dzulfiqar on 25/04/2017.
 */
public class SWPojoRequest {

    private String id;
    private String swId;
    private String swLocalId;
    private String status;
    private String action;
    private String page;
    private String appId;
    private String createdAt;
    private String createdBy;
    private String updatedAt;
    private String dayLight;
    private String flag;
    private String sentraId;
    private String sentraName;
    private String groupId;
    private String groupName;
    private String customerCif;
    private String customerId;
    private String customerName;
    private String customerType;
    private String customerExists;
    private String loanExists;
    private String idCardName;
    private String idCardNumber;
    private String pmId;
    private String productString;
    private String rejectedReason;
    private String surveyDate;
    private String syncStatus;
    private String totalPendapatan;
    private String income;
    private String netIncome;
    private String spending;
    private String hasApprovedMs;
    private String hasBusinessPlacePhoto;
    private String hasIdPhoto;
    private WismaSWReq wisma;
    private SWProfileReq profile;
    private SWAddressReq address;
    private SWBusinessReq business;
    private SWCalculationVariableReq calcVariable;
    private SWExpenseReq expense;
    private ItemCalcLainReq itemCalcLain;
    private SWPartnerReq partner;
    private BusinessImageFile businessImageFile;
    private IdImageFile idImageFile;
    private List<AWGMBuyList> awgmPurchasing;
    private List<DirectBuyList> directPurchasing;
    private List<ReferenceList> referenceList;
    private List<SWProductListRequest> swProducts;
    private List<SWApprovalHistoryReq> approvalHistories;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSwId() {
        return swId;
    }

    public void setSwId(String swId) {
        this.swId = swId;
    }

    public String getSwLocalId() {
        return swLocalId;
    }

    public void setSwLocalId(String swLocalId) {
        this.swLocalId = swLocalId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDayLight() {
        return dayLight;
    }

    public void setDayLight(String dayLight) {
        this.dayLight = dayLight;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getSentraId() {
        return sentraId;
    }

    public void setSentraId(String sentraId) {
        this.sentraId = sentraId;
    }

    public String getSentraName() {
        return sentraName;
    }

    public void setSentraName(String sentraName) {
        this.sentraName = sentraName;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getCustomerCif() {
        return customerCif;
    }

    public void setCustomerCif(String customerCif) {
        this.customerCif = customerCif;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getCustomerExists() {
        return customerExists;
    }

    public void setCustomerExists(String customerExists) {
        this.customerExists = customerExists;
    }

    public String getLoanExists() {
        return loanExists;
    }

    public void setLoanExists(String loanExists) {
        this.loanExists = loanExists;
    }

    public String getIdCardName() {
        return idCardName;
    }

    public void setIdCardName(String idCardName) {
        this.idCardName = idCardName;
    }

    public String getIdCardNumber() {
        return idCardNumber;
    }

    public void setIdCardNumber(String idCardNumber) {
        this.idCardNumber = idCardNumber;
    }

    public String getPmId() {
        return pmId;
    }

    public void setPmId(String pmId) {
        this.pmId = pmId;
    }

    public String getProductString() {
        return productString;
    }

    public void setProductString(String productString) {
        this.productString = productString;
    }

    public String getRejectedReason() {
        return rejectedReason;
    }

    public void setRejectedReason(String rejectedReason) {
        this.rejectedReason = rejectedReason;
    }

    public String getSurveyDate() {
        return surveyDate;
    }

    public void setSurveyDate(String surveyDate) {
        this.surveyDate = surveyDate;
    }

    public String getSyncStatus() {
        return syncStatus;
    }

    public void setSyncStatus(String syncStatus) {
        this.syncStatus = syncStatus;
    }

    public String getTotalPendapatan() {
        return totalPendapatan;
    }

    public void setTotalPendapatan(String totalPendapatan) {
        this.totalPendapatan = totalPendapatan;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getNetIncome() {
        return netIncome;
    }

    public void setNetIncome(String netIncome) {
        this.netIncome = netIncome;
    }

    public String getSpending() {
        return spending;
    }

    public void setSpending(String spending) {
        this.spending = spending;
    }

    public String getHasApprovedMs() {
        return hasApprovedMs;
    }

    public void setHasApprovedMs(String hasApprovedMs) {
        this.hasApprovedMs = hasApprovedMs;
    }

    public String getHasBusinessPlacePhoto() {
        return hasBusinessPlacePhoto;
    }

    public void setHasBusinessPlacePhoto(String hasBusinessPlacePhoto) {
        this.hasBusinessPlacePhoto = hasBusinessPlacePhoto;
    }

    public String getHasIdPhoto() {
        return hasIdPhoto;
    }

    public void setHasIdPhoto(String hasIdPhoto) {
        this.hasIdPhoto = hasIdPhoto;
    }

    public WismaSWReq getWisma() {
        return wisma;
    }

    public void setWisma(WismaSWReq wisma) {
        this.wisma = wisma;
    }

    public SWProfileReq getProfile() {
        return profile;
    }

    public void setProfile(SWProfileReq profile) {
        this.profile = profile;
    }

    public SWAddressReq getAddress() {
        return address;
    }

    public void setAddress(SWAddressReq address) {
        this.address = address;
    }

    public SWBusinessReq getBusiness() {
        return business;
    }

    public void setBusiness(SWBusinessReq business) {
        this.business = business;
    }

    public SWCalculationVariableReq getCalcVariable() {
        return calcVariable;
    }

    public void setCalcVariable(SWCalculationVariableReq calcVariable) {
        this.calcVariable = calcVariable;
    }

    public SWExpenseReq getExpense() {
        return expense;
    }

    public void setExpense(SWExpenseReq expense) {
        this.expense = expense;
    }

    public ItemCalcLainReq getItemCalcLain() {
        return itemCalcLain;
    }

    public void setItemCalcLain(ItemCalcLainReq itemCalcLain) {
        this.itemCalcLain = itemCalcLain;
    }

    public SWPartnerReq getPartner() {
        return partner;
    }

    public void setPartner(SWPartnerReq partner) {
        this.partner = partner;
    }

    public BusinessImageFile getBusinessImageFile() {
        return businessImageFile;
    }

    public void setBusinessImageFile(BusinessImageFile businessImageFile) {
        this.businessImageFile = businessImageFile;
    }

    public IdImageFile getIdImageFile() {
        return idImageFile;
    }

    public void setIdImageFile(IdImageFile idImageFile) {
        this.idImageFile = idImageFile;
    }

    public List<AWGMBuyList> getAwgmPurchasing() {
        return awgmPurchasing;
    }

    public void setAwgmPurchasing(List<AWGMBuyList> awgmPurchasing) {
        this.awgmPurchasing = awgmPurchasing;
    }

    public List<DirectBuyList> getDirectPurchasing() {
        return directPurchasing;
    }

    public void setDirectPurchasing(List<DirectBuyList> directPurchasing) {
        this.directPurchasing = directPurchasing;
    }

    public List<ReferenceList> getReferenceList() {
        return referenceList;
    }

    public void setReferenceList(List<ReferenceList> referenceList) {
        this.referenceList = referenceList;
    }

    public List<SWProductListRequest> getSwProducts() {
        return swProducts;
    }

    public void setSwProducts(List<SWProductListRequest> swProducts) {
        this.swProducts = swProducts;
    }

    public List<SWApprovalHistoryReq> getApprovalHistories() {
        return approvalHistories;
    }

    public void setApprovalHistories(List<SWApprovalHistoryReq> approvalHistories) {
        this.approvalHistories = approvalHistories;
    }

}