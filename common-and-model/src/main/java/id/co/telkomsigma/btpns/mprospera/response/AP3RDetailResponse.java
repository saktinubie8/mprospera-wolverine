package id.co.telkomsigma.btpns.mprospera.response;

public class AP3RDetailResponse extends BaseResponse {

    private AP3RList ap3rList;

    public AP3RList getAp3rList() {
        return ap3rList;
    }

    public void setAp3rList(AP3RList ap3rList) {
        this.ap3rList = ap3rList;
    }

}