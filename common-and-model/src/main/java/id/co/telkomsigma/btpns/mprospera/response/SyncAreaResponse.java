package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class SyncAreaResponse extends BaseResponse {

    public List<AreaProvinceResponse> getAreaListProvince() {
        return areaListProvince;
    }

    public void setAreaListProvince(List<AreaProvinceResponse> areaListProvince) {
        this.areaListProvince = areaListProvince;
    }

    private List<AreaProvinceResponse> areaListProvince;

}