package id.co.telkomsigma.btpns.mprospera.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Created by Dzulfiqar on 13/04/2017.
 */
@SuppressWarnings("ALL")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class SWBusinessResponse {

    private String address;
    private String ageMonth;
    private String ageYear;
    private String businessLocation;
    private String businessOwnership;
    private String businessRunner;
    private String businessShariaCompliance;
    private String desc;
    private String name;
    private String type;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAgeMonth() {
        return ageMonth;
    }

    public void setAgeMonth(String ageMonth) {
        this.ageMonth = ageMonth;
    }

    public String getAgeYear() {
        return ageYear;
    }

    public void setAgeYear(String ageYear) {
        this.ageYear = ageYear;
    }

    public String getBusinessLocation() {
        return businessLocation;
    }

    public void setBusinessLocation(String businessLocation) {
        this.businessLocation = businessLocation;
    }

    public String getBusinessOwnership() {
        return businessOwnership;
    }

    public void setBusinessOwnership(String businessOwnership) {
        this.businessOwnership = businessOwnership;
    }

    public String getBusinessRunner() {
        return businessRunner;
    }

    public void setBusinessRunner(String businessRunner) {
        this.businessRunner = businessRunner;
    }

    public String getBusinessShariaCompliance() {
        return businessShariaCompliance;
    }

    public void setBusinessShariaCompliance(String businessShariaCompliance) {
        this.businessShariaCompliance = businessShariaCompliance;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}