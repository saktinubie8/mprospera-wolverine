package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class ProductListResponse extends BaseResponse {

    private List<LoanProductResponse> productList;
    private String grandTotal;
    private String currentTotal;
    private String totalPage;

    public List<LoanProductResponse> getProductList() {
        return productList;
    }

    public void setProductList(List<LoanProductResponse> productList) {
        this.productList = productList;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getCurrentTotal() {
        return currentTotal;
    }

    public void setCurrentTotal(String currentTotal) {
        this.currentTotal = currentTotal;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

}