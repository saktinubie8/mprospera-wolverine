package id.co.telkomsigma.btpns.mprospera.model.sw;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_LOAN_PRODUCT")
public class LoanProduct implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private Long productId;
    private String prosperaId;
    private String productName;
    private Integer tenor;
    private Integer installmentCount;
    private String loanType;
    private String installmentFreqTime;
    private Integer installmentFreqCount;
    private String status;
    private BigDecimal margin;
    private BigDecimal productRate;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    @Column(name = "product_name")
    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Column(name = "tenor")
    public Integer getTenor() {
        return tenor;
    }

    public void setTenor(Integer tenor) {
        this.tenor = tenor;
    }

    @Column(name = "installment_count")
    public Integer getInstallmentCount() {
        return installmentCount;
    }

    public void setInstallmentCount(Integer installmentCount) {
        this.installmentCount = installmentCount;
    }

    @Column(name = "loan_type")
    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    @Column(name = "installment_freq_time")
    public String getInstallmentFreqTime() {
        return installmentFreqTime;
    }

    public void setInstallmentFreqTime(String installmentFreqTime) {
        this.installmentFreqTime = installmentFreqTime;
    }

    @Column(name = "installment_freq_count")
    public Integer getInstallmentFreqCount() {
        return installmentFreqCount;
    }

    public void setInstallmentFreqCount(Integer installmentFreqCount) {
        this.installmentFreqCount = installmentFreqCount;
    }

    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "margin")
    public BigDecimal getMargin() {
        return margin;
    }

    public void setMargin(BigDecimal margin) {
        this.margin = margin;
    }

    @Column(name = "prospera_id")
    public String getProsperaId() {
        return prosperaId;
    }

    public void setProsperaId(String prosperaId) {
        this.prosperaId = prosperaId;
    }

    @Column(name = "product_rate")
    public BigDecimal getProductRate() {
        return productRate;
    }

    public void setProductRate(BigDecimal productRate) {
        this.productRate = productRate;
    }

    @Override
    public String toString() {
        return "LoanProduct{" +
                "productId=" + productId +
                ", prosperaId='" + prosperaId + '\'' +
                ", productName='" + productName + '\'' +
                ", tenor=" + tenor +
                ", installmentCount=" + installmentCount +
                ", loanType='" + loanType + '\'' +
                ", installmentFreqTime='" + installmentFreqTime + '\'' +
                ", installmentFreqCount=" + installmentFreqCount +
                ", status='" + status + '\'' +
                ", margin=" + margin +
                '}';
    }

}