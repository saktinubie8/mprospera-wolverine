package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class ListMMResponse extends BaseResponse {

    private String grandTotal;
    private String currentTotal;
    private String totalPage;
    private List<MMResponse> mmList;

    private List<String> deletedMmList;

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getCurrentTotal() {
        return currentTotal;
    }

    public void setCurrentTotal(String currentTotal) {
        this.currentTotal = currentTotal;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    public List<MMResponse> getMmList() {
        return mmList;
    }

    public void setMmList(List<MMResponse> mmList) {
        this.mmList = mmList;
    }

    public List<String> getDeletedMmList() {
        return deletedMmList;
    }

    public void setDeletedMmList(List<String> deletedMmList) {
        this.deletedMmList = deletedMmList;
    }

    @Override
    public String toString() {
        return "ListMMResponse [grandTotal=" + grandTotal + ", currentTotal=" + currentTotal + ", totalPage="
                + totalPage + ", mmList=" + mmList + ", getResponseCode()=" + getResponseCode()
                + ", getResponseMessage()=" + getResponseMessage() + "]";
    }

}