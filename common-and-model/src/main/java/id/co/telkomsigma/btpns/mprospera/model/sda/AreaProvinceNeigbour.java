package id.co.telkomsigma.btpns.mprospera.model.sda;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;

/**
 * Created by hp on 12/05/2017.
 */
@Entity
@Table(name = "T_SDA_PROVINCE_NEIGHBOUR")
public class AreaProvinceNeigbour extends GenericModel {

    Long mappingId;
    String areaProvinceId;
    String areaProvinceNeighbourId;

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false, unique = true)
    public Long getMappingId() {
        return mappingId;
    }

    public void setMappingId(Long mappingId) {
        this.mappingId = mappingId;
    }

    @Column(name = "area_id")
    public String getAreaProvinceId() {
        return areaProvinceId;
    }

    public void setAreaProvinceId(String areaProvinceId) {
        this.areaProvinceId = areaProvinceId;
    }

    @Column(name = "area_neighbour_id")
    public String getAreaProvinceNeighbourId() {
        return areaProvinceNeighbourId;
    }

    public void setAreaProvinceNeighbourId(String areaProvinceNeighbourId) {
        this.areaProvinceNeighbourId = areaProvinceNeighbourId;
    }
}
