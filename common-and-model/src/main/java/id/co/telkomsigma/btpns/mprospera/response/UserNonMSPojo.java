package id.co.telkomsigma.btpns.mprospera.response;

/**
 * Created by Dzulfiqar on 2/05/2017.
 */
public class UserNonMSPojo {

    private String userId;
    private String name;
    private String role;
    private String limit;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

}